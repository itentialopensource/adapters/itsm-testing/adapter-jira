# Atlassian Jira

Vendor: Atlassian
Homepage: https://www.atlassian.com/

Product: Atlassian Jira
Product Page: https://www.atlassian.com/software/jira

## Introduction
We classify Atlassian Jira into the ITSM or Service Management domain as Atlassian Jira provides the ability to add, modify, and track source code changes.

## Why Integrate
The Atlassian Jira adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Atlassian Jira. With this adapter you have the ability to plan, track, and manage all your agile software development projects from a single tool.

With this adapter you have the ability to perform operations with Jira such as:

- Create, Modify Permissions for, and Complete Change Requests
- Add, Manage, and Remove Inventory Devices
- Open and Close Tickets
- Update and Close a Service Catalog Request
- Send Notifications to Jira Assignees and Approvers
- Change Jira Assignees
- Create and Retrieve Knowledge Base Articles

## Additional Product Documentation
The [API documents for Jira](https://developer.atlassian.com/cloud/jira/platform/rest/v3/intro/#version)