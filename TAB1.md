# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Jira System. The API that was used to build the adapter for Jira is usually available in the report directory of this adapter. The adapter utilizes the Jira API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Atlassian Jira adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Atlassian Jira. With this adapter you have the ability to plan, track, and manage all your agile software development projects from a single tool.

With this adapter you have the ability to perform operations with Jira such as:

- Create, Modify Permissions for, and Complete Change Requests
- Add, Manage, and Remove Inventory Devices
- Open and Close Tickets
- Update and Close a Service Catalog Request
- Send Notifications to Jira Assignees and Approvers
- Change Jira Assignees
- Create and Retrieve Knowledge Base Articles

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
