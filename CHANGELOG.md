
## 2.4.4 [10-15-2024]

* Changes made at 2024.10.14_21:38PM

See merge request itentialopensource/adapters/adapter-jira!27

---

## 2.4.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-jira!25

---

## 2.4.2 [08-15-2024]

* Changes made at 2024.08.14_19:59PM

See merge request itentialopensource/adapters/adapter-jira!24

---

## 2.4.1 [08-07-2024]

* Changes made at 2024.08.06_22:07PM

See merge request itentialopensource/adapters/adapter-jira!23

---

## 2.4.0 [07-08-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!22

---

## 2.3.6 [03-27-2024]

* Changes made at 2024.03.27_14:11PM

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!21

---

## 2.3.5 [03-21-2024]

* Changes made at 2024.03.21_14:49PM

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!20

---

## 2.3.4 [03-13-2024]

* Changes made at 2024.03.13_15:10PM

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!19

---

## 2.3.3 [03-11-2024]

* Changes made at 2024.03.11_14:50PM

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!18

---

## 2.3.2 [02-28-2024]

* Changes made at 2024.02.28_11:17AM

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!17

---

## 2.3.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!16

---

## 2.3.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!14

---

## 2.2.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!14

---

## 2.1.0 [09-26-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!13

---

## 2.0.1 [06-06-2023]

* Cleaned up description information.

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!11

---

## 2.0.0 [05-18-2023]

* Add calls

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!12

---

## 1.7.6 [09-22-2022]

* Patch/adapt 2380

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!10

---

## 1.7.5 [08-06-2022]

* Updated to the latest definition and Migration

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!9

---

## 1.7.4 [03-08-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!8

---

## 1.7.3 [03-08-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!8

---

## 1.7.2 [07-08-2020]

* migration

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!7

---

## 1.7.1 [01-13-2020]

* december migration

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!6

---

## 1.7.0 [11-22-2019]

* update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!5

---

## 1.6.0 [11-07-2019]

* update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!5

---

## 1.5.0 [09-18-2019]

* september migration

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!4

---
## 1.4.0 [07-30-2019]
* migration, cateogorization and artifactize

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!3

---

## 1.3.0 [07-18-2019]
* migration, cateogorization and artifactize

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!3

---

## 1.2.0 [07-18-2019]
* migration, cateogorization and artifactize

See merge request itentialopensource/adapters/itsm-testing/adapter-jira!3

---

## 1.1.0 [04-26-2019]
* Minor/ph 23751

See merge request itentialopensource/adapters/adapter-jira!2

---

## 1.0.1 [04-09-2019]
* Bug fixes and performance improvements

See commit 0fc5ba4

---
