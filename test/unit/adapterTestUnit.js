/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-jira',
      type: 'Jira',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Jira = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Jira Adapter Test', () => {
  describe('Jira Class Tests', () => {
    const a = new Jira(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('jira'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.7.3', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.14.2', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('jira'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Jira', pronghornDotJson.export);
          assert.equal('Jira', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-jira', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('jira'));
          assert.equal('Jira', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-jira', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-jira', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getRestapi2applicationrole - errors', () => {
      it('should have a getRestapi2applicationrole function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2applicationrole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2applicationrolekey - errors', () => {
      it('should have a getRestapi2applicationrolekey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2applicationrolekey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getRestapi2applicationrolekey(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('key is required for getRestapi2applicationrolekey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2auditingrecord - errors', () => {
      it('should have a getRestapi2auditingrecord function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2auditingrecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2dashboard - errors', () => {
      it('should have a getRestapi2dashboard function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2dashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2dashboardsearch - errors', () => {
      it('should have a getRestapi2dashboardsearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2dashboardsearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2dashboarddashboardIditemsitemIdproperties - errors', () => {
      it('should have a getRestapi2dashboarddashboardIditemsitemIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2dashboarddashboardIditemsitemIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.getRestapi2dashboarddashboardIditemsitemIdproperties(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('dashboardId is required for getRestapi2dashboarddashboardIditemsitemIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.getRestapi2dashboarddashboardIditemsitemIdproperties('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('itemId is required for getRestapi2dashboarddashboardIditemsitemIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey - errors', () => {
      it('should have a getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('dashboardId is required for getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('itemId is required for getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey - errors', () => {
      it('should have a putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('dashboardId is required for putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('itemId is required for putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('dashboardId is required for deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('itemId is required for deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2dashboardid - errors', () => {
      it('should have a getRestapi2dashboardid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2dashboardid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2dashboardid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2dashboardid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filter - errors', () => {
      it('should have a getRestapi2filter function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2filter - errors', () => {
      it('should have a postRestapi2filter function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2filter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2filter('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2filter', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filterfavourite - errors', () => {
      it('should have a getRestapi2filterfavourite function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filterfavourite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filtermy - errors', () => {
      it('should have a getRestapi2filtermy function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filtermy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filtersearch - errors', () => {
      it('should have a getRestapi2filtersearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filtersearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filterid - errors', () => {
      it('should have a getRestapi2filterid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2filterid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2filterid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filterid - errors', () => {
      it('should have a putRestapi2filterid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2filterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2filterid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2filterid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2filterid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2filterid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filterid - errors', () => {
      it('should have a deleteRestapi2filterid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2filterid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2filterid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2filterid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filteridcolumns - errors', () => {
      it('should have a getRestapi2filteridcolumns function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filteridcolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2filteridcolumns(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2filteridcolumns', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filteridcolumns - errors', () => {
      it('should have a putRestapi2filteridcolumns function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2filteridcolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2filteridcolumns(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2filteridcolumns', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filteridcolumns - errors', () => {
      it('should have a deleteRestapi2filteridcolumns function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2filteridcolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2filteridcolumns(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2filteridcolumns', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filteridfavourite - errors', () => {
      it('should have a putRestapi2filteridfavourite function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2filteridfavourite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2filteridfavourite(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2filteridfavourite', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filteridfavourite - errors', () => {
      it('should have a deleteRestapi2filteridfavourite function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2filteridfavourite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2filteridfavourite(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2filteridfavourite', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filterdefaultShareScope - errors', () => {
      it('should have a getRestapi2filterdefaultShareScope function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filterdefaultShareScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filterdefaultShareScope - errors', () => {
      it('should have a putRestapi2filterdefaultShareScope function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2filterdefaultShareScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2filterdefaultShareScope(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2filterdefaultShareScope', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filteridpermission - errors', () => {
      it('should have a getRestapi2filteridpermission function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filteridpermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2filteridpermission(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2filteridpermission', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2filteridpermission - errors', () => {
      it('should have a postRestapi2filteridpermission function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2filteridpermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2filteridpermission(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2filteridpermission', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2filteridpermission('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2filteridpermission', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filteridpermissionpermissionId - errors', () => {
      it('should have a getRestapi2filteridpermissionpermissionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2filteridpermissionpermissionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2filteridpermissionpermissionId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2filteridpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.getRestapi2filteridpermissionpermissionId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('permissionId is required for getRestapi2filteridpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filteridpermissionpermissionId - errors', () => {
      it('should have a deleteRestapi2filteridpermissionpermissionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2filteridpermissionpermissionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2filteridpermissionpermissionId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2filteridpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.deleteRestapi2filteridpermissionpermissionId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('permissionId is required for deleteRestapi2filteridpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2settingscolumns - errors', () => {
      it('should have a getRestapi2settingscolumns function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2settingscolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2settingscolumns - errors', () => {
      it('should have a putRestapi2settingscolumns function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2settingscolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2notificationscheme - errors', () => {
      it('should have a getRestapi2notificationscheme function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2notificationscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2notificationschemeid - errors', () => {
      it('should have a getRestapi2notificationschemeid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2notificationschemeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2notificationschemeid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2notificationschemeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issuepropertiespropertyKey - errors', () => {
      it('should have a putRestapi2issuepropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issuepropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2issuepropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2issuepropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issuepropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issuepropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issuepropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2issuepropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issuepropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2issuepropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2issuepropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteRestapi2issuepropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for deleteRestapi2issuepropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyproperties - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyproperties(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeypropertiespropertyKey - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeypropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeypropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeypropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeypropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeypropertiespropertyKey - errors', () => {
      it('should have a putRestapi2issueissueIdOrKeypropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKeypropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeypropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeypropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeypropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeypropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeypropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeypropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeypropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeypropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2issueissueIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2search - errors', () => {
      it('should have a getRestapi2search function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2search === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2search - errors', () => {
      it('should have a postRestapi2search function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2search === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2search(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2search', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuepicker - errors', () => {
      it('should have a getRestapi2issuepicker function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuepicker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2expressioneval - errors', () => {
      it('should have a postRestapi2expressioneval function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2expressioneval === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2expressioneval('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2expressioneval', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2applicationProperties - errors', () => {
      it('should have a getRestapi2applicationProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2applicationProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2applicationPropertiesadvancedSettings - errors', () => {
      it('should have a getRestapi2applicationPropertiesadvancedSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2applicationPropertiesadvancedSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2applicationPropertiesid - errors', () => {
      it('should have a putRestapi2applicationPropertiesid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2applicationPropertiesid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2applicationPropertiesid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2applicationPropertiesid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2applicationPropertiesid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2applicationPropertiesid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configuration - errors', () => {
      it('should have a getRestapi2configuration function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2configuration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2jqlautocompletedata - errors', () => {
      it('should have a getRestapi2jqlautocompletedata function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2jqlautocompletedata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2jqlautocompletedatasuggestions - errors', () => {
      it('should have a getRestapi2jqlautocompletedatasuggestions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2jqlautocompletedatasuggestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2jqlpdcleaner - errors', () => {
      it('should have a postRestapi2jqlpdcleaner function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2jqlpdcleaner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2jqlpdcleaner(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2jqlpdcleaner', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2mypreferences - errors', () => {
      it('should have a getRestapi2mypreferences function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2mypreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2mypreferences - errors', () => {
      it('should have a putRestapi2mypreferences function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2mypreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2mypreferences('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2mypreferences', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2mypreferences - errors', () => {
      it('should have a deleteRestapi2mypreferences function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2mypreferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2mypreferenceslocale - errors', () => {
      it('should have a getRestapi2mypreferenceslocale function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2mypreferenceslocale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2mypreferenceslocale - errors', () => {
      it('should have a putRestapi2mypreferenceslocale function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2mypreferenceslocale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2mypreferenceslocale(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2mypreferenceslocale', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2mypreferenceslocale - errors', () => {
      it('should have a deleteRestapi2mypreferenceslocale function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2mypreferenceslocale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2myself - errors', () => {
      it('should have a getRestapi2myself function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2myself === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2mypermissions - errors', () => {
      it('should have a getRestapi2mypermissions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2mypermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissions - errors', () => {
      it('should have a getRestapi2permissions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2permissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionscheck - errors', () => {
      it('should have a postRestapi2permissionscheck function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2permissionscheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2permissionscheck(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2permissionscheck', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionsproject - errors', () => {
      it('should have a postRestapi2permissionsproject function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2permissionsproject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2permissionsproject(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2permissionsproject', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionscheme - errors', () => {
      it('should have a getRestapi2permissionscheme function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2permissionscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionscheme - errors', () => {
      it('should have a postRestapi2permissionscheme function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2permissionscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2permissionscheme('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2permissionscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionschemeschemeId - errors', () => {
      it('should have a getRestapi2permissionschemeschemeId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2permissionschemeschemeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.getRestapi2permissionschemeschemeId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for getRestapi2permissionschemeschemeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2permissionschemeschemeId - errors', () => {
      it('should have a putRestapi2permissionschemeschemeId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2permissionschemeschemeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.putRestapi2permissionschemeschemeId(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for putRestapi2permissionschemeschemeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2permissionschemeschemeId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2permissionschemeschemeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2permissionschemeschemeId - errors', () => {
      it('should have a deleteRestapi2permissionschemeschemeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2permissionschemeschemeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.deleteRestapi2permissionschemeschemeId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for deleteRestapi2permissionschemeschemeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionschemeschemeIdpermission - errors', () => {
      it('should have a getRestapi2permissionschemeschemeIdpermission function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2permissionschemeschemeIdpermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.getRestapi2permissionschemeschemeIdpermission(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for getRestapi2permissionschemeschemeIdpermission', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionschemeschemeIdpermission - errors', () => {
      it('should have a postRestapi2permissionschemeschemeIdpermission function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2permissionschemeschemeIdpermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.postRestapi2permissionschemeschemeIdpermission(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for postRestapi2permissionschemeschemeIdpermission', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2permissionschemeschemeIdpermission('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2permissionschemeschemeIdpermission', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionschemeschemeIdpermissionpermissionId - errors', () => {
      it('should have a getRestapi2permissionschemeschemeIdpermissionpermissionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2permissionschemeschemeIdpermissionpermissionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.getRestapi2permissionschemeschemeIdpermissionpermissionId(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for getRestapi2permissionschemeschemeIdpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.getRestapi2permissionschemeschemeIdpermissionpermissionId('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('permissionId is required for getRestapi2permissionschemeschemeIdpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2permissionschemeschemeIdpermissionpermissionId - errors', () => {
      it('should have a deleteRestapi2permissionschemeschemeIdpermissionpermissionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2permissionschemeschemeIdpermissionpermissionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.deleteRestapi2permissionschemeschemeIdpermissionpermissionId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('schemeId is required for deleteRestapi2permissionschemeschemeIdpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permissionId', (done) => {
        try {
          a.deleteRestapi2permissionschemeschemeIdpermissionpermissionId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('permissionId is required for deleteRestapi2permissionschemeschemeIdpermissionpermissionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdnotificationscheme - errors', () => {
      it('should have a getRestapi2projectprojectKeyOrIdnotificationscheme function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectKeyOrIdnotificationscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKeyOrId', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdnotificationscheme(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectKeyOrId is required for getRestapi2projectprojectKeyOrIdnotificationscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2project - errors', () => {
      it('should have a getRestapi2project function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2project === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2project - errors', () => {
      it('should have a postRestapi2project function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2project === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2project(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2project', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectsearch - errors', () => {
      it('should have a getRestapi2projectsearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectsearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKey - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKey - errors', () => {
      it('should have a putRestapi2projectprojectIdOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectprojectIdOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for putRestapi2projectprojectIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2projectprojectIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectprojectIdOrKey - errors', () => {
      it('should have a deleteRestapi2projectprojectIdOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2projectprojectIdOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKey(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for deleteRestapi2projectprojectIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeystatuses - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeystatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeystatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeystatuses(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeystatuses', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeytypenewProjectTypeKey - errors', () => {
      it('should have a putRestapi2projectprojectIdOrKeytypenewProjectTypeKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectprojectIdOrKeytypenewProjectTypeKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeytypenewProjectTypeKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for putRestapi2projectprojectIdOrKeytypenewProjectTypeKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newProjectTypeKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeytypenewProjectTypeKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('newProjectTypeKey is required for putRestapi2projectprojectIdOrKeytypenewProjectTypeKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdpermissionscheme - errors', () => {
      it('should have a getRestapi2projectprojectKeyOrIdpermissionscheme function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectKeyOrIdpermissionscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKeyOrId', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdpermissionscheme(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectKeyOrId is required for getRestapi2projectprojectKeyOrIdpermissionscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectKeyOrIdpermissionscheme - errors', () => {
      it('should have a putRestapi2projectprojectKeyOrIdpermissionscheme function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectprojectKeyOrIdpermissionscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKeyOrId', (done) => {
        try {
          a.putRestapi2projectprojectKeyOrIdpermissionscheme(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectKeyOrId is required for putRestapi2projectprojectKeyOrIdpermissionscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2projectprojectKeyOrIdpermissionscheme('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2projectprojectKeyOrIdpermissionscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdsecuritylevel - errors', () => {
      it('should have a getRestapi2projectprojectKeyOrIdsecuritylevel function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectKeyOrIdsecuritylevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKeyOrId', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdsecuritylevel(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectKeyOrId is required for getRestapi2projectprojectKeyOrIdsecuritylevel', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme - errors', () => {
      it('should have a getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKeyOrId', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectKeyOrId is required for getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projecttype - errors', () => {
      it('should have a getRestapi2projecttype function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projecttype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projecttypeprojectTypeKey - errors', () => {
      it('should have a getRestapi2projecttypeprojectTypeKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projecttypeprojectTypeKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectTypeKey', (done) => {
        try {
          a.getRestapi2projecttypeprojectTypeKey(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectTypeKey is required for getRestapi2projecttypeprojectTypeKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projecttypeprojectTypeKeyaccessible - errors', () => {
      it('should have a getRestapi2projecttypeprojectTypeKeyaccessible function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projecttypeprojectTypeKeyaccessible === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectTypeKey', (done) => {
        try {
          a.getRestapi2projecttypeprojectTypeKeyaccessible(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectTypeKey is required for getRestapi2projecttypeprojectTypeKeyaccessible', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2serverInfo - errors', () => {
      it('should have a getRestapi2serverInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2serverInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2tasktaskId - errors', () => {
      it('should have a getRestapi2tasktaskId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2tasktaskId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getRestapi2tasktaskId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('taskId is required for getRestapi2tasktaskId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2tasktaskIdcancel - errors', () => {
      it('should have a postRestapi2tasktaskIdcancel function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2tasktaskIdcancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.postRestapi2tasktaskIdcancel(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('taskId is required for postRestapi2tasktaskIdcancel', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configurationtimetracking - errors', () => {
      it('should have a getRestapi2configurationtimetracking function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2configurationtimetracking === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2configurationtimetracking - errors', () => {
      it('should have a putRestapi2configurationtimetracking function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2configurationtimetracking === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2configurationtimetracking(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2configurationtimetracking', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2configurationtimetracking - errors', () => {
      it('should have a deleteRestapi2configurationtimetracking function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2configurationtimetracking === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configurationtimetrackinglist - errors', () => {
      it('should have a getRestapi2configurationtimetrackinglist function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2configurationtimetrackinglist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configurationtimetrackingoptions - errors', () => {
      it('should have a getRestapi2configurationtimetrackingoptions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2configurationtimetrackingoptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2configurationtimetrackingoptions - errors', () => {
      it('should have a putRestapi2configurationtimetrackingoptions function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2configurationtimetrackingoptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2configurationtimetrackingoptions(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2configurationtimetrackingoptions', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userbulk - errors', () => {
      it('should have a getRestapi2userbulk function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userbulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userbulkmigration - errors', () => {
      it('should have a getRestapi2userbulkmigration function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userbulkmigration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2user - errors', () => {
      it('should have a getRestapi2user function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2user === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2user - errors', () => {
      it('should have a postRestapi2user function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2user === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2user(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2user', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2user - errors', () => {
      it('should have a deleteRestapi2user function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2user === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usercolumns - errors', () => {
      it('should have a getRestapi2usercolumns function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2usercolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2usercolumns - errors', () => {
      it('should have a putRestapi2usercolumns function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2usercolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2usercolumns - errors', () => {
      it('should have a deleteRestapi2usercolumns function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2usercolumns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usergroups - errors', () => {
      it('should have a getRestapi2usergroups function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2usergroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userproperties - errors', () => {
      it('should have a getRestapi2userproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userpropertiespropertyKey - errors', () => {
      it('should have a getRestapi2userpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2userpropertiespropertyKey('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2userpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2userpropertiespropertyKey - errors', () => {
      it('should have a putRestapi2userpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2userpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2userpropertiespropertyKey('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2userpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2userpropertiespropertyKey('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2userpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2userpropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2userpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2userpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2userpropertiespropertyKey('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2userpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usersearchquery - errors', () => {
      it('should have a getRestapi2usersearchquery function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2usersearchquery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usersearchquerykey - errors', () => {
      it('should have a getRestapi2usersearchquerykey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2usersearchquerykey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userassignablemultiProjectSearch - errors', () => {
      it('should have a getRestapi2userassignablemultiProjectSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userassignablemultiProjectSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userassignablesearch - errors', () => {
      it('should have a getRestapi2userassignablesearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userassignablesearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userpermissionsearch - errors', () => {
      it('should have a getRestapi2userpermissionsearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userpermissionsearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userpicker - errors', () => {
      it('should have a getRestapi2userpicker function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userpicker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usersearch - errors', () => {
      it('should have a getRestapi2usersearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2usersearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userviewissuesearch - errors', () => {
      it('should have a getRestapi2userviewissuesearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2userviewissuesearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflow - errors', () => {
      it('should have a getRestapi2workflow function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workflowscheme - errors', () => {
      it('should have a postRestapi2workflowscheme function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2workflowscheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2workflowscheme(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2workflowscheme', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeid - errors', () => {
      it('should have a getRestapi2workflowschemeid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeid - errors', () => {
      it('should have a putRestapi2workflowschemeid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeid - errors', () => {
      it('should have a deleteRestapi2workflowschemeid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddefault - errors', () => {
      it('should have a getRestapi2workflowschemeiddefault function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeiddefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeiddefault(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeiddefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddefault - errors', () => {
      it('should have a putRestapi2workflowschemeiddefault function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeiddefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeiddefault(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeiddefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeiddefault('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeiddefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddefault - errors', () => {
      it('should have a deleteRestapi2workflowschemeiddefault function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeiddefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddefault(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeiddefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeidissuetypeissueType - errors', () => {
      it('should have a getRestapi2workflowschemeidissuetypeissueType function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeidissuetypeissueType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeidissuetypeissueType(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueType', (done) => {
        try {
          a.getRestapi2workflowschemeidissuetypeissueType('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueType is required for getRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeidissuetypeissueType - errors', () => {
      it('should have a putRestapi2workflowschemeidissuetypeissueType function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeidissuetypeissueType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeidissuetypeissueType(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueType', (done) => {
        try {
          a.putRestapi2workflowschemeidissuetypeissueType('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueType is required for putRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeidissuetypeissueType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeidissuetypeissueType - errors', () => {
      it('should have a deleteRestapi2workflowschemeidissuetypeissueType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeidissuetypeissueType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeidissuetypeissueType(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueType', (done) => {
        try {
          a.deleteRestapi2workflowschemeidissuetypeissueType('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueType is required for deleteRestapi2workflowschemeidissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeidworkflow - errors', () => {
      it('should have a getRestapi2workflowschemeidworkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeidworkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeidworkflow(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeidworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeidworkflow - errors', () => {
      it('should have a putRestapi2workflowschemeidworkflow function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeidworkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeidworkflow(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeidworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeidworkflow('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeidworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeidworkflow - errors', () => {
      it('should have a deleteRestapi2workflowschemeidworkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeidworkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeidworkflow(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeidworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workflowschemeidcreatedraft - errors', () => {
      it('should have a postRestapi2workflowschemeidcreatedraft function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2workflowschemeidcreatedraft === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2workflowschemeidcreatedraft(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2workflowschemeidcreatedraft', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraft - errors', () => {
      it('should have a getRestapi2workflowschemeiddraft function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeiddraft === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeiddraft(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeiddraft', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraft - errors', () => {
      it('should have a putRestapi2workflowschemeiddraft function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeiddraft === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeiddraft(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeiddraft', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeiddraft('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeiddraft', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraft - errors', () => {
      it('should have a deleteRestapi2workflowschemeiddraft function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeiddraft === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraft(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeiddraft', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraftdefault - errors', () => {
      it('should have a getRestapi2workflowschemeiddraftdefault function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeiddraftdefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftdefault(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeiddraftdefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraftdefault - errors', () => {
      it('should have a putRestapi2workflowschemeiddraftdefault function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeiddraftdefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftdefault(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeiddraftdefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftdefault('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeiddraftdefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraftdefault - errors', () => {
      it('should have a deleteRestapi2workflowschemeiddraftdefault function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeiddraftdefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftdefault(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeiddraftdefault', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraftissuetypeissueType - errors', () => {
      it('should have a getRestapi2workflowschemeiddraftissuetypeissueType function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeiddraftissuetypeissueType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftissuetypeissueType(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueType', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftissuetypeissueType('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueType is required for getRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraftissuetypeissueType - errors', () => {
      it('should have a putRestapi2workflowschemeiddraftissuetypeissueType function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeiddraftissuetypeissueType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftissuetypeissueType(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueType', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftissuetypeissueType('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueType is required for putRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftissuetypeissueType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraftissuetypeissueType - errors', () => {
      it('should have a deleteRestapi2workflowschemeiddraftissuetypeissueType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeiddraftissuetypeissueType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftissuetypeissueType(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueType', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftissuetypeissueType('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueType is required for deleteRestapi2workflowschemeiddraftissuetypeissueType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraftworkflow - errors', () => {
      it('should have a getRestapi2workflowschemeiddraftworkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowschemeiddraftworkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftworkflow(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2workflowschemeiddraftworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraftworkflow - errors', () => {
      it('should have a putRestapi2workflowschemeiddraftworkflow function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowschemeiddraftworkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftworkflow(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2workflowschemeiddraftworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftworkflow('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowschemeiddraftworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraftworkflow - errors', () => {
      it('should have a deleteRestapi2workflowschemeiddraftworkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowschemeiddraftworkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftworkflow(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2workflowschemeiddraftworkflow', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should have a getRestapi2workflowtransitionstransitionIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2workflowtransitionstransitionIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitionId', (done) => {
        try {
          a.getRestapi2workflowtransitionstransitionIdproperties(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('transitionId is required for getRestapi2workflowtransitionstransitionIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should have a putRestapi2workflowtransitionstransitionIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2workflowtransitionstransitionIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitionId', (done) => {
        try {
          a.putRestapi2workflowtransitionstransitionIdproperties(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('transitionId is required for putRestapi2workflowtransitionstransitionIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2workflowtransitionstransitionIdproperties('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2workflowtransitionstransitionIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should have a postRestapi2workflowtransitionstransitionIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2workflowtransitionstransitionIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitionId', (done) => {
        try {
          a.postRestapi2workflowtransitionstransitionIdproperties(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('transitionId is required for postRestapi2workflowtransitionstransitionIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2workflowtransitionstransitionIdproperties('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2workflowtransitionstransitionIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should have a deleteRestapi2workflowtransitionstransitionIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2workflowtransitionstransitionIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitionId', (done) => {
        try {
          a.deleteRestapi2workflowtransitionstransitionIdproperties(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('transitionId is required for deleteRestapi2workflowtransitionstransitionIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentmeta - errors', () => {
      it('should have a getRestapi2attachmentmeta function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2attachmentmeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentid - errors', () => {
      it('should have a getRestapi2attachmentid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2attachmentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2attachmentid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2attachmentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2attachmentid - errors', () => {
      it('should have a deleteRestapi2attachmentid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2attachmentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2attachmentid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2attachmentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentidexpandhuman - errors', () => {
      it('should have a getRestapi2attachmentidexpandhuman function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2attachmentidexpandhuman === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2attachmentidexpandhuman(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2attachmentidexpandhuman', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentidexpandraw - errors', () => {
      it('should have a getRestapi2attachmentidexpandraw function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2attachmentidexpandraw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2attachmentidexpandraw(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2attachmentidexpandraw', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyattachments - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeyattachments function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeyattachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyattachments(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeyattachments', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2avatartypesystem - errors', () => {
      it('should have a getRestapi2avatartypesystem function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2avatartypesystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getRestapi2avatartypesystem(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('type is required for getRestapi2avatartypesystem', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2universalAvatartypetypeownerentityId - errors', () => {
      it('should have a getRestapi2universalAvatartypetypeownerentityId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2universalAvatartypetypeownerentityId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getRestapi2universalAvatartypetypeownerentityId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('type is required for getRestapi2universalAvatartypetypeownerentityId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityId', (done) => {
        try {
          a.getRestapi2universalAvatartypetypeownerentityId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('entityId is required for getRestapi2universalAvatartypetypeownerentityId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2universalAvatartypetypeownerentityId - errors', () => {
      it('should have a postRestapi2universalAvatartypetypeownerentityId function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2universalAvatartypetypeownerentityId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.postRestapi2universalAvatartypetypeownerentityId(null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('type is required for postRestapi2universalAvatartypetypeownerentityId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityId', (done) => {
        try {
          a.postRestapi2universalAvatartypetypeownerentityId('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('entityId is required for postRestapi2universalAvatartypetypeownerentityId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid - errors', () => {
      it('should have a deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('type is required for deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owningObjectId', (done) => {
        try {
          a.deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('owningObjectId is required for deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2commentlist - errors', () => {
      it('should have a postRestapi2commentlist function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2commentlist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2commentlist('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2commentlist', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeycomment - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeycomment function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeycomment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeycomment(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeycomment', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeycomment - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeycomment function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeycomment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeycomment(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeycomment', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeycomment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueissueIdOrKeycomment', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeycommentid - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeycommentid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeycommentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeycommentid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeycommentid('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeycommentid - errors', () => {
      it('should have a putRestapi2issueissueIdOrKeycommentid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKeycommentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeycommentid(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeycommentid('fakeparam', null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeycommentid('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeycommentid - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeycommentid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeycommentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeycommentid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeycommentid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2issueissueIdOrKeycommentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2commentcommentIdproperties - errors', () => {
      it('should have a getRestapi2commentcommentIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2commentcommentIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getRestapi2commentcommentIdproperties(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('commentId is required for getRestapi2commentcommentIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2commentcommentIdpropertiespropertyKey - errors', () => {
      it('should have a getRestapi2commentcommentIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2commentcommentIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getRestapi2commentcommentIdpropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('commentId is required for getRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2commentcommentIdpropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2commentcommentIdpropertiespropertyKey - errors', () => {
      it('should have a putRestapi2commentcommentIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2commentcommentIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.putRestapi2commentcommentIdpropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('commentId is required for putRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2commentcommentIdpropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2commentcommentIdpropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2commentcommentIdpropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2commentcommentIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2commentcommentIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.deleteRestapi2commentcommentIdpropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('commentId is required for deleteRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2commentcommentIdpropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2commentcommentIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2component - errors', () => {
      it('should have a postRestapi2component function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2component === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2component(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2component', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2componentid - errors', () => {
      it('should have a getRestapi2componentid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2componentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2componentid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2componentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2componentid - errors', () => {
      it('should have a putRestapi2componentid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2componentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2componentid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2componentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2componentid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2componentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2componentid - errors', () => {
      it('should have a deleteRestapi2componentid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2componentid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2componentid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2componentid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2componentidrelatedIssueCounts - errors', () => {
      it('should have a getRestapi2componentidrelatedIssueCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2componentidrelatedIssueCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2componentidrelatedIssueCounts(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2componentidrelatedIssueCounts', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeycomponent - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeycomponent function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeycomponent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeycomponent(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeycomponent', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeycomponents - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeycomponents function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeycomponents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeycomponents(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeycomponents', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2customFieldOptionid - errors', () => {
      it('should have a getRestapi2customFieldOptionid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2customFieldOptionid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2customFieldOptionid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2customFieldOptionid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2field - errors', () => {
      it('should have a getRestapi2field function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2field === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2field - errors', () => {
      it('should have a postRestapi2field function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2field === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2field(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2field', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoption - errors', () => {
      it('should have a getRestapi2fieldfieldKeyoption function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2fieldfieldKeyoption === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoption('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for getRestapi2fieldfieldKeyoption', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2fieldfieldKeyoption - errors', () => {
      it('should have a postRestapi2fieldfieldKeyoption function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2fieldfieldKeyoption === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.postRestapi2fieldfieldKeyoption(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for postRestapi2fieldfieldKeyoption', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2fieldfieldKeyoption('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2fieldfieldKeyoption', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoptionsuggestionsedit - errors', () => {
      it('should have a getRestapi2fieldfieldKeyoptionsuggestionsedit function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2fieldfieldKeyoptionsuggestionsedit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionsuggestionsedit('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for getRestapi2fieldfieldKeyoptionsuggestionsedit', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoptionsuggestionssearch - errors', () => {
      it('should have a getRestapi2fieldfieldKeyoptionsuggestionssearch function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2fieldfieldKeyoptionsuggestionssearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionsuggestionssearch('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for getRestapi2fieldfieldKeyoptionsuggestionssearch', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoptionoptionId - errors', () => {
      it('should have a getRestapi2fieldfieldKeyoptionoptionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2fieldfieldKeyoptionoptionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionoptionId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for getRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionoptionId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('optionId is required for getRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2fieldfieldKeyoptionoptionId - errors', () => {
      it('should have a putRestapi2fieldfieldKeyoptionoptionId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2fieldfieldKeyoptionoptionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.putRestapi2fieldfieldKeyoptionoptionId(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for putRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.putRestapi2fieldfieldKeyoptionoptionId('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('optionId is required for putRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2fieldfieldKeyoptionoptionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2fieldfieldKeyoptionoptionId - errors', () => {
      it('should have a deleteRestapi2fieldfieldKeyoptionoptionId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2fieldfieldKeyoptionoptionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.deleteRestapi2fieldfieldKeyoptionoptionId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for deleteRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.deleteRestapi2fieldfieldKeyoptionoptionId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('optionId is required for deleteRestapi2fieldfieldKeyoptionoptionId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2fieldfieldKeyoptionoptionIdissue - errors', () => {
      it('should have a deleteRestapi2fieldfieldKeyoptionoptionIdissue function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2fieldfieldKeyoptionoptionIdissue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldKey', (done) => {
        try {
          a.deleteRestapi2fieldfieldKeyoptionoptionIdissue('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldKey is required for deleteRestapi2fieldfieldKeyoptionoptionIdissue', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.deleteRestapi2fieldfieldKeyoptionoptionIdissue('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('optionId is required for deleteRestapi2fieldfieldKeyoptionoptionIdissue', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2group - errors', () => {
      it('should have a getRestapi2group function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2group === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2group - errors', () => {
      it('should have a postRestapi2group function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2group === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2group(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2group', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2group - errors', () => {
      it('should have a deleteRestapi2group function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2group === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2groupmember - errors', () => {
      it('should have a getRestapi2groupmember function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2groupmember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2groupuser - errors', () => {
      it('should have a postRestapi2groupuser function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2groupuser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2groupuser('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2groupuser', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2groupuser - errors', () => {
      it('should have a deleteRestapi2groupuser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2groupuser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2groupspicker - errors', () => {
      it('should have a getRestapi2groupspicker function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2groupspicker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2groupuserpicker - errors', () => {
      it('should have a getRestapi2groupuserpicker function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2groupuserpicker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issue - errors', () => {
      it('should have a postRestapi2issue function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issue('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issue', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issuebulk - errors', () => {
      it('should have a postRestapi2issuebulk function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issuebulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issuebulk(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issuebulk', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuecreatemeta - errors', () => {
      it('should have a getRestapi2issuecreatemeta function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuecreatemeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKey - errors', () => {
      it('should have a getRestapi2issueissueIdOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKey(null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKey - errors', () => {
      it('should have a putRestapi2issueissueIdOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKey(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKey('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKey - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyassignee - errors', () => {
      it('should have a putRestapi2issueissueIdOrKeyassignee function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKeyassignee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyassignee(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKeyassignee', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyassignee('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKeyassignee', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeychangelog - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeychangelog function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeychangelog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeychangelog(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeychangelog', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyeditmeta - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyeditmeta function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyeditmeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyeditmeta(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyeditmeta', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeynotify - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeynotify function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeynotify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeynotify(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeynotify', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeynotify('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueissueIdOrKeynotify', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeytransitions - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeytransitions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeytransitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeytransitions(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeytransitions', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeytransitions - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeytransitions function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeytransitions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeytransitions(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeytransitions', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeytransitions('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueissueIdOrKeytransitions', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyremotelink - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyremotelink function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyremotelink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyremotelink(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyremotelink', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyremotelink - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeyremotelink function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeyremotelink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyremotelink(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeyremotelink', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyremotelink('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueissueIdOrKeyremotelink', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyremotelink - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeyremotelink function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeyremotelink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyremotelink(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeyremotelink', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyremotelinklinkId - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyremotelinklinkId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyremotelinklinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyremotelinklinkId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyremotelinklinkId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('linkId is required for getRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyremotelinklinkId - errors', () => {
      it('should have a putRestapi2issueissueIdOrKeyremotelinklinkId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKeyremotelinklinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyremotelinklinkId(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyremotelinklinkId('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('linkId is required for putRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyremotelinklinkId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyremotelinklinkId - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeyremotelinklinkId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeyremotelinklinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyremotelinklinkId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyremotelinklinkId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('linkId is required for deleteRestapi2issueissueIdOrKeyremotelinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyvotes - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyvotes function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyvotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyvotes(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyvotes', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyvotes - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeyvotes function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeyvotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyvotes(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeyvotes', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyvotes - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeyvotes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeyvotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyvotes(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeyvotes', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeywatchers - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeywatchers function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeywatchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeywatchers(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeywatchers', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeywatchers - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeywatchers function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeywatchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeywatchers(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeywatchers', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeywatchers('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueissueIdOrKeywatchers', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeywatchers - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeywatchers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeywatchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeywatchers(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeywatchers', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklog - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyworklog function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyworklog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklog(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyworklog', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyworklog - errors', () => {
      it('should have a postRestapi2issueissueIdOrKeyworklog function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueissueIdOrKeyworklog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyworklog(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for postRestapi2issueissueIdOrKeyworklog', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyworklog('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueissueIdOrKeyworklog', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklogid - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyworklogid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyworklogid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogid('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyworklogid - errors', () => {
      it('should have a putRestapi2issueissueIdOrKeyworklogid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKeyworklogid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogid(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogid('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogid('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyworklogid - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeyworklogid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeyworklogid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogid(null, null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogid('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2issueissueIdOrKeyworklogid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2worklogdeleted - errors', () => {
      it('should have a getRestapi2worklogdeleted function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2worklogdeleted === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workloglist - errors', () => {
      it('should have a postRestapi2workloglist function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2workloglist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2workloglist('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2workloglist', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2worklogupdated - errors', () => {
      it('should have a getRestapi2worklogupdated function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2worklogupdated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklogworklogIdproperties - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyworklogworklogIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyworklogworklogIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdproperties(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyworklogworklogIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing worklogId', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdproperties('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('worklogId is required for getRestapi2issueissueIdOrKeyworklogworklogIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey - errors', () => {
      it('should have a getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing worklogId', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('worklogId is required for getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey - errors', () => {
      it('should have a putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing worklogId', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('worklogId is required for putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueIdOrKey is required for deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing worklogId', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('worklogId is required for deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueLink - errors', () => {
      it('should have a postRestapi2issueLink function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueLink(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueLink', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueLinklinkId - errors', () => {
      it('should have a getRestapi2issueLinklinkId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueLinklinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getRestapi2issueLinklinkId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('linkId is required for getRestapi2issueLinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueLinklinkId - errors', () => {
      it('should have a deleteRestapi2issueLinklinkId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueLinklinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.deleteRestapi2issueLinklinkId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('linkId is required for deleteRestapi2issueLinklinkId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueLinkType - errors', () => {
      it('should have a getRestapi2issueLinkType function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueLinkType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueLinkType - errors', () => {
      it('should have a postRestapi2issueLinkType function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issueLinkType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issueLinkType(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issueLinkType', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueLinkTypeissueLinkTypeId - errors', () => {
      it('should have a getRestapi2issueLinkTypeissueLinkTypeId function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issueLinkTypeissueLinkTypeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueLinkTypeId', (done) => {
        try {
          a.getRestapi2issueLinkTypeissueLinkTypeId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueLinkTypeId is required for getRestapi2issueLinkTypeissueLinkTypeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueLinkTypeissueLinkTypeId - errors', () => {
      it('should have a putRestapi2issueLinkTypeissueLinkTypeId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issueLinkTypeissueLinkTypeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueLinkTypeId', (done) => {
        try {
          a.putRestapi2issueLinkTypeissueLinkTypeId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueLinkTypeId is required for putRestapi2issueLinkTypeissueLinkTypeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issueLinkTypeissueLinkTypeId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issueLinkTypeissueLinkTypeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueLinkTypeissueLinkTypeId - errors', () => {
      it('should have a deleteRestapi2issueLinkTypeissueLinkTypeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issueLinkTypeissueLinkTypeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueLinkTypeId', (done) => {
        try {
          a.deleteRestapi2issueLinkTypeissueLinkTypeId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueLinkTypeId is required for deleteRestapi2issueLinkTypeissueLinkTypeId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuesecurityschemes - errors', () => {
      it('should have a getRestapi2issuesecurityschemes function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuesecurityschemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuesecurityschemesid - errors', () => {
      it('should have a getRestapi2issuesecurityschemesid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuesecurityschemesid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2issuesecurityschemesid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2issuesecurityschemesid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetype - errors', () => {
      it('should have a getRestapi2issuetype function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuetype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issuetype - errors', () => {
      it('should have a postRestapi2issuetype function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issuetype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2issuetype(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2issuetype', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetypeid - errors', () => {
      it('should have a getRestapi2issuetypeid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuetypeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2issuetypeid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2issuetypeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issuetypeid - errors', () => {
      it('should have a putRestapi2issuetypeid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issuetypeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2issuetypeid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2issuetypeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issuetypeid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issuetypeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issuetypeid - errors', () => {
      it('should have a deleteRestapi2issuetypeid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issuetypeid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2issuetypeid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2issuetypeid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetypeidalternatives - errors', () => {
      it('should have a getRestapi2issuetypeidalternatives function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuetypeidalternatives === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2issuetypeidalternatives(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2issuetypeidalternatives', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issuetypeidavatar2 - errors', () => {
      it('should have a postRestapi2issuetypeidavatar2 function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2issuetypeidavatar2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2issuetypeidavatar2(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2issuetypeidavatar2', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetypeissueTypeIdproperties - errors', () => {
      it('should have a getRestapi2issuetypeissueTypeIdproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuetypeissueTypeIdproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeId', (done) => {
        try {
          a.getRestapi2issuetypeissueTypeIdproperties(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueTypeId is required for getRestapi2issuetypeissueTypeIdproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetypeissueTypeIdpropertiespropertyKey - errors', () => {
      it('should have a getRestapi2issuetypeissueTypeIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2issuetypeissueTypeIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeId', (done) => {
        try {
          a.getRestapi2issuetypeissueTypeIdpropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueTypeId is required for getRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issuetypeissueTypeIdpropertiespropertyKey - errors', () => {
      it('should have a putRestapi2issuetypeissueTypeIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2issuetypeissueTypeIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeId', (done) => {
        try {
          a.putRestapi2issuetypeissueTypeIdpropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueTypeId is required for putRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeId', (done) => {
        try {
          a.deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('issueTypeId is required for deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2priority - errors', () => {
      it('should have a getRestapi2priority function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2priority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2priorityid - errors', () => {
      it('should have a getRestapi2priorityid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2priorityid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2priorityid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2priorityid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeyavatar - errors', () => {
      it('should have a putRestapi2projectprojectIdOrKeyavatar function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectprojectIdOrKeyavatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyavatar(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for putRestapi2projectprojectIdOrKeyavatar', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyavatar('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2projectprojectIdOrKeyavatar', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectprojectIdOrKeyavatarid - errors', () => {
      it('should have a deleteRestapi2projectprojectIdOrKeyavatarid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2projectprojectIdOrKeyavatarid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeyavatarid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for deleteRestapi2projectprojectIdOrKeyavatarid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeyavatarid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2projectprojectIdOrKeyavatarid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2projectprojectIdOrKeyavatar2 - errors', () => {
      it('should have a postRestapi2projectprojectIdOrKeyavatar2 function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2projectprojectIdOrKeyavatar2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.postRestapi2projectprojectIdOrKeyavatar2(null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for postRestapi2projectprojectIdOrKeyavatar2', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyavatars - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyavatars function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyavatars === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyavatars(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyavatars', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyproperties - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyproperties function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyproperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyproperties(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyproperties', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeypropertiespropertyKey - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeypropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeypropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeypropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeypropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for getRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeypropertiespropertyKey - errors', () => {
      it('should have a putRestapi2projectprojectIdOrKeypropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectprojectIdOrKeypropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeypropertiespropertyKey(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for putRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeypropertiespropertyKey('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for putRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeypropertiespropertyKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectprojectIdOrKeypropertiespropertyKey - errors', () => {
      it('should have a deleteRestapi2projectprojectIdOrKeypropertiespropertyKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2projectprojectIdOrKeypropertiespropertyKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeypropertiespropertyKey(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for deleteRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeypropertiespropertyKey('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('propertyKey is required for deleteRestapi2projectprojectIdOrKeypropertiespropertyKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyrole - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyrole function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyrole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyrole(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyrole', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyroleid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyroleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyroleid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyroleid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyroledetails - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyroledetails function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyroledetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyroledetails(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyroledetails', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2role - errors', () => {
      it('should have a getRestapi2role function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2role === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2role - errors', () => {
      it('should have a postRestapi2role function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2role === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2role(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2role', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2roleid - errors', () => {
      it('should have a getRestapi2roleid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2roleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2roleid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2roleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2roleid - errors', () => {
      it('should have a putRestapi2roleid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2roleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2roleid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2roleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2roleid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2roleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2roleid - errors', () => {
      it('should have a postRestapi2roleid function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2roleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2roleid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2roleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2roleid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2roleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2roleid - errors', () => {
      it('should have a deleteRestapi2roleid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2roleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2roleid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2roleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should have a putRestapi2projectprojectIdOrKeyroleid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectprojectIdOrKeyroleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyroleid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for putRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyroleid('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyroleid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should have a postRestapi2projectprojectIdOrKeyroleid function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2projectprojectIdOrKeyroleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.postRestapi2projectprojectIdOrKeyroleid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for postRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2projectprojectIdOrKeyroleid('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2projectprojectIdOrKeyroleid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should have a deleteRestapi2projectprojectIdOrKeyroleid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2projectprojectIdOrKeyroleid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeyroleid(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for deleteRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeyroleid('fakeparam', null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2projectprojectIdOrKeyroleid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2roleidactors - errors', () => {
      it('should have a getRestapi2roleidactors function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2roleidactors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2roleidactors(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2roleidactors', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2roleidactors - errors', () => {
      it('should have a postRestapi2roleidactors function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2roleidactors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2roleidactors(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2roleidactors', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2roleidactors('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2roleidactors', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2roleidactors - errors', () => {
      it('should have a deleteRestapi2roleidactors function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2roleidactors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2roleidactors(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2roleidactors', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyversion - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyversion function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyversion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyversion(null, null, null, null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyversion', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyversions - errors', () => {
      it('should have a getRestapi2projectprojectIdOrKeyversions function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectprojectIdOrKeyversions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyversions(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('projectIdOrKey is required for getRestapi2projectprojectIdOrKeyversions', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2version - errors', () => {
      it('should have a postRestapi2version function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2version === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2version(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2version', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2versionid - errors', () => {
      it('should have a getRestapi2versionid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2versionid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2versionid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2versionid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2versionid - errors', () => {
      it('should have a putRestapi2versionid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2versionid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2versionid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2versionid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2versionid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2versionid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2versionid - errors', () => {
      it('should have a deleteRestapi2versionid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2versionid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2versionid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2versionid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2versionidmergetomoveIssuesTo - errors', () => {
      it('should have a putRestapi2versionidmergetomoveIssuesTo function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2versionidmergetomoveIssuesTo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2versionidmergetomoveIssuesTo(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2versionidmergetomoveIssuesTo', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing moveIssuesTo', (done) => {
        try {
          a.putRestapi2versionidmergetomoveIssuesTo('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('moveIssuesTo is required for putRestapi2versionidmergetomoveIssuesTo', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2versionidmove - errors', () => {
      it('should have a postRestapi2versionidmove function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2versionidmove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2versionidmove(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2versionidmove', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2versionidmove('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2versionidmove', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2versionidrelatedIssueCounts - errors', () => {
      it('should have a getRestapi2versionidrelatedIssueCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2versionidrelatedIssueCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2versionidrelatedIssueCounts(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2versionidrelatedIssueCounts', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2versionidremoveAndSwap - errors', () => {
      it('should have a postRestapi2versionidremoveAndSwap function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2versionidremoveAndSwap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2versionidremoveAndSwap(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2versionidremoveAndSwap', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2versionidremoveAndSwap('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2versionidremoveAndSwap', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2versionidunresolvedIssueCount - errors', () => {
      it('should have a getRestapi2versionidunresolvedIssueCount function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2versionidunresolvedIssueCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2versionidunresolvedIssueCount(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2versionidunresolvedIssueCount', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectCategory - errors', () => {
      it('should have a getRestapi2projectCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2projectCategory - errors', () => {
      it('should have a postRestapi2projectCategory function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2projectCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2projectCategory(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2projectCategory', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectCategoryid - errors', () => {
      it('should have a getRestapi2projectCategoryid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectCategoryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2projectCategoryid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2projectCategoryid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectCategoryid - errors', () => {
      it('should have a putRestapi2projectCategoryid function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2projectCategoryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRestapi2projectCategoryid(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for putRestapi2projectCategoryid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2projectCategoryid('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2projectCategoryid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectCategoryid - errors', () => {
      it('should have a deleteRestapi2projectCategoryid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2projectCategoryid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2projectCategoryid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2projectCategoryid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectvalidatekey - errors', () => {
      it('should have a getRestapi2projectvalidatekey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectvalidatekey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectvalidatevalidProjectKey - errors', () => {
      it('should have a getRestapi2projectvalidatevalidProjectKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectvalidatevalidProjectKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectvalidatevalidProjectName - errors', () => {
      it('should have a getRestapi2projectvalidatevalidProjectName function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2projectvalidatevalidProjectName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2resolution - errors', () => {
      it('should have a getRestapi2resolution function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2resolution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2resolutionid - errors', () => {
      it('should have a getRestapi2resolutionid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2resolutionid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2resolutionid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2resolutionid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screens - errors', () => {
      it('should have a getRestapi2screens function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2screens === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensaddToDefaultfieldId - errors', () => {
      it('should have a postRestapi2screensaddToDefaultfieldId function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2screensaddToDefaultfieldId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.postRestapi2screensaddToDefaultfieldId(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('fieldId is required for postRestapi2screensaddToDefaultfieldId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screensscreenIdavailableFields - errors', () => {
      it('should have a getRestapi2screensscreenIdavailableFields function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2screensscreenIdavailableFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.getRestapi2screensscreenIdavailableFields(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for getRestapi2screensscreenIdavailableFields', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screensscreenIdtabs - errors', () => {
      it('should have a getRestapi2screensscreenIdtabs function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2screensscreenIdtabs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.getRestapi2screensscreenIdtabs(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for getRestapi2screensscreenIdtabs', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabs - errors', () => {
      it('should have a postRestapi2screensscreenIdtabs function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2screensscreenIdtabs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabs(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for postRestapi2screensscreenIdtabs', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2screensscreenIdtabs('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2screensscreenIdtabs', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2screensscreenIdtabstabId - errors', () => {
      it('should have a putRestapi2screensscreenIdtabstabId function', (done) => {
        try {
          assert.equal(true, typeof a.putRestapi2screensscreenIdtabstabId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.putRestapi2screensscreenIdtabstabId(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for putRestapi2screensscreenIdtabstabId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.putRestapi2screensscreenIdtabstabId('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for putRestapi2screensscreenIdtabstabId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRestapi2screensscreenIdtabstabId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for putRestapi2screensscreenIdtabstabId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2screensscreenIdtabstabId - errors', () => {
      it('should have a deleteRestapi2screensscreenIdtabstabId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2screensscreenIdtabstabId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabId(null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for deleteRestapi2screensscreenIdtabstabId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabId('fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for deleteRestapi2screensscreenIdtabstabId', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screensscreenIdtabstabIdfields - errors', () => {
      it('should have a getRestapi2screensscreenIdtabstabIdfields function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2screensscreenIdtabstabIdfields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.getRestapi2screensscreenIdtabstabIdfields(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for getRestapi2screensscreenIdtabstabIdfields', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.getRestapi2screensscreenIdtabstabIdfields('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for getRestapi2screensscreenIdtabstabIdfields', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabstabIdfields - errors', () => {
      it('should have a postRestapi2screensscreenIdtabstabIdfields function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2screensscreenIdtabstabIdfields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfields(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for postRestapi2screensscreenIdtabstabIdfields', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfields('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for postRestapi2screensscreenIdtabstabIdfields', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfields('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2screensscreenIdtabstabIdfields', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2screensscreenIdtabstabIdfieldsid - errors', () => {
      it('should have a deleteRestapi2screensscreenIdtabstabIdfieldsid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRestapi2screensscreenIdtabstabIdfieldsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabIdfieldsid(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for deleteRestapi2screensscreenIdtabstabIdfieldsid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabIdfieldsid('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for deleteRestapi2screensscreenIdtabstabIdfieldsid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabIdfieldsid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for deleteRestapi2screensscreenIdtabstabIdfieldsid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabstabIdfieldsidmove - errors', () => {
      it('should have a postRestapi2screensscreenIdtabstabIdfieldsidmove function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2screensscreenIdtabstabIdfieldsidmove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfieldsidmove(null, null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for postRestapi2screensscreenIdtabstabIdfieldsidmove', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfieldsidmove('fakeparam', null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for postRestapi2screensscreenIdtabstabIdfieldsidmove', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfieldsidmove('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for postRestapi2screensscreenIdtabstabIdfieldsidmove', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfieldsidmove('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('body is required for postRestapi2screensscreenIdtabstabIdfieldsidmove', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabstabIdmovepos - errors', () => {
      it('should have a postRestapi2screensscreenIdtabstabIdmovepos function', (done) => {
        try {
          assert.equal(true, typeof a.postRestapi2screensscreenIdtabstabIdmovepos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdmovepos(null, null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('screenId is required for postRestapi2screensscreenIdtabstabIdmovepos', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tabId', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdmovepos('fakeparam', null, null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('tabId is required for postRestapi2screensscreenIdtabstabIdmovepos', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pos', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdmovepos('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('pos is required for postRestapi2screensscreenIdtabstabIdmovepos', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2securitylevelid - errors', () => {
      it('should have a getRestapi2securitylevelid function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2securitylevelid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRestapi2securitylevelid(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('id is required for getRestapi2securitylevelid', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2status - errors', () => {
      it('should have a getRestapi2status function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2status === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2statusidOrName - errors', () => {
      it('should have a getRestapi2statusidOrName function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2statusidOrName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idOrName', (done) => {
        try {
          a.getRestapi2statusidOrName(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('idOrName is required for getRestapi2statusidOrName', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2statuscategory - errors', () => {
      it('should have a getRestapi2statuscategory function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2statuscategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2statuscategoryidOrKey - errors', () => {
      it('should have a getRestapi2statuscategoryidOrKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRestapi2statuscategoryidOrKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idOrKey', (done) => {
        try {
          a.getRestapi2statuscategoryidOrKey(null, (data, error) => {
            try {
              assert.notEqual(undefined, error);
              assert.notEqual(null, error);
              assert.equal(null, data);
              assert.equal('idOrKey is required for getRestapi2statuscategoryidOrKey', error);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMultipleCustomFieldValues - errors', () => {
      it('should have a updateMultipleCustomFieldValues function', (done) => {
        try {
          assert.equal(true, typeof a.updateMultipleCustomFieldValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMultipleCustomFieldValues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateMultipleCustomFieldValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomFieldValue - errors', () => {
      it('should have a updateCustomFieldValue function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomFieldValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldIdOrKey', (done) => {
        try {
          a.updateCustomFieldValue(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCustomFieldValue('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomFieldConfiguration - errors', () => {
      it('should have a getCustomFieldConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomFieldConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldIdOrKey', (done) => {
        try {
          a.getCustomFieldConfiguration(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getCustomFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomFieldConfiguration - errors', () => {
      it('should have a updateCustomFieldConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomFieldConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldIdOrKey', (done) => {
        try {
          a.updateCustomFieldConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'fieldIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCustomFieldConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentContent - errors', () => {
      it('should have a getAttachmentContent function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachmentContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAttachmentContent(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAttachmentContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentThumbnail - errors', () => {
      it('should have a getAttachmentThumbnail function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachmentThumbnail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAttachmentThumbnail(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAttachmentThumbnail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDashboard - errors', () => {
      it('should have a createDashboard function', (done) => {
        try {
          assert.equal(true, typeof a.createDashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDashboard(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createDashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDashboard - errors', () => {
      it('should have a updateDashboard function', (done) => {
        try {
          assert.equal(true, typeof a.updateDashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateDashboard(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateDashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDashboard('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateDashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDashboard - errors', () => {
      it('should have a deleteDashboard function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDashboard(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteDashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#copyDashboard - errors', () => {
      it('should have a copyDashboard function', (done) => {
        try {
          assert.equal(true, typeof a.copyDashboard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.copyDashboard(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-copyDashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.copyDashboard('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-copyDashboard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEvents - errors', () => {
      it('should have a getEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChangeLogsByIds - errors', () => {
      it('should have a getChangeLogsByIds function', (done) => {
        try {
          assert.equal(true, typeof a.getChangeLogsByIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueIdOrKey', (done) => {
        try {
          a.getChangeLogsByIds(null, null, (data, error) => {
            try {
              const displayE = 'issueIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getChangeLogsByIds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getChangeLogsByIds('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getChangeLogsByIds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#analyseExpression - errors', () => {
      it('should have a analyseExpression function', (done) => {
        try {
          assert.equal(true, typeof a.analyseExpression === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.analyseExpression('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-analyseExpression', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldsPaginated - errors', () => {
      it('should have a getFieldsPaginated function', (done) => {
        try {
          assert.equal(true, typeof a.getFieldsPaginated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomField - errors', () => {
      it('should have a updateCustomField function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.updateCustomField(null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCustomField('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContextsForFieldDeprecated - errors', () => {
      it('should have a getContextsForFieldDeprecated function', (done) => {
        try {
          assert.equal(true, typeof a.getContextsForFieldDeprecated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getContextsForFieldDeprecated(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getContextsForFieldDeprecated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomField - errors', () => {
      it('should have a deleteCustomField function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCustomField(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteCustomField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreCustomField - errors', () => {
      it('should have a restoreCustomField function', (done) => {
        try {
          assert.equal(true, typeof a.restoreCustomField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.restoreCustomField(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-restoreCustomField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trashCustomField - errors', () => {
      it('should have a trashCustomField function', (done) => {
        try {
          assert.equal(true, typeof a.trashCustomField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.trashCustomField(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-trashCustomField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContextsForField - errors', () => {
      it('should have a getContextsForField function', (done) => {
        try {
          assert.equal(true, typeof a.getContextsForField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getContextsForField(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getContextsForField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomFieldContext - errors', () => {
      it('should have a createCustomFieldContext function', (done) => {
        try {
          assert.equal(true, typeof a.createCustomFieldContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.createCustomFieldContext(null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCustomFieldContext('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultValues - errors', () => {
      it('should have a getDefaultValues function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getDefaultValues(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getDefaultValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultValues - errors', () => {
      it('should have a setDefaultValues function', (done) => {
        try {
          assert.equal(true, typeof a.setDefaultValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.setDefaultValues(null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setDefaultValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setDefaultValues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setDefaultValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeMappingsForContexts - errors', () => {
      it('should have a getIssueTypeMappingsForContexts function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypeMappingsForContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getIssueTypeMappingsForContexts(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getIssueTypeMappingsForContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomFieldContextsForProjectsAndIssueTypes - errors', () => {
      it('should have a getCustomFieldContextsForProjectsAndIssueTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomFieldContextsForProjectsAndIssueTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getCustomFieldContextsForProjectsAndIssueTypes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getCustomFieldContextsForProjectsAndIssueTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getCustomFieldContextsForProjectsAndIssueTypes('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getCustomFieldContextsForProjectsAndIssueTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectContextMapping - errors', () => {
      it('should have a getProjectContextMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getProjectContextMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getProjectContextMapping(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getProjectContextMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomFieldContext - errors', () => {
      it('should have a updateCustomFieldContext function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomFieldContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.updateCustomFieldContext(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.updateCustomFieldContext('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCustomFieldContext('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomFieldContext - errors', () => {
      it('should have a deleteCustomFieldContext function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomFieldContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.deleteCustomFieldContext(null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.deleteCustomFieldContext('fakeparam', null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIssueTypesToContext - errors', () => {
      it('should have a addIssueTypesToContext function', (done) => {
        try {
          assert.equal(true, typeof a.addIssueTypesToContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.addIssueTypesToContext(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addIssueTypesToContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.addIssueTypesToContext('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addIssueTypesToContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIssueTypesToContext('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addIssueTypesToContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIssueTypesFromContext - errors', () => {
      it('should have a removeIssueTypesFromContext function', (done) => {
        try {
          assert.equal(true, typeof a.removeIssueTypesFromContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.removeIssueTypesFromContext(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypesFromContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.removeIssueTypesFromContext('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypesFromContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeIssueTypesFromContext('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypesFromContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignProjectsToCustomFieldContext - errors', () => {
      it('should have a assignProjectsToCustomFieldContext function', (done) => {
        try {
          assert.equal(true, typeof a.assignProjectsToCustomFieldContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.assignProjectsToCustomFieldContext(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignProjectsToCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.assignProjectsToCustomFieldContext('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignProjectsToCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignProjectsToCustomFieldContext('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignProjectsToCustomFieldContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeCustomFieldContextFromProjects - errors', () => {
      it('should have a removeCustomFieldContextFromProjects function', (done) => {
        try {
          assert.equal(true, typeof a.removeCustomFieldContextFromProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.removeCustomFieldContextFromProjects(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeCustomFieldContextFromProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.removeCustomFieldContextFromProjects('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeCustomFieldContextFromProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeCustomFieldContextFromProjects('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeCustomFieldContextFromProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOptionsForContext - errors', () => {
      it('should have a getOptionsForContext function', (done) => {
        try {
          assert.equal(true, typeof a.getOptionsForContext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getOptionsForContext(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getOptionsForContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.getOptionsForContext('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getOptionsForContext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomFieldOption - errors', () => {
      it('should have a updateCustomFieldOption function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomFieldOption === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.updateCustomFieldOption(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.updateCustomFieldOption('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCustomFieldOption('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomFieldOption - errors', () => {
      it('should have a createCustomFieldOption function', (done) => {
        try {
          assert.equal(true, typeof a.createCustomFieldOption === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.createCustomFieldOption(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.createCustomFieldOption('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCustomFieldOption('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reorderCustomFieldOptions - errors', () => {
      it('should have a reorderCustomFieldOptions function', (done) => {
        try {
          assert.equal(true, typeof a.reorderCustomFieldOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.reorderCustomFieldOptions(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-reorderCustomFieldOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.reorderCustomFieldOptions('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-reorderCustomFieldOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.reorderCustomFieldOptions('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-reorderCustomFieldOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomFieldOption - errors', () => {
      it('should have a deleteCustomFieldOption function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomFieldOption === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.deleteCustomFieldOption(null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing contextId', (done) => {
        try {
          a.deleteCustomFieldOption('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'contextId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.deleteCustomFieldOption('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'optionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteCustomFieldOption', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScreensForField - errors', () => {
      it('should have a getScreensForField function', (done) => {
        try {
          assert.equal(true, typeof a.getScreensForField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getScreensForField(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getScreensForField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createScreen - errors', () => {
      it('should have a createScreen function', (done) => {
        try {
          assert.equal(true, typeof a.createScreen === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createScreen(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createScreen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateScreen - errors', () => {
      it('should have a updateScreen function', (done) => {
        try {
          assert.equal(true, typeof a.updateScreen === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.updateScreen(null, null, (data, error) => {
            try {
              const displayE = 'screenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateScreen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateScreen('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateScreen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScreen - errors', () => {
      it('should have a deleteScreen function', (done) => {
        try {
          assert.equal(true, typeof a.deleteScreen === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenId', (done) => {
        try {
          a.deleteScreen(null, (data, error) => {
            try {
              const displayE = 'screenId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteScreen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFieldConfigurations - errors', () => {
      it('should have a getAllFieldConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFieldConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFieldConfiguration - errors', () => {
      it('should have a createFieldConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createFieldConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFieldConfiguration(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFieldConfiguration - errors', () => {
      it('should have a updateFieldConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateFieldConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFieldConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFieldConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFieldConfiguration - errors', () => {
      it('should have a deleteFieldConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFieldConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteFieldConfiguration(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteFieldConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldConfigurationItems - errors', () => {
      it('should have a getFieldConfigurationItems function', (done) => {
        try {
          assert.equal(true, typeof a.getFieldConfigurationItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getFieldConfigurationItems(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getFieldConfigurationItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFieldConfigurationItems - errors', () => {
      it('should have a updateFieldConfigurationItems function', (done) => {
        try {
          assert.equal(true, typeof a.updateFieldConfigurationItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFieldConfigurationItems(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateFieldConfigurationItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFieldConfigurationItems('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateFieldConfigurationItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFieldConfigurationSchemes - errors', () => {
      it('should have a getAllFieldConfigurationSchemes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFieldConfigurationSchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFieldConfigurationScheme - errors', () => {
      it('should have a createFieldConfigurationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.createFieldConfigurationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFieldConfigurationScheme(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createFieldConfigurationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldConfigurationSchemeMappings - errors', () => {
      it('should have a getFieldConfigurationSchemeMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getFieldConfigurationSchemeMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldConfigurationSchemeProjectMapping - errors', () => {
      it('should have a getFieldConfigurationSchemeProjectMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getFieldConfigurationSchemeProjectMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getFieldConfigurationSchemeProjectMapping('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getFieldConfigurationSchemeProjectMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignFieldConfigurationSchemeToProject - errors', () => {
      it('should have a assignFieldConfigurationSchemeToProject function', (done) => {
        try {
          assert.equal(true, typeof a.assignFieldConfigurationSchemeToProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignFieldConfigurationSchemeToProject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignFieldConfigurationSchemeToProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFieldConfigurationScheme - errors', () => {
      it('should have a updateFieldConfigurationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateFieldConfigurationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateFieldConfigurationScheme(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateFieldConfigurationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFieldConfigurationScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateFieldConfigurationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFieldConfigurationScheme - errors', () => {
      it('should have a deleteFieldConfigurationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFieldConfigurationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteFieldConfigurationScheme(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteFieldConfigurationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setFieldConfigurationSchemeMapping - errors', () => {
      it('should have a setFieldConfigurationSchemeMapping function', (done) => {
        try {
          assert.equal(true, typeof a.setFieldConfigurationSchemeMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.setFieldConfigurationSchemeMapping(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setFieldConfigurationSchemeMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setFieldConfigurationSchemeMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setFieldConfigurationSchemeMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIssueTypesFromGlobalFieldConfigurationScheme - errors', () => {
      it('should have a removeIssueTypesFromGlobalFieldConfigurationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.removeIssueTypesFromGlobalFieldConfigurationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.removeIssueTypesFromGlobalFieldConfigurationScheme(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypesFromGlobalFieldConfigurationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeIssueTypesFromGlobalFieldConfigurationScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypesFromGlobalFieldConfigurationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkGetGroups - errors', () => {
      it('should have a bulkGetGroups function', (done) => {
        try {
          assert.equal(true, typeof a.bulkGetGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicense - errors', () => {
      it('should have a getLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkSetIssuesPropertiesList - errors', () => {
      it('should have a bulkSetIssuesPropertiesList function', (done) => {
        try {
          assert.equal(true, typeof a.bulkSetIssuesPropertiesList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.bulkSetIssuesPropertiesList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-bulkSetIssuesPropertiesList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkSetIssuePropertiesByIssue - errors', () => {
      it('should have a bulkSetIssuePropertiesByIssue function', (done) => {
        try {
          assert.equal(true, typeof a.bulkSetIssuePropertiesByIssue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.bulkSetIssuePropertiesByIssue(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-bulkSetIssuePropertiesByIssue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIsWatchingIssueBulk - errors', () => {
      it('should have a getIsWatchingIssueBulk function', (done) => {
        try {
          assert.equal(true, typeof a.getIsWatchingIssueBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getIsWatchingIssueBulk(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getIsWatchingIssueBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueSecurityLevelMembers - errors', () => {
      it('should have a getIssueSecurityLevelMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueSecurityLevelMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueSecuritySchemeId', (done) => {
        try {
          a.getIssueSecurityLevelMembers(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'issueSecuritySchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getIssueSecurityLevelMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypesForProject - errors', () => {
      it('should have a getIssueTypesForProject function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypesForProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getIssueTypesForProject(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getIssueTypesForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIssueTypeSchemes - errors', () => {
      it('should have a getAllIssueTypeSchemes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIssueTypeSchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIssueTypeScheme - errors', () => {
      it('should have a createIssueTypeScheme function', (done) => {
        try {
          assert.equal(true, typeof a.createIssueTypeScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIssueTypeScheme(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeSchemesMapping - errors', () => {
      it('should have a getIssueTypeSchemesMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypeSchemesMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeSchemeForProjects - errors', () => {
      it('should have a getIssueTypeSchemeForProjects function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypeSchemeForProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getIssueTypeSchemeForProjects('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getIssueTypeSchemeForProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignIssueTypeSchemeToProject - errors', () => {
      it('should have a assignIssueTypeSchemeToProject function', (done) => {
        try {
          assert.equal(true, typeof a.assignIssueTypeSchemeToProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignIssueTypeSchemeToProject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignIssueTypeSchemeToProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIssueTypeScheme - errors', () => {
      it('should have a updateIssueTypeScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateIssueTypeScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeSchemeId', (done) => {
        try {
          a.updateIssueTypeScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIssueTypeScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIssueTypeScheme - errors', () => {
      it('should have a deleteIssueTypeScheme function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIssueTypeScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeSchemeId', (done) => {
        try {
          a.deleteIssueTypeScheme(null, (data, error) => {
            try {
              const displayE = 'issueTypeSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIssueTypesToIssueTypeScheme - errors', () => {
      it('should have a addIssueTypesToIssueTypeScheme function', (done) => {
        try {
          assert.equal(true, typeof a.addIssueTypesToIssueTypeScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeSchemeId', (done) => {
        try {
          a.addIssueTypesToIssueTypeScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addIssueTypesToIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIssueTypesToIssueTypeScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addIssueTypesToIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reorderIssueTypesInIssueTypeScheme - errors', () => {
      it('should have a reorderIssueTypesInIssueTypeScheme function', (done) => {
        try {
          assert.equal(true, typeof a.reorderIssueTypesInIssueTypeScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeSchemeId', (done) => {
        try {
          a.reorderIssueTypesInIssueTypeScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-reorderIssueTypesInIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.reorderIssueTypesInIssueTypeScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-reorderIssueTypesInIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIssueTypeFromIssueTypeScheme - errors', () => {
      it('should have a removeIssueTypeFromIssueTypeScheme function', (done) => {
        try {
          assert.equal(true, typeof a.removeIssueTypeFromIssueTypeScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeSchemeId', (done) => {
        try {
          a.removeIssueTypeFromIssueTypeScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypeFromIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeId', (done) => {
        try {
          a.removeIssueTypeFromIssueTypeScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeIssueTypeFromIssueTypeScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeScreenSchemes - errors', () => {
      it('should have a getIssueTypeScreenSchemes function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypeScreenSchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIssueTypeScreenScheme - errors', () => {
      it('should have a createIssueTypeScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.createIssueTypeScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIssueTypeScreenScheme(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeScreenSchemeMappings - errors', () => {
      it('should have a getIssueTypeScreenSchemeMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypeScreenSchemeMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeScreenSchemeProjectAssociations - errors', () => {
      it('should have a getIssueTypeScreenSchemeProjectAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getIssueTypeScreenSchemeProjectAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getIssueTypeScreenSchemeProjectAssociations('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getIssueTypeScreenSchemeProjectAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignIssueTypeScreenSchemeToProject - errors', () => {
      it('should have a assignIssueTypeScreenSchemeToProject function', (done) => {
        try {
          assert.equal(true, typeof a.assignIssueTypeScreenSchemeToProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignIssueTypeScreenSchemeToProject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignIssueTypeScreenSchemeToProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIssueTypeScreenScheme - errors', () => {
      it('should have a updateIssueTypeScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateIssueTypeScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeScreenSchemeId', (done) => {
        try {
          a.updateIssueTypeScreenScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeScreenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIssueTypeScreenScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIssueTypeScreenScheme - errors', () => {
      it('should have a deleteIssueTypeScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIssueTypeScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeScreenSchemeId', (done) => {
        try {
          a.deleteIssueTypeScreenScheme(null, (data, error) => {
            try {
              const displayE = 'issueTypeScreenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appendMappingsForIssueTypeScreenScheme - errors', () => {
      it('should have a appendMappingsForIssueTypeScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.appendMappingsForIssueTypeScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeScreenSchemeId', (done) => {
        try {
          a.appendMappingsForIssueTypeScreenScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeScreenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-appendMappingsForIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.appendMappingsForIssueTypeScreenScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-appendMappingsForIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDefaultScreenScheme - errors', () => {
      it('should have a updateDefaultScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateDefaultScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeScreenSchemeId', (done) => {
        try {
          a.updateDefaultScreenScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeScreenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateDefaultScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDefaultScreenScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateDefaultScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeMappingsFromIssueTypeScreenScheme - errors', () => {
      it('should have a removeMappingsFromIssueTypeScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.removeMappingsFromIssueTypeScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeScreenSchemeId', (done) => {
        try {
          a.removeMappingsFromIssueTypeScreenScheme(null, null, (data, error) => {
            try {
              const displayE = 'issueTypeScreenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeMappingsFromIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeMappingsFromIssueTypeScreenScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeMappingsFromIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectsForIssueTypeScreenScheme - errors', () => {
      it('should have a getProjectsForIssueTypeScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.getProjectsForIssueTypeScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueTypeScreenSchemeId', (done) => {
        try {
          a.getProjectsForIssueTypeScreenScheme(null, null, null, (data, error) => {
            try {
              const displayE = 'issueTypeScreenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getProjectsForIssueTypeScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAutoCompletePost - errors', () => {
      it('should have a getAutoCompletePost function', (done) => {
        try {
          assert.equal(true, typeof a.getAutoCompletePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getAutoCompletePost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAutoCompletePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#parseJqlQueries - errors', () => {
      it('should have a parseJqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.parseJqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.parseJqlQueries('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-parseJqlQueries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#matchIssues - errors', () => {
      it('should have a matchIssues function', (done) => {
        try {
          assert.equal(true, typeof a.matchIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.matchIssues(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-matchIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLabels - errors', () => {
      it('should have a getAllLabels function', (done) => {
        try {
          assert.equal(true, typeof a.getAllLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecent - errors', () => {
      it('should have a getRecent function', (done) => {
        try {
          assert.equal(true, typeof a.getRecent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#archiveProject - errors', () => {
      it('should have a archiveProject function', (done) => {
        try {
          assert.equal(true, typeof a.archiveProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.archiveProject(null, (data, error) => {
            try {
              const displayE = 'projectIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-archiveProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProjectAsynchronously - errors', () => {
      it('should have a deleteProjectAsynchronously function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProjectAsynchronously === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.deleteProjectAsynchronously(null, (data, error) => {
            try {
              const displayE = 'projectIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteProjectAsynchronously', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restore - errors', () => {
      it('should have a restore function', (done) => {
        try {
          assert.equal(true, typeof a.restore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.restore(null, (data, error) => {
            try {
              const displayE = 'projectIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-restore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHierarchy - errors', () => {
      it('should have a getHierarchy function', (done) => {
        try {
          assert.equal(true, typeof a.getHierarchy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getHierarchy(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getHierarchy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessibleProjectTypes - errors', () => {
      it('should have a getAllAccessibleProjectTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAccessibleProjectTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeaturesForProject - errors', () => {
      it('should have a getFeaturesForProject function', (done) => {
        try {
          assert.equal(true, typeof a.getFeaturesForProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.getFeaturesForProject(null, (data, error) => {
            try {
              const displayE = 'projectIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getFeaturesForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#toggleFeatureForProject - errors', () => {
      it('should have a toggleFeatureForProject function', (done) => {
        try {
          assert.equal(true, typeof a.toggleFeatureForProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectIdOrKey', (done) => {
        try {
          a.toggleFeatureForProject(null, null, null, (data, error) => {
            try {
              const displayE = 'projectIdOrKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-toggleFeatureForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing featureKey', (done) => {
        try {
          a.toggleFeatureForProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'featureKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-toggleFeatureForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.toggleFeatureForProject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-toggleFeatureForProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectEmail - errors', () => {
      it('should have a getProjectEmail function', (done) => {
        try {
          assert.equal(true, typeof a.getProjectEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getProjectEmail(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getProjectEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateProjectEmail - errors', () => {
      it('should have a updateProjectEmail function', (done) => {
        try {
          assert.equal(true, typeof a.updateProjectEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateProjectEmail(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateProjectEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateProjectEmail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateProjectEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScreenSchemes - errors', () => {
      it('should have a getScreenSchemes function', (done) => {
        try {
          assert.equal(true, typeof a.getScreenSchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createScreenScheme - errors', () => {
      it('should have a createScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.createScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createScreenScheme(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateScreenScheme - errors', () => {
      it('should have a updateScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenSchemeId', (done) => {
        try {
          a.updateScreenScheme(null, null, (data, error) => {
            try {
              const displayE = 'screenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateScreenScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScreenScheme - errors', () => {
      it('should have a deleteScreenScheme function', (done) => {
        try {
          assert.equal(true, typeof a.deleteScreenScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing screenSchemeId', (done) => {
        try {
          a.deleteScreenScheme(null, (data, error) => {
            try {
              const displayE = 'screenSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteScreenScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatarImageByType - errors', () => {
      it('should have a getAvatarImageByType function', (done) => {
        try {
          assert.equal(true, typeof a.getAvatarImageByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAvatarImageByType(null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAvatarImageByType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatarImageByID - errors', () => {
      it('should have a getAvatarImageByID function', (done) => {
        try {
          assert.equal(true, typeof a.getAvatarImageByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAvatarImageByID(null, null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAvatarImageByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAvatarImageByID('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAvatarImageByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatarImageByOwner - errors', () => {
      it('should have a getAvatarImageByOwner function', (done) => {
        try {
          assert.equal(true, typeof a.getAvatarImageByOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAvatarImageByOwner(null, null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAvatarImageByOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityId', (done) => {
        try {
          a.getAvatarImageByOwner('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'entityId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAvatarImageByOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmail - errors', () => {
      it('should have a getUserEmail function', (done) => {
        try {
          assert.equal(true, typeof a.getUserEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getUserEmail(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getUserEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmailBulk - errors', () => {
      it('should have a getUserEmailBulk function', (done) => {
        try {
          assert.equal(true, typeof a.getUserEmailBulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getUserEmailBulk(null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getUserEmailBulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsersDefault - errors', () => {
      it('should have a getAllUsersDefault function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUsersDefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsers - errors', () => {
      it('should have a getAllUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAllUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicWebhooksForApp - errors', () => {
      it('should have a getDynamicWebhooksForApp function', (done) => {
        try {
          assert.equal(true, typeof a.getDynamicWebhooksForApp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDynamicWebhooks - errors', () => {
      it('should have a registerDynamicWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.registerDynamicWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.registerDynamicWebhooks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-registerDynamicWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebhookById - errors', () => {
      it('should have a deleteWebhookById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebhookById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteWebhookById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteWebhookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFailedWebhooks - errors', () => {
      it('should have a getFailedWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.getFailedWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshWebhooks - errors', () => {
      it('should have a refreshWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.refreshWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.refreshWebhooks(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-refreshWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWorkflow - errors', () => {
      it('should have a createWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.createWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createWorkflow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowsPaginated - errors', () => {
      it('should have a getWorkflowsPaginated function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowsPaginated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInactiveWorkflow - errors', () => {
      it('should have a deleteInactiveWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInactiveWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityId', (done) => {
        try {
          a.deleteInactiveWorkflow(null, (data, error) => {
            try {
              const displayE = 'entityId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteInactiveWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowTransitionRuleConfigurations - errors', () => {
      it('should have a getWorkflowTransitionRuleConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowTransitionRuleConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing types', (done) => {
        try {
          a.getWorkflowTransitionRuleConfigurations('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'types is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getWorkflowTransitionRuleConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWorkflowTransitionRuleConfigurations - errors', () => {
      it('should have a updateWorkflowTransitionRuleConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.updateWorkflowTransitionRuleConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateWorkflowTransitionRuleConfigurations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateWorkflowTransitionRuleConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowTransitionRuleConfigurations - errors', () => {
      it('should have a deleteWorkflowTransitionRuleConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowTransitionRuleConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteWorkflowTransitionRuleConfigurations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteWorkflowTransitionRuleConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllWorkflowSchemes - errors', () => {
      it('should have a getAllWorkflowSchemes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllWorkflowSchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowSchemeProjectAssociations - errors', () => {
      it('should have a getWorkflowSchemeProjectAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowSchemeProjectAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getWorkflowSchemeProjectAssociations(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getWorkflowSchemeProjectAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignSchemeToProject - errors', () => {
      it('should have a assignSchemeToProject function', (done) => {
        try {
          assert.equal(true, typeof a.assignSchemeToProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignSchemeToProject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-assignSchemeToProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#publishDraftWorkflowScheme - errors', () => {
      it('should have a publishDraftWorkflowScheme function', (done) => {
        try {
          assert.equal(true, typeof a.publishDraftWorkflowScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.publishDraftWorkflowScheme(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-publishDraftWorkflowScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.publishDraftWorkflowScheme('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-publishDraftWorkflowScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBanner - errors', () => {
      it('should have a getBanner function', (done) => {
        try {
          assert.equal(true, typeof a.getBanner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setBanner - errors', () => {
      it('should have a setBanner function', (done) => {
        try {
          assert.equal(true, typeof a.setBanner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setBanner(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setBanner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAvailableDashboardGadgets - errors', () => {
      it('should have a getAllAvailableDashboardGadgets function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAvailableDashboardGadgets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGadgets - errors', () => {
      it('should have a getAllGadgets function', (done) => {
        try {
          assert.equal(true, typeof a.getAllGadgets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.getAllGadgets(null, null, null, null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getAllGadgets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGadget - errors', () => {
      it('should have a addGadget function', (done) => {
        try {
          assert.equal(true, typeof a.addGadget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.addGadget(null, null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addGadget('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGadget - errors', () => {
      it('should have a updateGadget function', (done) => {
        try {
          assert.equal(true, typeof a.updateGadget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.updateGadget(null, null, null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gadgetId', (done) => {
        try {
          a.updateGadget('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gadgetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGadget('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGadget - errors', () => {
      it('should have a removeGadget function', (done) => {
        try {
          assert.equal(true, typeof a.removeGadget === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashboardId', (done) => {
        try {
          a.removeGadget(null, null, (data, error) => {
            try {
              const displayE = 'dashboardId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gadgetId', (done) => {
        try {
          a.removeGadget('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gadgetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeGadget', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrashedFieldsPaginated - errors', () => {
      it('should have a getTrashedFieldsPaginated function', (done) => {
        try {
          assert.equal(true, typeof a.getTrashedFieldsPaginated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeFilterOwner - errors', () => {
      it('should have a changeFilterOwner function', (done) => {
        try {
          assert.equal(true, typeof a.changeFilterOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeFilterOwner(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-changeFilterOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeFilterOwner('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-changeFilterOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIssueSecurityScheme - errors', () => {
      it('should have a createIssueSecurityScheme function', (done) => {
        try {
          assert.equal(true, typeof a.createIssueSecurityScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIssueSecurityScheme(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createIssueSecurityScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityLevels - errors', () => {
      it('should have a getSecurityLevels function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityLevels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultLevels - errors', () => {
      it('should have a setDefaultLevels function', (done) => {
        try {
          assert.equal(true, typeof a.setDefaultLevels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setDefaultLevels(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setDefaultLevels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityLevelMembers - errors', () => {
      it('should have a getSecurityLevelMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityLevelMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchProjectsUsingSecuritySchemes - errors', () => {
      it('should have a searchProjectsUsingSecuritySchemes function', (done) => {
        try {
          assert.equal(true, typeof a.searchProjectsUsingSecuritySchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchSecuritySchemes - errors', () => {
      it('should have a searchSecuritySchemes function', (done) => {
        try {
          assert.equal(true, typeof a.searchSecuritySchemes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIssueSecurityScheme - errors', () => {
      it('should have a updateIssueSecurityScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateIssueSecurityScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateIssueSecurityScheme(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateIssueSecurityScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIssueSecurityScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateIssueSecurityScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityScheme - errors', () => {
      it('should have a deleteSecurityScheme function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.deleteSecurityScheme(null, (data, error) => {
            try {
              const displayE = 'schemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteSecurityScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSecurityLevel - errors', () => {
      it('should have a addSecurityLevel function', (done) => {
        try {
          assert.equal(true, typeof a.addSecurityLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.addSecurityLevel(null, null, (data, error) => {
            try {
              const displayE = 'schemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSecurityLevel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityLevel - errors', () => {
      it('should have a updateSecurityLevel function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.updateSecurityLevel(null, null, null, (data, error) => {
            try {
              const displayE = 'schemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing levelId', (done) => {
        try {
          a.updateSecurityLevel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'levelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSecurityLevel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeLevel - errors', () => {
      it('should have a removeLevel function', (done) => {
        try {
          assert.equal(true, typeof a.removeLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.removeLevel(null, null, null, (data, error) => {
            try {
              const displayE = 'schemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing levelId', (done) => {
        try {
          a.removeLevel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'levelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSecurityLevelMembers - errors', () => {
      it('should have a addSecurityLevelMembers function', (done) => {
        try {
          assert.equal(true, typeof a.addSecurityLevelMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.addSecurityLevelMembers(null, null, null, (data, error) => {
            try {
              const displayE = 'schemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addSecurityLevelMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing levelId', (done) => {
        try {
          a.addSecurityLevelMembers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'levelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addSecurityLevelMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSecurityLevelMembers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addSecurityLevelMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeMemberFromSecurityLevel - errors', () => {
      it('should have a removeMemberFromSecurityLevel function', (done) => {
        try {
          assert.equal(true, typeof a.removeMemberFromSecurityLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schemeId', (done) => {
        try {
          a.removeMemberFromSecurityLevel(null, null, null, (data, error) => {
            try {
              const displayE = 'schemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeMemberFromSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing levelId', (done) => {
        try {
          a.removeMemberFromSecurityLevel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'levelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeMemberFromSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memberId', (done) => {
        try {
          a.removeMemberFromSecurityLevel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'memberId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeMemberFromSecurityLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sanitiseJqlQueries - errors', () => {
      it('should have a sanitiseJqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.sanitiseJqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.sanitiseJqlQueries(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-sanitiseJqlQueries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrecomputations - errors', () => {
      it('should have a getPrecomputations function', (done) => {
        try {
          assert.equal(true, typeof a.getPrecomputations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePrecomputations - errors', () => {
      it('should have a updatePrecomputations function', (done) => {
        try {
          assert.equal(true, typeof a.updatePrecomputations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePrecomputations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updatePrecomputations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApproximateLicenseCount - errors', () => {
      it('should have a getApproximateLicenseCount function', (done) => {
        try {
          assert.equal(true, typeof a.getApproximateLicenseCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApproximateApplicationLicenseCount - errors', () => {
      it('should have a getApproximateApplicationLicenseCount function', (done) => {
        try {
          assert.equal(true, typeof a.getApproximateApplicationLicenseCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationKey', (done) => {
        try {
          a.getApproximateApplicationLicenseCount(null, (data, error) => {
            try {
              const displayE = 'applicationKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-getApproximateApplicationLicenseCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNotificationScheme - errors', () => {
      it('should have a createNotificationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.createNotificationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNotificationScheme(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createNotificationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotificationSchemeToProjectMappings - errors', () => {
      it('should have a getNotificationSchemeToProjectMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getNotificationSchemeToProjectMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNotificationScheme - errors', () => {
      it('should have a updateNotificationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.updateNotificationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateNotificationScheme(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateNotificationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateNotificationScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateNotificationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNotifications - errors', () => {
      it('should have a addNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.addNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.addNotifications(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addNotifications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addNotifications('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addNotifications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNotificationScheme - errors', () => {
      it('should have a deleteNotificationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNotificationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationSchemeId', (done) => {
        try {
          a.deleteNotificationScheme(null, (data, error) => {
            try {
              const displayE = 'notificationSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteNotificationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNotificationFromNotificationScheme - errors', () => {
      it('should have a removeNotificationFromNotificationScheme function', (done) => {
        try {
          assert.equal(true, typeof a.removeNotificationFromNotificationScheme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationSchemeId', (done) => {
        try {
          a.removeNotificationFromNotificationScheme(null, null, (data, error) => {
            try {
              const displayE = 'notificationSchemeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeNotificationFromNotificationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationId', (done) => {
        try {
          a.removeNotificationFromNotificationScheme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'notificationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-removeNotificationFromNotificationScheme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPriority - errors', () => {
      it('should have a createPriority function', (done) => {
        try {
          assert.equal(true, typeof a.createPriority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPriority(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createPriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultPriority - errors', () => {
      it('should have a setDefaultPriority function', (done) => {
        try {
          assert.equal(true, typeof a.setDefaultPriority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setDefaultPriority(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setDefaultPriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#movePriorities - errors', () => {
      it('should have a movePriorities function', (done) => {
        try {
          assert.equal(true, typeof a.movePriorities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.movePriorities(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-movePriorities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchPriorities - errors', () => {
      it('should have a searchPriorities function', (done) => {
        try {
          assert.equal(true, typeof a.searchPriorities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePriority - errors', () => {
      it('should have a updatePriority function', (done) => {
        try {
          assert.equal(true, typeof a.updatePriority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePriority(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updatePriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePriority('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updatePriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePriority - errors', () => {
      it('should have a deletePriority function', (done) => {
        try {
          assert.equal(true, typeof a.deletePriority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePriority(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deletePriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing replaceWith', (done) => {
        try {
          a.deletePriority('fakeparam', null, (data, error) => {
            try {
              const displayE = 'replaceWith is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deletePriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResolution - errors', () => {
      it('should have a createResolution function', (done) => {
        try {
          assert.equal(true, typeof a.createResolution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createResolution(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createResolution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultResolution - errors', () => {
      it('should have a setDefaultResolution function', (done) => {
        try {
          assert.equal(true, typeof a.setDefaultResolution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setDefaultResolution(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-setDefaultResolution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveResolutions - errors', () => {
      it('should have a moveResolutions function', (done) => {
        try {
          assert.equal(true, typeof a.moveResolutions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.moveResolutions(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-moveResolutions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchResolutions - errors', () => {
      it('should have a searchResolutions function', (done) => {
        try {
          assert.equal(true, typeof a.searchResolutions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateResolution - errors', () => {
      it('should have a updateResolution function', (done) => {
        try {
          assert.equal(true, typeof a.updateResolution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateResolution(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateResolution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateResolution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateResolution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResolution - errors', () => {
      it('should have a deleteResolution function', (done) => {
        try {
          assert.equal(true, typeof a.deleteResolution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteResolution(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteResolution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing replaceWith', (done) => {
        try {
          a.deleteResolution('fakeparam', null, (data, error) => {
            try {
              const displayE = 'replaceWith is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteResolution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusesById - errors', () => {
      it('should have a getStatusesById function', (done) => {
        try {
          assert.equal(true, typeof a.getStatusesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStatuses - errors', () => {
      it('should have a updateStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.updateStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateStatuses(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createStatuses - errors', () => {
      it('should have a createStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.createStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createStatuses(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStatusesById - errors', () => {
      it('should have a deleteStatusesById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteStatusesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search - errors', () => {
      it('should have a search function', (done) => {
        try {
          assert.equal(true, typeof a.search === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUiModifications - errors', () => {
      it('should have a getUiModifications function', (done) => {
        try {
          assert.equal(true, typeof a.getUiModifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUiModification - errors', () => {
      it('should have a createUiModification function', (done) => {
        try {
          assert.equal(true, typeof a.createUiModification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUiModification(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-createUiModification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUiModification - errors', () => {
      it('should have a updateUiModification function', (done) => {
        try {
          assert.equal(true, typeof a.updateUiModification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uiModificationId', (done) => {
        try {
          a.updateUiModification(null, null, (data, error) => {
            try {
              const displayE = 'uiModificationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateUiModification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUiModification('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-updateUiModification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUiModification - errors', () => {
      it('should have a deleteUiModification function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUiModification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uiModificationId', (done) => {
        try {
          a.deleteUiModification(null, (data, error) => {
            try {
              const displayE = 'uiModificationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-deleteUiModification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceGetAddonPropertiesGet - errors', () => {
      it('should have a addonPropertiesResourceGetAddonPropertiesGet function', (done) => {
        try {
          assert.equal(true, typeof a.addonPropertiesResourceGetAddonPropertiesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonKey', (done) => {
        try {
          a.addonPropertiesResourceGetAddonPropertiesGet(null, (data, error) => {
            try {
              const displayE = 'addonKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourceGetAddonPropertiesGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceGetAddonPropertyGet - errors', () => {
      it('should have a addonPropertiesResourceGetAddonPropertyGet function', (done) => {
        try {
          assert.equal(true, typeof a.addonPropertiesResourceGetAddonPropertyGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonKey', (done) => {
        try {
          a.addonPropertiesResourceGetAddonPropertyGet(null, null, (data, error) => {
            try {
              const displayE = 'addonKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourceGetAddonPropertyGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.addonPropertiesResourceGetAddonPropertyGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'propertyKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourceGetAddonPropertyGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourcePutAddonPropertyPut - errors', () => {
      it('should have a addonPropertiesResourcePutAddonPropertyPut function', (done) => {
        try {
          assert.equal(true, typeof a.addonPropertiesResourcePutAddonPropertyPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonKey', (done) => {
        try {
          a.addonPropertiesResourcePutAddonPropertyPut(null, null, null, (data, error) => {
            try {
              const displayE = 'addonKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourcePutAddonPropertyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.addonPropertiesResourcePutAddonPropertyPut('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'propertyKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourcePutAddonPropertyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addonPropertiesResourcePutAddonPropertyPut('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourcePutAddonPropertyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceDeleteAddonPropertyDelete - errors', () => {
      it('should have a addonPropertiesResourceDeleteAddonPropertyDelete function', (done) => {
        try {
          assert.equal(true, typeof a.addonPropertiesResourceDeleteAddonPropertyDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addonKey', (done) => {
        try {
          a.addonPropertiesResourceDeleteAddonPropertyDelete(null, null, (data, error) => {
            try {
              const displayE = 'addonKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourceDeleteAddonPropertyDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.addonPropertiesResourceDeleteAddonPropertyDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'propertyKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourceDeleteAddonPropertyDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourcePutAppPropertyPut - errors', () => {
      it('should have a addonPropertiesResourcePutAppPropertyPut function', (done) => {
        try {
          assert.equal(true, typeof a.addonPropertiesResourcePutAppPropertyPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.addonPropertiesResourcePutAppPropertyPut(null, null, (data, error) => {
            try {
              const displayE = 'propertyKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourcePutAppPropertyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addonPropertiesResourcePutAppPropertyPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourcePutAppPropertyPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceDeleteAppPropertyDelete - errors', () => {
      it('should have a addonPropertiesResourceDeleteAppPropertyDelete function', (done) => {
        try {
          assert.equal(true, typeof a.addonPropertiesResourceDeleteAppPropertyDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing propertyKey', (done) => {
        try {
          a.addonPropertiesResourceDeleteAppPropertyDelete(null, (data, error) => {
            try {
              const displayE = 'propertyKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-addonPropertiesResourceDeleteAppPropertyDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dynamicModulesResourceGetModulesGet - errors', () => {
      it('should have a dynamicModulesResourceGetModulesGet function', (done) => {
        try {
          assert.equal(true, typeof a.dynamicModulesResourceGetModulesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dynamicModulesResourceRegisterModulesPost - errors', () => {
      it('should have a dynamicModulesResourceRegisterModulesPost function', (done) => {
        try {
          assert.equal(true, typeof a.dynamicModulesResourceRegisterModulesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.dynamicModulesResourceRegisterModulesPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-dynamicModulesResourceRegisterModulesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dynamicModulesResourceRemoveModulesDelete - errors', () => {
      it('should have a dynamicModulesResourceRemoveModulesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.dynamicModulesResourceRemoveModulesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appIssueFieldValueUpdateResourceUpdateIssueFieldsPut - errors', () => {
      it('should have a appIssueFieldValueUpdateResourceUpdateIssueFieldsPut function', (done) => {
        try {
          assert.equal(true, typeof a.appIssueFieldValueUpdateResourceUpdateIssueFieldsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.appIssueFieldValueUpdateResourceUpdateIssueFieldsPut(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-appIssueFieldValueUpdateResourceUpdateIssueFieldsPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#migrationResourceUpdateEntityPropertiesValuePut - errors', () => {
      it('should have a migrationResourceUpdateEntityPropertiesValuePut function', (done) => {
        try {
          assert.equal(true, typeof a.migrationResourceUpdateEntityPropertiesValuePut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing entityType', (done) => {
        try {
          a.migrationResourceUpdateEntityPropertiesValuePut(null, null, (data, error) => {
            try {
              const displayE = 'entityType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-migrationResourceUpdateEntityPropertiesValuePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.migrationResourceUpdateEntityPropertiesValuePut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-migrationResourceUpdateEntityPropertiesValuePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#migrationResourceWorkflowRuleSearchPost - errors', () => {
      it('should have a migrationResourceWorkflowRuleSearchPost function', (done) => {
        try {
          assert.equal(true, typeof a.migrationResourceWorkflowRuleSearchPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.migrationResourceWorkflowRuleSearchPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-jira-adapter-migrationResourceWorkflowRuleSearchPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
