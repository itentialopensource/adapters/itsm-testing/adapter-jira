/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-jira',
      type: 'Jira',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Jira = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Jira Adapter Test', () => {
  describe('Jira Class Tests', () => {
    const a = new Jira(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    // As a part of test setup, make sure that all constants below are valid in your Jira environment
    const Consts = {
      projectType: 'software', // a valid project type for the particular jira system
      issueType: 'standard',
      issuetypeId: '10001', // a valid issueType
      permissionSearch: 'ASSIGN_ISSUE', // a valid permission for the particular jira system
      dashboardItemId: '10001', // See https://community.atlassian.com/t5/Jira-Core-questions/How-to-obtain-ItemId-in-api-2-dashboard-dashboardId-items-itemId/qaq-p/189400
      expand: 'description',
      date: '2018-10-03',
      start: 0,
      max: 10,
      fake: 'fakedata',
      dummyKey: 'dummyKey',
      dummyData: {
        value: 'here is some dummy data'
      },
      dummyString: 'some dummy string value',
      dummyStringOther: 'some other dummy string value',
      dummyDisplayName: 'John Smith',
      dummyEmail: 'john.smith@example.com',
      dummyUserKey: 'JohnSmithUserKey',
      dummyUserName: 'JohnSmithUsername',
      dummyGroupName: 'it-group',
      dummyProjectKey: 'ITTEST',
      dummyProjectKey2: 'ITTEST2',
      dummyProjectName: 'Adapter Test Project',
      dummyProjectName2: 'Adapter Test Project 2',
      dummyUpdatedProjectName: 'Adapter Test Project new',
      loremIpsum: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget venenatis elit. Duis eu justo eget augue iaculis fermentum. Sed semper quam laoreet nisi egestas',
      dummyIssueTypeName: 'new issue type name',
      dummyIssueTypeNameUpdated: 'updated issue type name'
    };

    describe('System Integration Tests', () => {
      let testProjectId;
      let myUserId;
      mocha.before(() => {
        let setupPromise;
        if (!stub) {
          setupPromise = new Promise((resolveAll) => {
            new Promise((resolve) => {
              a.getRestapi2myself(undefined, (data, error) => {
                if (error) {
                  console.log(error);
                }
                myUserId = data.response.accountId;
                resolve();
              });
            }).then(() => {
              new Promise((resolve) => {
                const projectbody = {
                  key: Consts.dummyProjectKey,
                  name: Consts.dummyProjectName,
                  projectTypeKey: Consts.projectType,
                  leadAccountId: myUserId
                };
                a.postRestapi2project(projectbody, (data, error) => {
                  if (error) {
                    console.log(error);
                  }
                  testProjectId = data.response.id;
                  resolve();
                });
              }).then(resolveAll)
                .catch(() => {
                  log('Cannot create test project, please check that the user associated with the Jira adapter has admin access and try again.');
                });
            });
          });
        }
        return setupPromise;
      });

      mocha.after(() => {
        let teardownPromise;
        if (!stub) {
          teardownPromise = new Promise((resolve) => {
            a.deleteRestapi2projectprojectIdOrKey(testProjectId, (data) => {
              resolve(data);
            });
          });
        }
        return teardownPromise;
      });

      describe('Application Role Entity', () => {
        let applicationRoleKey;
        describe('#getRestapi2applicationrole - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              a.getRestapi2applicationrole((data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    // There should be at least one application role
                    assert.equal(data.response.length > 0, true);
                    applicationRoleKey = data.response[0].key;
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2applicationrolekey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              const roleKey = applicationRoleKey || Consts.fake;
              a.getRestapi2applicationrolekey(roleKey, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('in', data.response.key);
                    assert.equal(true, Array.isArray(data.response.groups));
                    assert.equal('Duis eiusmod', data.response.name);
                    assert.equal(true, Array.isArray(data.response.defaultGroups));
                    assert.equal(false, data.response.selectedByDefault);
                    assert.equal(true, data.response.defined);
                    assert.equal(-543742, data.response.numberOfSeats);
                    assert.equal(97353986, data.response.remainingSeats);
                    assert.equal(55713618, data.response.userCount);
                    assert.equal('mollit', data.response.userCountDescription);
                    assert.equal(true, data.response.hasUnlimitedSeats);
                    assert.equal(true, data.response.platform);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Auditrecord Entity', () => {
        describe('#getRestapi2auditingrecord - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let offset;
              let limit;
              let filter;
              let from;
              let to;
              if (stub) {
                offset = Consts.fake;
                limit = Consts.fake;
                filter = Consts.fake;
                from = Consts.fake;
                to = Consts.fake;
              } else {
                offset = 0;
                limit = 10;
                filter = '';
                from = Consts.date;
                to = Consts.date;
              }
              a.getRestapi2auditingrecord(offset, limit, filter, from, to, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(4929371, data.response.offset);
                    assert.equal(89248195, data.response.limit);
                    assert.equal(50005341, data.response.total);
                    assert.equal(true, Array.isArray(data.response.records));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(data.response.offset, offset);
                    assert.equal(data.response.limit, limit);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Dashboard Entity', () => {
        let dashboardObj;
        describe('#getRestapi2dashboard - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            let filter;
            let startAt;
            let maxResult;
            if (stub) {
              filter = Consts.fake;
              startAt = Consts.fake;
              maxResult = Consts.fake;
            } else {
              filter = '';
              startAt = Consts.start;
              maxResult = Consts.max;
            }
            try {
              a.getRestapi2dashboard(filter, startAt, maxResult, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(-47030680, data.response.startAt);
                    assert.equal(51581726, data.response.maxResults);
                    assert.equal(-30100979, data.response.total);
                    assert.equal('velit aliquip Duis', data.response.prev);
                    assert.equal('minim proident deserunt reprehenderit sed', data.response.next);
                    assert.equal(true, Array.isArray(data.response.dashboards));
                  } else {
                    [dashboardObj] = data.response.dashboards;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    // there should be at least one default dashboard
                    assert.equal(data.response.dashboards.length > 0, true);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2dashboardsearch - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            let dashboardName;
            let accountId;
            let owner;
            let groupname;
            let projectId;
            let orderBy;
            let startAt;
            let maxResults;
            let expand;
            if (stub) {
              dashboardName = Consts.fake;
              accountId = Consts.fake;
              owner = Consts.fake;
              groupname = Consts.fake;
              projectId = Consts.fake;
              orderBy = Consts.fake;
              startAt = Consts.fake;
              maxResults = Consts.fake;
              expand = Consts.fake;
            } else {
              dashboardName = dashboardObj.name;
              accountId = null;
              owner = '';
              groupname = '';
              projectId = '';
              orderBy = 'id';
              startAt = Consts.start;
              maxResults = Consts.max;
              expand = Consts.expane;
            }

            try {
              a.getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('https://yJihQinYGPSKUxmuyqZHrHyEXVpkOAZM.tqofqtF.e45', data.response.self);
                    assert.equal('https://NPIHReiLiTjNQiIqlpc.xhwMv+-rvuaKPVC1SaMAR0uNcMsJAGX.ww,fhle', data.response.nextPage);
                    assert.equal(-30980114, data.response.maxResults);
                    assert.equal(85027301, data.response.startAt);
                    assert.equal(-80944909, data.response.total);
                    assert.equal(true, data.response.isLast);
                    assert.equal(true, Array.isArray(data.response.values));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    const dashboards = data.response.values;
                    assert.equal(dashboards.length > 0, true);
                    assert.equal(dashboards[0].id, dashboardObj.id);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2dashboardid - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            let getid;
            if (stub) {
              getid = Consts.fake;
            } else {
              getid = dashboardObj.id;
            }
            try {
              a.getRestapi2dashboardid(getid, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('amet et cillum nostrud fugiat', data.response.description);
                    assert.equal('cillum non Ut id dolore', data.response.id);
                    assert.equal(true, data.response.isFavourite);
                    assert.equal('magna', data.response.name);
                    assert.equal('object', typeof data.response.owner);
                    assert.equal(-84813416, data.response.popularity);
                    assert.equal(9171912, data.response.rank);
                    assert.equal('https://pbImsnXxqEZEfJm.spuzTt8MeG8qCTzYyve,st63Qng3vWy4hio', data.response.self);
                    assert.equal(true, Array.isArray(data.response.sharePermissions));
                    assert.equal('proident Lorem nisi officia', data.response.view);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(data.response.id, dashboardObj.id);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2dashboarddashboardIditemsitemIdproperties - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let dashboardId;
              let itemId;
              if (stub) {
                dashboardId = Consts.fake;
                itemId = Consts.fake;
              } else {
                dashboardId = dashboardObj.id;
                itemId = Consts.dashboardItemId;
              }

              a.getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.keys));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(data.response.keys.length > 0, true);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            let dashboardId;
            let itemId;
            let propertyKey;
            let body;

            if (stub) {
              dashboardId = Consts.fake;
              itemId = Consts.fake;
              propertyKey = Consts.fake;
              body = Consts.fake;
            } else {
              dashboardId = dashboardObj.id;
              itemId = Consts.dashboardItemId;
              propertyKey = Consts.dummyKey;
              body = Consts.dummyData;
            }

            try {
              a.putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let dashboardId;
              let itemId;
              let propertyKey;

              if (stub) {
                dashboardId = Consts.fake;
                itemId = Consts.fake;
                propertyKey = Consts.fake;
              } else {
                dashboardId = dashboardObj.id;
                itemId = Consts.dashboardItemId;
                propertyKey = Consts.dummyKey;
              }

              a.getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('ipsum reprehenderit amet', data.response.key);
                    assert.equal('object', typeof data.response.value);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            let dashboardId;
            let itemId;
            let propertyKey;

            if (stub) {
              dashboardId = Consts.fake;
              itemId = Consts.fake;
              propertyKey = Consts.fake;
            } else {
              dashboardId = dashboardObj.id;
              itemId = Consts.dashboardItemId;
              propertyKey = Consts.dummyKey;
            }
            try {
              a.deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Issue Entity', () => {
        let issueId;
        let newTransitionId;
        describe('#postRestapi2issue - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            let updateHistory;
            let body;
            if (stub) {
              updateHistory = Consts.fake;
              body = Consts.fake;
            } else {
              body = {
                fields: {
                  summary: Consts.dummyString,
                  project: {
                    id: testProjectId
                  },
                  issuetype: {
                    id: Consts.issuetypeId
                  }
                }
              };
              updateHistory = false;
            }
            try {
              a.postRestapi2issue(updateHistory, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('ullamco', data.response.id);
                    assert.equal('mollit', data.response.key);
                    assert.equal('eu mollit non voluptate minim', data.response.self);
                    assert.equal('object', typeof data.response.transition);
                  } else {
                    issueId = data.response.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issuecreatemeta - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let projectIds;
              let projectKeys;
              let issuetypeIds;
              let issuetypeNames;
              let expand;
              if (stub) {
                projectIds = Consts.fake;
                projectKeys = Consts.fake;
                issuetypeIds = Consts.fake;
                issuetypeNames = Consts.fake;
                expand = Consts.fake;
              } else {
                projectIds = [testProjectId];
                issuetypeIds = [Consts.issuetypeId];
              }
              a.getRestapi2issuecreatemeta(projectIds, projectKeys, issuetypeIds, issuetypeNames, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('aliqua sit veniam consequat', data.response.expand);
                    assert.equal(true, Array.isArray(data.response.projects));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.notEqual(undefined, data.response.projects);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issueissueIdOrKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueIdOrKey;
              let fields;
              let fieldsByKeys;
              let expand;
              let properties;
              let updateHistory;
              if (stub) {
                issueIdOrKey = Consts.fake;
                fields = Consts.fake;
                fieldsByKeys = Consts.fake;
                expand = Consts.fake;
                properties = Consts.fake;
                updateHistory = Consts.fake;
              } else {
                issueIdOrKey = issueId;
              }
              a.getRestapi2issueissueIdOrKey(issueIdOrKey, fields, fieldsByKeys, expand, properties, updateHistory, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('Excepteur et aliquip irure', data.response.expand);
                    assert.equal('laborum dolore veniam', data.response.id);
                    assert.equal('http://qdgdsCFllbzOSctPqE.kwpzyNzSDhm--bvnjv9V+BQGxP71QcNJP8ML,+H-PYzmSxlEvTTmqxCsAY3LuQKmh', data.response.self);
                    assert.equal('aliquip voluptate', data.response.key);
                    assert.equal('object', typeof data.response.renderedFields);
                    assert.equal('object', typeof data.response.properties);
                    assert.equal('object', typeof data.response.names);
                    assert.equal('object', typeof data.response.schema);
                    assert.equal(true, Array.isArray(data.response.transitions));
                    assert.equal('object', typeof data.response.operations);
                    assert.equal('object', typeof data.response.editmeta);
                    assert.equal('object', typeof data.response.changelog);
                    assert.equal('object', typeof data.response.versionedRepresentations);
                    assert.equal('object', typeof data.response.fieldsToInclude);
                    assert.equal('object', typeof data.response.fields);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(data.response.id, issueId);
                    assert.equal(data.response.fields.summary, Consts.dummyString);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2issueissueIdOrKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueIdOrKey;
              let notifyUsers;
              let overrideScreenSecurity;
              let overrideEditableFlag;
              let body;
              if (stub) {
                issueIdOrKey = Consts.fake;
                notifyUsers = Consts.fake;
                overrideScreenSecurity = Consts.fake;
                overrideEditableFlag = Consts.fake;
                body = Consts.fake;
              } else {
                issueIdOrKey = issueId;
                body = {
                  update: {
                    summary: [
                      {
                        set: Consts.dummyStringOther
                      }
                    ]
                  }
                };
              }
              a.putRestapi2issueissueIdOrKey(issueIdOrKey, notifyUsers, overrideScreenSecurity, overrideEditableFlag, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2issueissueIdOrKeyassignee - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueIdOrKey;
              let body;
              if (stub) {
                issueIdOrKey = Consts.fake;
                body = Consts.fake;
              } else {
                issueIdOrKey = issueId;
                body = {
                  accountId: myUserId
                };
              }

              a.putRestapi2issueissueIdOrKeyassignee(issueIdOrKey, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issueissueIdOrKeytransitions - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueIdOrKey;
              let expand;
              let transitionId;
              let skipRemoteOnlyCondition;
              if (stub) {
                issueIdOrKey = Consts.fake;
                expand = Consts.fake;
                transitionId = Consts.fake;
                skipRemoteOnlyCondition = Consts.fake;
              } else {
                issueIdOrKey = issueId;
                expand = 'fields';
              }
              a.getRestapi2issueissueIdOrKeytransitions(issueIdOrKey, expand, transitionId, skipRemoteOnlyCondition, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('qui', data.response.expand);
                    assert.equal(true, Array.isArray(data.response.transitions));
                  } else {
                    newTransitionId = data.response.transitions[0].id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postRestapi2issueissueIdOrKeytransitions - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueIdOrKey;
              let body;
              if (stub) {
                issueIdOrKey = Consts.fake;
                body = Consts.fake;
              } else {
                issueIdOrKey = issueId;
                body = {
                  transition: {
                    id: newTransitionId
                  }
                };
              }
              a.postRestapi2issueissueIdOrKeytransitions(issueIdOrKey, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postRestapi2issueissueIdOrKeynotify - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueIdOrKey;
              let body;
              if (stub) {
                issueIdOrKey = Consts.fake;
                body = Consts.fake;
              } else {
                issueIdOrKey = issueId;
                body = {
                  subject: Consts.dummyString,
                  textBody: Consts.loremIpsum,
                  to: {
                    reporter: true
                  }
                };
              }
              a.postRestapi2issueissueIdOrKeynotify(issueIdOrKey, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2issueissueIdOrKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueIdOrKey;
              let deleteSubtasks;
              if (stub) {
                issueIdOrKey = Consts.fake;
                deleteSubtasks = Consts.fake;
              } else {
                issueIdOrKey = issueId;
                deleteSubtasks = true;
              }
              a.deleteRestapi2issueissueIdOrKey(issueIdOrKey, deleteSubtasks, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Issue Comment Entity', () => {
        let newIssueId;
        let newCommentId;
        mocha.before(() => {
          if (!stub) {
            const body = {
              fields: {
                summary: 'issue comment test issue for integration test',
                project: {
                  id: testProjectId
                },
                issuetype: {
                  id: Consts.issuetypeId
                }
              }
            };
            return new Promise((resolve) => {
              const updateHistory = false;
              a.postRestapi2issue(updateHistory, body, (data, err) => {
                if (err) {
                  log(err);
                }
                newIssueId = data.response.id;
              });
            });
          }
          return true;
        });

        mocha.after(() => {
          if (!stub) {
            const issueIdOrKey = newIssueId;
            const deleteSubtasks = true;
            return new Promise((resolve) => {
              a.deleteRestapi2issueissueIdOrKey(issueIdOrKey, deleteSubtasks, (data, err) => {
                if (err) {
                  log(err);
                }
              });
            });
          }
          return true;
        });

        describe('#postRestapi2issueissueIdOrKeycomment - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueIdOrKey;
              let expand;
              let body;
              if (stub) {
                issueIdOrKey = Consts.fake;
                expand = Consts.fake;
                body = Consts.fake;
              } else {
                issueIdOrKey = newIssueId;
                expand = false;
                body = {
                  body: Consts.dummyString
                };
              }
              a.postRestapi2issueissueIdOrKeycomment(issueIdOrKey, expand, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('esse sint nostrud ipsum qui', data.response.self);
                    assert.equal('pariatur deserunt', data.response.id);
                    assert.equal('object', typeof data.response.author);
                    assert.equal('consectetur magna proident', data.response.body);
                    assert.equal('mollit voluptate', data.response.renderedBody);
                    assert.equal('object', typeof data.response.updateAuthor);
                    assert.equal('2018-07-28T19:50:17.392Z', data.response.created);
                    assert.equal('1977-03-08T15:25:30.894Z', data.response.updated);
                    assert.equal('object', typeof data.response.visibility);
                    assert.equal(false, data.response.jsdPublic);
                    assert.equal(true, Array.isArray(data.response.properties));
                  } else {
                    newCommentId = data.response.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postRestapi2commentlist - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let expand;
              let body;
              if (stub) {
                expand = Consts.fake;
                body = Consts.fake;
              } else {
                body = {
                  ids: [newCommentId]
                };
              }
              a.postRestapi2commentlist(expand, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('http://vrQUthOeEPi.pxvbHmop+1xNRp6hlHeK3IUVKGQ6rshmzRzOeRvMMWAYUxr8vxOmMGMH3nt,rqimkqLw,KdsUgm', data.response.self);
                    assert.equal('https://fVTWPnOfPfJZTCIkGjT.ihzYoDOhJrjKyjpTPW', data.response.nextPage);
                    assert.equal(86909254, data.response.maxResults);
                    assert.equal(30698471, data.response.startAt);
                    assert.equal(-29363339, data.response.total);
                    assert.equal(true, data.response.isLast);
                    assert.equal(true, Array.isArray(data.response.values));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2commentcommentIdproperties - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            let commentId;
            if (stub) {
              commentId = Consts.fake;
            } else {
              commentId = newCommentId;
            }

            try {
              a.getRestapi2commentcommentIdproperties(commentId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.keys));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issueissueIdOrKeycomment - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueId;
              let startAt;
              let maxResults;
              let orderBy;
              let expand;
              if (stub) {
                issueId = Consts.fake;
                startAt = Consts.fake;
                maxResults = Consts.fake;
                orderBy = Consts.fake;
                expand = Consts.fake;
              } else {
                issueId = newIssueId;
              }

              a.getRestapi2issueissueIdOrKeycomment(issueId, startAt, maxResults, orderBy, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(-87310979, data.response.startAt);
                    assert.equal(-12067595, data.response.maxResults);
                    assert.equal(37874557, data.response.total);
                    assert.equal(true, Array.isArray(data.response.comments));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issueissueIdOrKeycommentid - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueId;
              let id;
              let expand;
              if (stub) {
                issueId = Consts.fake;
                id = Consts.fake;
                expand = Consts.fake;
              } else {
                issueId = newIssueId;
                id = newCommentId;
              }

              a.getRestapi2issueissueIdOrKeycommentid(issueId, id, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('exercitation', data.response.self);
                    assert.equal('laboris sed do', data.response.id);
                    assert.equal('object', typeof data.response.author);
                    assert.equal('aliqua elit', data.response.body);
                    assert.equal('dolore sunt cupidatat pariatur', data.response.renderedBody);
                    assert.equal('object', typeof data.response.updateAuthor);
                    assert.equal('2010-02-15T13:05:34.731Z', data.response.created);
                    assert.equal('1949-10-24T20:18:37.060Z', data.response.updated);
                    assert.equal('object', typeof data.response.visibility);
                    assert.equal(true, data.response.jsdPublic);
                    assert.equal(true, Array.isArray(data.response.properties));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2issueissueIdOrKeycommentid - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueId;
              let id;
              let expand;
              let body;

              if (stub) {
                issueId = Consts.fake;
                id = Consts.fake;
                expand = Consts.fake;
                body = Consts.fake;
              } else {
                issueId = newIssueId;
                id = newCommentId;
                body = {
                  body: Consts.loremIpsum
                };
              }
              a.putRestapi2issueissueIdOrKeycommentid(issueId, id, expand, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('do', data.response.self);
                    assert.equal('in Excepteur Ut sint', data.response.id);
                    assert.equal('object', typeof data.response.author);
                    assert.equal('sit sed eiusmod', data.response.body);
                    assert.equal('eu incididunt et consequat', data.response.renderedBody);
                    assert.equal('object', typeof data.response.updateAuthor);
                    assert.equal('2017-09-14T21:11:30.215Z', data.response.created);
                    assert.equal('2006-05-13T08:45:37.374Z', data.response.updated);
                    assert.equal('object', typeof data.response.visibility);
                    assert.equal(false, data.response.jsdPublic);
                    assert.equal(true, Array.isArray(data.response.properties));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2issueissueIdOrKeycommentid - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueId;
              let id;
              if (stub) {
                issueId = Consts.fake;
                id = Consts.fake;
              } else {
                issueId = newIssueId;
                id = newCommentId;
              }
              a.deleteRestapi2issueissueIdOrKeycommentid(issueId, id, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Issue Property Entity', () => {
        let newIssueId;
        mocha.before(() => {
          if (!stub) {
            const body = {
              fields: {
                summary: 'issue comment test issue for integration test',
                project: {
                  id: testProjectId
                },
                issuetype: {
                  id: Consts.issuetypeId
                }
              }
            };
            return new Promise((resolve) => {
              const updateHistory = false;
              a.postRestapi2issue(updateHistory, body, (data) => {
                newIssueId = data.response.id;
              });
            });
          }
          return true;
        });

        mocha.after(() => {
          if (!stub) {
            const issueIdOrKey = newIssueId;
            const deleteSubtasks = true;
            return new Promise((resolve) => {
              a.deleteRestapi2issueissueIdOrKey(issueIdOrKey, deleteSubtasks, (data) => {
              });
            });
          }
          return true;
        });

        describe('#getRestapi2issueissueIdOrKeyproperties - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueId;
              if (stub) {
                issueId = Consts.fake;
              } else {
                issueId = newIssueId;
              }
              a.getRestapi2issueissueIdOrKeyproperties(issueId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.keys));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2issueissueIdOrKeypropertiespropertyKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueId;
              let propertyKey;
              let body;
              if (stub) {
                issueId = Consts.fake;
                propertyKey = Consts.fake;
                body = Consts.fake;
              } else {
                issueId = newIssueId;
                propertyKey = Consts.dummyKey;
                body = Consts.dummyData;
              }
              a.putRestapi2issueissueIdOrKeypropertiespropertyKey(issueId, propertyKey, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issueissueIdOrKeypropertiespropertyKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueId;
              let propertyKey;
              if (stub) {
                issueId = Consts.fake;
                propertyKey = Consts.fake;
              } else {
                issueId = newIssueId;
                propertyKey = Consts.dummyKey;
              }
              a.getRestapi2issueissueIdOrKeypropertiespropertyKey(issueId, propertyKey, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('voluptate dolore qui sit', data.response.key);
                    assert.equal('object', typeof data.response.value);
                  } else {
                    assert.equal(data.response[Consts.dummyKey], Consts.dummyValue);
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2issueissueIdOrKeypropertiespropertyKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueId;
              let propertyKey;
              if (stub) {
                issueId = Consts.fake;
                propertyKey = Consts.fake;
              } else {
                issueId = newIssueId;
                propertyKey = Consts.dummyKey;
              }
              a.deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueId, propertyKey, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Issue Resolution Entity', () => {
        let issueResolutionId;
        describe('#getRestapi2resolution - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              a.getRestapi2resolution((data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    issueResolutionId = data.response[0].id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2resolutionid - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let resolutionId;
              if (stub) {
                resolutionId = Consts.fake;
              } else {
                resolutionId = issueResolutionId;
              }
              a.getRestapi2resolutionid(resolutionId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('http://LUPWyMFZx.beraQfX27-WbH-S-pQMhUf0grdgsKUJbfgAuWH0,SnrRl14k8xtRIkSUbxGe-dUo5', data.response.self);
                    assert.equal('eiusmod', data.response.id);
                    assert.equal('nisi dolore', data.response.description);
                    assert.equal('enim ea nulla sed', data.response.name);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Issue Field Property', () => {
        describe('#getRestapi2field - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              a.getRestapi2field((data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Project Entity', () => {
        let newProjectId;
        describe('#postRestapi2project - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let body;
              if (stub) {
                body = Consts.fake;
              } else {
                body = {
                  key: Consts.dummyProjectKey2,
                  name: Consts.dummyProjectName2,
                  projectTypeKey: Consts.projectType,
                  leadAccountId: myUserId
                };
              }
              a.postRestapi2project(body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(32184167, data.response.id);
                    assert.equal('incididunt tempor amet', data.response.key);
                    assert.equal('https://MmvAESOynfzkMzKxNgIpXfFlNwLdpCe.pynAoRy9-mhyD', data.response.self);
                  } else {
                    newProjectId = data.response.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2project - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let expanded;
              let recent;
              if (stub) {
                expanded = Consts.fake;
                recent = Consts.fake;
              } else {
                expanded = Consts.expand;
              }
              a.getRestapi2project(expanded, recent, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2projectsearch - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let startAt;
              let maxResults;
              let orderBy;
              let query;
              let typeKey;
              let categoryId;
              let action;
              let expand;
              if (stub) {
                startAt = Consts.fake;
                maxResults = Consts.fake;
                orderBy = Consts.fake;
                query = Consts.fake;
                typeKey = Consts.fake;
                categoryId = Consts.fake;
                action = Consts.fake;
                expand = Consts.fake;
              } else {
                query = 'test';
              }
              a.getRestapi2projectsearch(startAt, maxResults, orderBy, query, typeKey, categoryId, action, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('http://kVWlXtKgPvPxJREw.tfpX-j-oRjBiYr4kXi1FnUToOD6MQanOKjIiMlTJB-w17tP,87.WwdeXOcpV,-DaYTR', data.response.self);
                    assert.equal('http://mZPSigBTxteJxn.jzpnZQtfIubqC7MMFWkMfoBLWKum3m2jqEAl1HnnrmimaJu1UY1rEJz,dO1a', data.response.nextPage);
                    assert.equal(72555668, data.response.maxResults);
                    assert.equal(91159625, data.response.startAt);
                    assert.equal(60261632, data.response.total);
                    assert.equal(true, data.response.isLast);
                    assert.equal(true, Array.isArray(data.response.values));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2projectprojectIdOrKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let projectId;
              let expanded;
              let body;
              if (stub) {
                projectId = Consts.fake;
                expanded = Consts.fake;
                body = Consts.fake;
              } else {
                projectId = newProjectId;
                expanded = Consts.expand;
                body = {
                  name: Consts.dummyUpdatedProjectName
                };
              }
              a.putRestapi2projectprojectIdOrKey(projectId, expanded, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('exercitation', data.response.expand);
                    assert.equal('https://DlT.tontLVUJuYX7lVYDbzwFw2AXLkxNrYDj8uVZ', data.response.self);
                    assert.equal('dolor lab', data.response.id);
                    assert.equal('est fugiat', data.response.key);
                    assert.equal('elit proident', data.response.description);
                    assert.equal('object', typeof data.response.lead);
                    assert.equal(true, Array.isArray(data.response.components));
                    assert.equal(true, Array.isArray(data.response.issueTypes));
                    assert.equal('qui', data.response.url);
                    assert.equal('in dolore nostrud', data.response.email);
                    assert.equal('UNASSIGNED', data.response.assigneeType);
                    assert.equal(true, Array.isArray(data.response.versions));
                    assert.equal('ex Excepteur sit quis', data.response.name);
                    assert.equal('object', typeof data.response.roles);
                    assert.equal('object', typeof data.response.avatarUrls);
                    assert.equal('object', typeof data.response.projectCategory);
                    assert.equal('ops', data.response.projectTypeKey);
                    assert.equal(false, data.response.simplified);
                    assert.equal('CLASSIC', data.response.style);
                    assert.equal(true, data.response.isPrivate);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2projectprojectIdOrKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let projectId;
              let expand;
              if (stub) {
                projectId = Consts.fake;
                expand = Consts.fake;
              } else {
                projectId = newProjectId;
              }
              a.getRestapi2projectprojectIdOrKey(projectId, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('Excepteur ut', data.response.expand);
                    assert.equal('http://eGtyBAVmWuhknpElScbCkgyYxaPT.ijqOpkZ6B,akpC5FpqULvAP+p', data.response.self);
                    assert.equal('', data.response.id);
                    assert.equal('Ut dolor', data.response.key);
                    assert.equal('tempor ipsum irure do', data.response.description);
                    assert.equal('object', typeof data.response.lead);
                    assert.equal(true, Array.isArray(data.response.components));
                    assert.equal(true, Array.isArray(data.response.issueTypes));
                    assert.equal('eu', data.response.url);
                    assert.equal('in ipsum laborum in', data.response.email);
                    assert.equal('UNASSIGNED', data.response.assigneeType);
                    assert.equal(true, Array.isArray(data.response.versions));
                    assert.equal('reprehenderit', data.response.name);
                    assert.equal('object', typeof data.response.roles);
                    assert.equal('object', typeof data.response.avatarUrls);
                    assert.equal('object', typeof data.response.projectCategory);
                    assert.equal('business', data.response.projectTypeKey);
                    assert.equal(true, data.response.simplified);
                    assert.equal('next-gen', data.response.style);
                    assert.equal(false, data.response.isPrivate);
                  } else {
                    assert.equal(data.response.name, Consts.dummyUpdatedProjectName);
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2projectprojectIdOrKeystatuses - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let projectId;
              if (stub) {
                projectId = Consts.fake;
              } else {
                projectId = newProjectId;
              }
              a.getRestapi2projectprojectIdOrKeystatuses(projectId, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2projectprojectIdOrKeyproperties - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let projectId;
              if (stub) {
                projectId = Consts.fake;
              } else {
                projectId = newProjectId;
              }
              a.getRestapi2projectprojectIdOrKeyproperties(projectId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.keys));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2projectprojectIdOrKeypropertiespropertyKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let projectId;
              let propertyKey;
              let body;
              if (stub) {
                projectId = Consts.fake;
                propertyKey = Consts.fake;
                body = Consts.fake;
              } else {
                projectId = newProjectId;
                propertyKey = Consts.dummyKey;
                body = Consts.dummyData;
              }
              a.putRestapi2projectprojectIdOrKeypropertiespropertyKey(projectId, propertyKey, body, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2projectprojectIdOrKeypropertiespropertyKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let projectId;
              let propertyKey;
              if (stub) {
                projectId = Consts.fake;
                propertyKey = Consts.fake;
              } else {
                projectId = newProjectId;
                propertyKey = Consts.dummyKey;
              }
              a.getRestapi2projectprojectIdOrKeypropertiespropertyKey(projectId, propertyKey, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('eiusmod in', data.response.key);
                    assert.equal('object', typeof data.response.value);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2projectprojectIdOrKeypropertiespropertyKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let projectId;
              let propertyKey;
              if (stub) {
                projectId = Consts.fake;
                propertyKey = Consts.fake;
              } else {
                projectId = newProjectId;
                propertyKey = Consts.dummyKey;
              }
              a.deleteRestapi2projectprojectIdOrKeypropertiespropertyKey(projectId, propertyKey, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2projectprojectIdOrKey - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let projectId;
              if (stub) {
                projectId = Consts.fake;
              } else {
                projectId = newProjectId;
              }
              a.deleteRestapi2projectprojectIdOrKey(projectId, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }
                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Group Entity', () => {
        describe('#postRestapi2group- errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let body;
              if (stub) {
                body = Consts.fake;
              } else {
                body = {
                  name: Consts.dummyGroupName
                };
              }
              a.postRestapi2group(body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('sed sunt fugiat sit est', data.response.name);
                    assert.equal('https://JgIAU.jwsfdtCPcADc6nFw5Zx.H3YqE.nxiMpECenrm.r', data.response.self);
                    assert.equal('object', typeof data.response.users);
                    assert.equal('labore ullamco voluptate', data.response.expand);
                  } else {
                    if (error) {
                      console.log(error);
                    }
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2group - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let groupname;
              let includeInactiveUsers;
              if (stub) {
                groupname = Consts.fake;
                includeInactiveUsers = Consts.fake;
              } else {
                groupname = Consts.dummyGroupName;
              }
              a.getRestapi2group(groupname, includeInactiveUsers, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('sunt ea in in', data.response.name);
                    assert.equal('http://Bnc.cxaesF,sR055IBn+XCpIGiDjRMVSPteXgIEGknzQ1psw,aotgze1h', data.response.self);
                    assert.equal('object', typeof data.response.users);
                    assert.equal('pariatur cillum non', data.response.expand);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postRestapi2groupuser - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let groupname;
              let body;
              if (stub) {
                groupname = Consts.fake;
                body = Consts.fake;
              } else {
                groupname = Consts.dummyGroupName;
                body = {
                  accountId: myUserId
                };
              }
              a.postRestapi2groupuser(groupname, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('mollit Duis', data.response.name);
                    assert.equal('http://zWGqhmZaBvZlLyDjanDPzaBeGG.zrXwUJI1Iyrv.U1MPjvCnA9iUsfr.VYV.punMJpngmfEY8l', data.response.self);
                    assert.equal('object', typeof data.response.users);
                    assert.equal('do ea Ut sunt', data.response.expand);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2groupmember - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let groupname;
              let includeInactiveUsers;
              let startAt;
              let maxResults;
              if (stub) {
                groupname = Consts.fake;
                includeInactiveUsers = Consts.fake;
                startAt = Consts.fake;
                maxResults = Consts.fake;
              } else {
                groupname = Consts.dummyGroupName;
              }
              a.getRestapi2groupmember(groupname, includeInactiveUsers, startAt, maxResults, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('http://snEIRNDPRSwqX.optuDcUcamUGfQoSJiRuKF1ZIvE9kMfCsR', data.response.self);
                    assert.equal('http://ilDtELsMsIDdOfZP.tzmkPV6ilZwRknvaZutVXtHY9niSrDqJhMyeK2YrBtj', data.response.nextPage);
                    assert.equal(-82690568, data.response.maxResults);
                    assert.equal(-87773747, data.response.startAt);
                    assert.equal(-50886375, data.response.total);
                    assert.equal(true, data.response.isLast);
                    assert.equal(true, Array.isArray(data.response.values));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2groupuser - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let groupname;
              let user;
              let accountId;
              if (stub) {
                groupname = Consts.fake;
                user = Consts.fake;
                accountId = Consts.fake;
              } else {
                groupname = Consts.dummyGroupName;
                accountId = myUserId;
              }
              a.deleteRestapi2groupuser(groupname, user, accountId, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2group - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let groupname;
              let swapGroup;
              if (stub) {
                groupname = Consts.fake;
                swapGroup = Consts.fake;
              } else {
                groupname = Consts.dummyGroupName;
                swapGroup = false;
              }
              a.deleteRestapi2group(groupname, swapGroup, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('User Entity', () => {
        let newIssueKey;
        mocha.before(() => {
          if (!stub) {
            const body = {
              fields: {
                summary: 'issue comment test issue for integration test',
                project: {
                  id: testProjectId
                },
                issuetype: {
                  id: Consts.issuetypeId
                }
              }
            };
            return new Promise((resolve) => {
              const updateHistory = false;
              a.postRestapi2issue(updateHistory, body, (data) => {
                newIssueKey = data.response.key;
                resolve(data);
              });
            });
          }
          return true;
        });

        mocha.after(() => {
          if (!stub) {
            const issueIdOrKey = newIssueKey;
            const deleteSubtasks = true;
            return new Promise((resolve) => {
              a.deleteRestapi2issueissueIdOrKey(issueIdOrKey, deleteSubtasks, (data) => {
                resolve(data);
              });
            });
          }
          return true;
        });

        describe('#getRestapi2userassignablesearch - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let query;
              let user;
              let accountId;
              let project;
              let issueKey;
              let startAt;
              let maxResults;
              let actionDescriptorId;
              if (stub) {
                query = Consts.fake;
                accountId = Consts.fake;
                project = Consts.fake;
                issueKey = Consts.fake;
                startAt = Consts.fake;
                maxResults = Consts.fake;
                actionDescriptorId = Consts.fake;
              } else {
                issueKey = newIssueKey;
              }
              a.getRestapi2userassignablesearch(query, user, accountId, project, issueKey, startAt, maxResults, actionDescriptorId, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2user - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let accountId;
              let user;
              let key;
              let expand;

              if (stub) {
                accountId = Consts.fake;
                user = Consts.fake;
                key = Consts.fake;
                expand = Consts.fake;
              } else {
                accountId = myUserId;
              }
              a.getRestapi2user(accountId, user, key, expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('https://sj.rmm0bUDJ,as3Xh,cUUHblv9FG9V6flgwVuj5Rw4flid3u', data.response.self);
                    assert.equal('sunt in esse occaecat', data.response.key);
                    assert.equal('fugiat ut', data.response.accountId);
                    assert.equal('app', data.response.accountType);
                    assert.equal('ex sed', data.response.name);
                    assert.equal('cillum in aliquip minim anim', data.response.emailAddress);
                    assert.equal('object', typeof data.response.avatarUrls);
                    assert.equal('mollit aliquip', data.response.displayName);
                    assert.equal(true, data.response.active);
                    assert.equal('dolore pariatur ad dolor officia', data.response.timeZone);
                    assert.equal('in consectetur Excepteur Lorem', data.response.locale);
                    assert.equal('object', typeof data.response.groups);
                    assert.equal('object', typeof data.response.applicationRoles);
                    assert.equal('amet esse aute ad', data.response.expand);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2userbulk - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let startAt;
              let maxResults;
              let user;
              let key;
              let accountId;
              if (stub) {
                startAt = Consts.fake;
                maxResults = Consts.fake;
                user = Consts.fake;
                key = Consts.fake;
                accountId = Consts.fake;
              } else {
                accountId = [myUserId];
              }
              a.getRestapi2userbulk(startAt, maxResults, user, key, accountId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('http://lmlMxMOxmiUsewxEzSSNOFe.bcfgvEAlGoa+J', data.response.self);
                    assert.equal('http://xjoQNtxlWFppSLjRUvdOvdRPdMKrd.mjieKHx', data.response.nextPage);
                    assert.equal(-73379834, data.response.maxResults);
                    assert.equal(-12847176, data.response.startAt);
                    assert.equal(84339191, data.response.total);
                    assert.equal(true, data.response.isLast);
                    assert.equal(true, Array.isArray(data.response.values));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2usergroups - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let accountId;
              let user;
              let key;
              if (stub) {
                accountId = Consts.fake;
                user = Consts.fake;
                key = Consts.fake;
              } else {
                accountId = myUserId;
              }
              a.getRestapi2usergroups(accountId, user, key, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2userproperties - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let accountId;
              let user;
              let key;
              if (stub) {
                accountId = Consts.fake;
                user = Consts.fake;
                key = Consts.fake;
              } else {
                accountId = myUserId;
              }

              a.getRestapi2userproperties(accountId, user, key, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.keys));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2userassignablemultiProjectSearch - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let query;
              let user;
              let accountId;
              let projectKeys;
              let startAt;
              let maxResults;
              if (stub) {
                query = Consts.fake;
                user = Consts.fake;
                accountId = Consts.fake;
                projectKeys = Consts.fake;
                startAt = Consts.fake;
                maxResults = Consts.fake;
              } else {
                projectKeys = [Consts.dummyProjectKey];
              }
              a.getRestapi2userassignablemultiProjectSearch(query, user, accountId, projectKeys, startAt, maxResults, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2usersearch - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let query;
              let user;
              let accountId;
              let startAt;
              let maxResults;
              let includeActive;
              let includeInactive;
              let property;
              if (stub) {
                query = Consts.fake;
                user = Consts.fake;
                accountId = Consts.fake;
                maxResults = Consts.fake;
                includeActive = Consts.fake;
                includeInactive = Consts.fake;
                property = Consts.fake;
              } else {
                accountId = myUserId;
                query = 'test';
              }
              a.getRestapi2usersearch(query, user, accountId, startAt, maxResults, includeActive, includeInactive, property, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2userpermissionsearch - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let query;
              let user;
              let accountId;
              let permissions;
              let issueKey;
              let projectKey;
              let startAt;
              let maxResults;
              if (stub) {
                query = Consts.fake;
                user = Consts.fake;
                accountId = Consts.fake;
                permissions = Consts.fake;
                issueKey = Consts.fake;
                projectKey = Consts.fake;
                startAt = Consts.fake;
                maxResults = Consts.fake;
              } else {
                accountId = myUserId;
                issueKey = newIssueKey;
                query = 'test';
                permissions = Consts.permissionSearch;
              }
              a.getRestapi2userpermissionsearch(query, user, accountId, permissions, issueKey, projectKey, startAt, maxResults, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Myself Entity', () => {
        describe('#getRestapi2myself - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let expand;
              if (stub) {
                expand = Consts.fake;
              }
              a.getRestapi2myself(expand, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('http://VwzRDhuN.znavV-tskSNabmObazOlkmXkY+wgG', data.response.self);
                    assert.equal('sunt enim amet in', data.response.key);
                    assert.equal('laboris', data.response.accountId);
                    assert.equal('app', data.response.accountType);
                    assert.equal('labore eiusmod tempor enim', data.response.name);
                    assert.equal('velit', data.response.emailAddress);
                    assert.equal('object', typeof data.response.avatarUrls);
                    assert.equal('cillum minim sit ut dolore', data.response.displayName);
                    assert.equal(false, data.response.active);
                    assert.equal('exercitation Duis velit laboris', data.response.timeZone);
                    assert.equal('dolor non tempor', data.response.locale);
                    assert.equal('object', typeof data.response.groups);
                    assert.equal('object', typeof data.response.applicationRoles);
                    assert.equal('ullamco eu', data.response.expand);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Status Entity', () => {
        let statusId;
        let categoryId;
        describe('#getRestapi2status - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              a.getRestapi2status((data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    statusId = data.response[0].id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2statusidOrName - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let id;
              if (stub) {
                id = Consts.fake;
              } else {
                id = statusId;
              }
              a.getRestapi2statusidOrName(id, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('consequat id ipsum', data.response.self);
                    assert.equal('minim amet', data.response.description);
                    assert.equal('occaecat et labore nulla eiusmod', data.response.iconUrl);
                    assert.equal('fugiat labore sunt eiusmod', data.response.name);
                    assert.equal('enim Duis ad pariatur sit', data.response.id);
                    assert.equal('object', typeof data.response.statusCategory);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2statuscategory - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              a.getRestapi2statuscategory((data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    categoryId = data.response[0].id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2statuscategoryidOrKey - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let id;
              if (stub) {
                id = Consts.fake;
              } else {
                id = categoryId;
              }
              a.getRestapi2statuscategoryidOrKey(id, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('ullamco exercitation anim eu esse', data.response.self);
                    assert.equal(39456113, data.response.id);
                    assert.equal('anim eu in sit ex', data.response.key);
                    assert.equal('cupidatat', data.response.colorName);
                    assert.equal('et in deserunt', data.response.name);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Permission Entity', () => {
        let newProjectId;
        mocha.before(() => {
          if (!stub) {
            return new Promise((resolve) => {
              const body = {
                key: Consts.dummyProjectKey2,
                name: Consts.dummyProjectName2,
                projectTypeKey: Consts.projectType,
                leadAccountId: myUserId
              };
              a.postRestapi2project(body, (data, error) => {
                if (error) {
                  log(error);
                }
                newProjectId = data.response.id;
                resolve(data);
              });
            });
          }
          return true;
        });

        mocha.after(() => {
          if (!stub) {
            return new Promise((resolve) => {
              a.deleteRestapi2projectprojectIdOrKey(newProjectId, (data, error) => {
                if (error) {
                  log(error);
                }
                resolve(data);
              });
            });
          }
          return true;
        });

        describe('#getRestapi2mypermissions - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let projectKey;
              let projectId;
              let issueKey;
              let issueId;
              let permissions;
              if (stub) {
                projectKey = Consts.fake;
                projectId = Consts.fake;
                issueKey = Consts.fake;
                issueId = Consts.fake;
                permissions = Consts.fake;
              } else {
                projectId = newProjectId;
              }

              a.getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.permissions);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2permissions - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              a.getRestapi2permissions((data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.permissions);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Issue Type Entity', () => {
        let newIssueTypeId;
        describe('#getRestapi2issuetype - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              a.getRestapi2issuetype((data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postRestapi2issuetype - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let body;
              if (stub) {
                body = Consts.fake;
              } else {
                body = {
                  name: Consts.dummyIssueTypeName,
                  description: Consts.loremIpsum,
                  type: Consts.issueType
                };
              }
              a.postRestapi2issuetype(body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('magna dolore non sint nulla', data.response.self);
                    assert.equal('consequat aute', data.response.id);
                    assert.equal('adipisicing in laborum ad magna', data.response.description);
                    assert.equal('ex in', data.response.iconUrl);
                    assert.equal('et reprehenderit', data.response.name);
                    assert.equal(false, data.response.subtask);
                    assert.equal(82875654, data.response.avatarId);
                    assert.equal('object', typeof data.response.scope);
                  } else {
                    newIssueTypeId = data.response.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getRestapi2issuetypeid - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueTypeId;
              if (stub) {
                issueTypeId = Consts.fake;
              } else {
                issueTypeId = newIssueTypeId;
              }
              a.getRestapi2issuetypeid(issueTypeId, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('ut in consequat eu laboris', data.response.self);
                    assert.equal('nisi non', data.response.id);
                    assert.equal('irure laborum cillum', data.response.description);
                    assert.equal('Excepteur dolore irure', data.response.iconUrl);
                    assert.equal('ipsum nisi qui commodo', data.response.name);
                    assert.equal(false, data.response.subtask);
                    assert.equal(72535884, data.response.avatarId);
                    assert.equal('object', typeof data.response.scope);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#putRestapi2issuetypeid - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let issueTypeId;
              let body;
              if (stub) {
                issueTypeId = Consts.fake;
                body = Consts.fake;
              } else {
                issueTypeId = newIssueTypeId;
                body = {
                  name: Consts.dummyIssueTypeNameUpdated
                };
              }
              a.putRestapi2issuetypeid(issueTypeId, body, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('dolore', data.response.self);
                    assert.equal('sed elit nisi tempor', data.response.id);
                    assert.equal('sint proident', data.response.description);
                    assert.equal('sunt sint', data.response.iconUrl);
                    assert.equal('consequat deserunt commodo nostrud eiusmod', data.response.name);
                    assert.equal(true, data.response.subtask);
                    assert.equal(-3372010, data.response.avatarId);
                    assert.equal('object', typeof data.response.scope);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#deleteRestapi2issuetypeid - errors', () => {
          it('should work if integrated but since no mockdata should error when run standalone', (done) => {
            try {
              let issueTypeId;
              let alternativeIssueTypeId;
              if (stub) {
                issueTypeId = Consts.fake;
                alternativeIssueTypeId = Consts.fake;
              } else {
                issueTypeId = newIssueTypeId;
              }

              a.deleteRestapi2issuetypeid(issueTypeId, alternativeIssueTypeId, (data, error) => {
                try {
                  if (stub) {
                    assert.notEqual(undefined, error);
                    assert.notEqual(null, error);
                    assert.equal(null, data);
                    const temp = 'no mock data for';
                    assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });
    });

    describe('#getRestapi2issuetypeidalternatives - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2issuetypeidalternatives('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issuetypeidavatar2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2issuetypeidavatar2('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Duis magna mollit', data.response.id);
                assert.equal('tempor dolor incididunt', data.response.owner);
                assert.equal(false, data.response.isSystemAvatar);
                assert.equal(false, data.response.isSelected);
                assert.equal(true, data.response.isDeletable);
                assert.equal('est Ut', data.response.fileName);
                assert.equal('object', typeof data.response.urls);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2securitylevelid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2securitylevelid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('tempor est incididunt deserunt aute', data.response.self);
                assert.equal('sunt ut aliqua labore', data.response.id);
                assert.equal('minim ut non', data.response.description);
                assert.equal('dolor Excepteur do', data.response.name);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issuebulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2issuebulk('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.issues));
                assert.equal(true, Array.isArray(data.response.errors));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeychangelog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeychangelog('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://XVIVDuV.ttrshV1uOfX.lwjW.4pa-CNZmfNvzXakz1bKjj9X1X+R', data.response.self);
                assert.equal('https://EPSnZmTLmuSwzrGRXHJumbujqHfmPOs.rkcuZioL2wJOYUs9jAF1zMMxjM56ycD0bECyQEMN2CQOvMcDDMpdm', data.response.nextPage);
                assert.equal(-18755818, data.response.maxResults);
                assert.equal(-71565110, data.response.startAt);
                assert.equal(67664741, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyeditmeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyeditmeta('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.fields);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentmeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2attachmentmeta((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.enabled);
                assert.equal(56813708, data.response.uploadLimit);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentidexpandraw - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2attachmentidexpandraw('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal(-95743632, data.response.totalEntryCount);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2search - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2search('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('velit elit', data.response.expand);
                assert.equal(-39144584, data.response.startAt);
                assert.equal(-89141263, data.response.maxResults);
                assert.equal(-39073845, data.response.total);
                assert.equal(true, Array.isArray(data.response.issues));
                assert.equal(true, Array.isArray(data.response.warningMessages));
                assert.equal('object', typeof data.response.names);
                assert.equal('object', typeof data.response.schema);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2search - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2search('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('non velit sit Lorem ', data.response.expand);
                assert.equal(-56644528, data.response.startAt);
                assert.equal(78270684, data.response.maxResults);
                assert.equal(-97596020, data.response.total);
                assert.equal(true, Array.isArray(data.response.issues));
                assert.equal(true, Array.isArray(data.response.warningMessages));
                assert.equal('object', typeof data.response.names);
                assert.equal('object', typeof data.response.schema);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filterdefaultShareScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2filterdefaultShareScope((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('GLOBAL', data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filterdefaultShareScope - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2filterdefaultShareScope('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('GLOBAL', data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyremotelink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyremotelink('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-78238001, data.response.id);
                assert.equal('http://MCingmGdKt.urvawi4Nw7uDnhwRJbC', data.response.self);
                assert.equal('aute qui', data.response.globalId);
                assert.equal('object', typeof data.response.application);
                assert.equal('ullamco', data.response.relationship);
                assert.equal('object', typeof data.response.object);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyremotelink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyremotelink('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-94586325, data.response.id);
                assert.equal('sit', data.response.self);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyremotelink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyremotelink('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyremotelinklinkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyremotelinklinkId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-39833307, data.response.id);
                assert.equal('https://pRfFQhNeNHNqPdfJvtegBJTLBGgsWbN.vhrH6Z21RtD0oB8HY5uYL++qyy-', data.response.self);
                assert.equal('irure lab', data.response.globalId);
                assert.equal('object', typeof data.response.application);
                assert.equal('in', data.response.relationship);
                assert.equal('object', typeof data.response.object);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyremotelinklinkId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyremotelinklinkId('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyremotelinklinkId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyremotelinklinkId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuesecurityschemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issuesecurityschemes((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.issueSecuritySchemes));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuesecurityschemesid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issuesecurityschemesid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('et aute Lorem', data.response.self);
                assert.equal(31664874, data.response.id);
                assert.equal('ut culpa irure fugiat', data.response.name);
                assert.equal('ullamco sed', data.response.description);
                assert.equal(-19531153, data.response.defaultSecurityLevelId);
                assert.equal(true, Array.isArray(data.response.levels));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetypeissueTypeIdproperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issuetypeissueTypeIdproperties('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.keys));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuetypeissueTypeIdpropertiespropertyKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('cillum qui minim', data.response.key);
                assert.equal('object', typeof data.response.value);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issuetypeissueTypeIdpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyvotes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyvotes('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://FBZkenJPhriMzyiAfbVOhRqvfuKuziTST.eyKKWV9yOsc-hUMhJA8LrWrN', data.response.self);
                assert.equal(72509988, data.response.votes);
                assert.equal(true, data.response.hasVoted);
                assert.equal(true, Array.isArray(data.response.voters));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyvotes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyvotes('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyvotes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyvotes('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeywatchers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeywatchers('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Duis', data.response.self);
                assert.equal(true, data.response.isWatching);
                assert.equal(-2173642, data.response.watchCount);
                assert.equal(true, Array.isArray(data.response.watchers));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeywatchers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeywatchers('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeywatchers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeywatchers('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklog('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(77215925, data.response.startAt);
                assert.equal(-3530177, data.response.maxResults);
                assert.equal(-93708896, data.response.total);
                assert.equal(true, Array.isArray(data.response.worklogs));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyworklog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2issueissueIdOrKeyworklog('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://Pr.xvEvLgYhgjqFNnta-muCupcSkpDcDiTlZO3q7fe52VUp1cuBuly,XUK4DiM4,mPQyop', data.response.self);
                assert.equal('object', typeof data.response.author);
                assert.equal('object', typeof data.response.updateAuthor);
                assert.equal('nisi ad', data.response.comment);
                assert.equal('2017-06-18T21:19:14.561Z', data.response.created);
                assert.equal('1956-07-13T02:12:29.835Z', data.response.updated);
                assert.equal('object', typeof data.response.visibility);
                assert.equal('1971-02-11T23:09:38.524Z', data.response.started);
                assert.equal('est nisi veniam do', data.response.timeSpent);
                assert.equal(94065525, data.response.timeSpentSeconds);
                assert.equal('ut consectetur deserunt', data.response.id);
                assert.equal('ad esse', data.response.issueId);
                assert.equal(true, Array.isArray(data.response.properties));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklogid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://rWXRFqrhjShxkLS.kjdtgy', data.response.self);
                assert.equal('object', typeof data.response.author);
                assert.equal('object', typeof data.response.updateAuthor);
                assert.equal('ut elit est velit voluptate', data.response.comment);
                assert.equal('1987-02-26T09:14:46.484Z', data.response.created);
                assert.equal('2014-01-25T03:27:45.540Z', data.response.updated);
                assert.equal('object', typeof data.response.visibility);
                assert.equal('1965-05-07T06:52:11.755Z', data.response.started);
                assert.equal('consequat aute', data.response.timeSpent);
                assert.equal(74983739, data.response.timeSpentSeconds);
                assert.equal('ad est', data.response.id);
                assert.equal('eu eiusmod in dolor', data.response.issueId);
                assert.equal(true, Array.isArray(data.response.properties));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyworklogid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogid('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://smSsmfISPyKNvQrkHbstSULvkQXAWrP.foXXS+bZ8Rn', data.response.self);
                assert.equal('object', typeof data.response.author);
                assert.equal('object', typeof data.response.updateAuthor);
                assert.equal('non eiusmod laboris', data.response.comment);
                assert.equal('1992-03-17T01:11:19.522Z', data.response.created);
                assert.equal('1942-09-04T20:35:33.204Z', data.response.updated);
                assert.equal('object', typeof data.response.visibility);
                assert.equal('1955-06-19T21:31:53.397Z', data.response.started);
                assert.equal('ea aliquip sunt officia', data.response.timeSpent);
                assert.equal(-49922704, data.response.timeSpentSeconds);
                assert.equal('deserunt', data.response.id);
                assert.equal('enim aliqu', data.response.issueId);
                assert.equal(true, Array.isArray(data.response.properties));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyworklogid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogid('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2worklogdeleted - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2worklogdeleted('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(-75952862, data.response.since);
                assert.equal(35076093, data.response.until);
                assert.equal('https://smGzFiRjKclUvIJqtxjLO.ksjMvqSmXgR9B2IjY0fISCdaSS-djkV0UR,lfJcHXF9SrOp26HyRSQ0ujc+Bh', data.response.self);
                assert.equal('http://beYQNaTtIaM.ysyzQ2zCwqXDdqqe2r0ah0ZLAp0xjjVa.Qg', data.response.nextPage);
                assert.equal(false, data.response.lastPage);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workloglist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2workloglist('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2worklogupdated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2worklogupdated('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(68021549, data.response.since);
                assert.equal(20760180, data.response.until);
                assert.equal('https://XGPU.alfxwN.Z7RbRFKGxwaTXeZDzbq1W3qM-PD-TuxVkp0LDH', data.response.self);
                assert.equal('https://YPKAAzB.vkZV8hqy', data.response.nextPage);
                assert.equal(true, data.response.lastPage);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklogworklogIdproperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdproperties('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.keys));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('in', data.response.key);
                assert.equal('object', typeof data.response.value);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueLinkType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueLinkType((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.issueLinkTypes));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueLinkType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2issueLinkType('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('culpa eu in est proident', data.response.id);
                assert.equal('ea', data.response.name);
                assert.equal('laborum', data.response.inward);
                assert.equal('dolore ullamco cupidatat', data.response.outward);
                assert.equal('http://hQwztIEMBstMDCnkJfCZUa.fqurUG0v6,ZUwS8GHfY', data.response.self);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueLinkTypeissueLinkTypeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueLinkTypeissueLinkTypeId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('vol', data.response.id);
                assert.equal('sit', data.response.name);
                assert.equal('sint', data.response.inward);
                assert.equal('aliquip', data.response.outward);
                assert.equal('https://FXNUWuOUnoAorRBCXAOILU.tzH.ToQcve', data.response.self);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issueLinkTypeissueLinkTypeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2issueLinkTypeissueLinkTypeId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('laborum velit', data.response.id);
                assert.equal('Lorem', data.response.name);
                assert.equal('reprehenderit et qui aliquip Duis', data.response.inward);
                assert.equal('aliqua adipisicing ex', data.response.outward);
                assert.equal('http://ITzQbBiRZoNFWVtmBRfeZZbHq.jhagzqdYNu05aUt9yg8i', data.response.self);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueLinkTypeissueLinkTypeId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueLinkTypeissueLinkTypeId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2user - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          let body;
          if (stub) {
            body = Consts.fake;
          }
          a.postRestapi2user(body, (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://mkFneHKYVqcqMReT.nuluYjAp+UQpZTF.CzYNGd7', data.response.self);
                assert.equal('magna dolore deserunt et sit', data.response.key);
                assert.equal('reprehenderit ex fugiat', data.response.accountId);
                assert.equal('customer', data.response.accountType);
                assert.equal('aute reprehenderit dolore non', data.response.name);
                assert.equal('ipsum mollit', data.response.emailAddress);
                assert.equal('object', typeof data.response.avatarUrls);
                assert.equal('sunt consequat', data.response.displayName);
                assert.equal(false, data.response.active);
                assert.equal('adipisicing consectetur eiusmod dolore', data.response.timeZone);
                assert.equal('fugiat commodo', data.response.locale);
                assert.equal('object', typeof data.response.groups);
                assert.equal('object', typeof data.response.applicationRoles);
                assert.equal('labore incididunt enim laborum', data.response.expand);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2user - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          let accountId;
          let user;
          let key;
          if (stub) {
            accountId = Consts.fake;
            user = Consts.fake;
            key = Consts.fake;
          }
          a.deleteRestapi2user(accountId, user, key, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usersearchquerykey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2usersearchquerykey('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://ZJObEwVYhLmZFczkBRuFkqbosqh.qwfnxxJUH.9y67JxKflLh2XP', data.response.self);
                assert.equal('https://LfmzXJAfrOlCnNjlqGhDFmy.gwDQ8rmt1Q3v0YL5jSf', data.response.nextPage);
                assert.equal(38665405, data.response.maxResults);
                assert.equal(98277601, data.response.startAt);
                assert.equal(33281971, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2issueLink('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issueLinklinkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issueLinklinkId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('est culpa cillum', data.response.id);
                assert.equal('http://uNDOWbIVnWBMa.gnNSXCHPul0zxCm,.', data.response.self);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.inwardIssue);
                assert.equal('object', typeof data.response.outwardIssue);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issueLinklinkId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issueLinklinkId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filterid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2filterid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('laboris occaecat proident exercitation', data.response.name);
                assert.equal('https://zPaAWabs.wxO1GZU0k1k9GXrzoOZ4QyxcJl9ozVnJ', data.response.self);
                assert.equal('non aliquip', data.response.id);
                assert.equal('cupidatat el', data.response.description);
                assert.equal('object', typeof data.response.owner);
                assert.equal('ut exercitation aute', data.response.jql);
                assert.equal('https://vVXqUzRemJ.ympff4+RtoUeINB.Ztl9kAQ-o7YnZELc4cmGwQsoFrDysPIMpUnfDK', data.response.viewUrl);
                assert.equal('https://uAluWdHGMMFwOaqBrCiIFyoa.ywhnqCApgR-c9crRXgivj3f7KYsk,-FFs,uZd44hTx8nsQi2qgylE3Bd3cO', data.response.searchUrl);
                assert.equal(true, data.response.favourite);
                assert.equal(-86140029, data.response.favouritedCount);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('object', typeof data.response.sharedUsers);
                assert.equal('object', typeof data.response.subscriptions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filteridcolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2filteridcolumns('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filteridcolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2filteridcolumns('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filteridcolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2filteridcolumns('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filteridfavourite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2filteridfavourite('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('cupidatat eu ut veniam sed', data.response.name);
                assert.equal('https://KBEvRHhlnRQwY.vfEiDZUy9q5HDEBZS+jFoCUayAvlm8TajmCxAeUAbxy', data.response.self);
                assert.equal('occaecat deserunt aliquip', data.response.id);
                assert.equal('ut reprehenderit', data.response.description);
                assert.equal('object', typeof data.response.owner);
                assert.equal('', data.response.jql);
                assert.equal('https://XEpPSdLDJuCwcZPeFRUXqgVpUCPm.lzNxLgNb-yIQoef7vBb.lk,eTE2VG1LHhFVd64H8gvRi5HvjF', data.response.viewUrl);
                assert.equal('http://PvHqyGSUKyUopbvsGkhvUVyzkuc.mmU2EJj,11+jlmdhBuMgmpzVzHMr,tG4T', data.response.searchUrl);
                assert.equal(false, data.response.favourite);
                assert.equal(79461678, data.response.favouritedCount);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('object', typeof data.response.sharedUsers);
                assert.equal('object', typeof data.response.subscriptions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filteridfavourite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRestapi2filteridfavourite('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('laboris', data.response.name);
                assert.equal('https://BvPnXHYO.metL5nmDo6w1Ckf5Vv3OHA', data.response.self);
                assert.equal('qui mollit', data.response.id);
                assert.equal('sint in irure id dolore', data.response.description);
                assert.equal('object', typeof data.response.owner);
                assert.equal('nulla minim ex id dolore', data.response.jql);
                assert.equal('https://SbElQyvKzNiCj.lgkmY87-3tT+C2txgPgBKq7GVXEA4CsRQRIHi2+EOQ.T3qC22IjwuQTOk7', data.response.viewUrl);
                assert.equal('http://ksXgYSkMMETVwEnDkRlw.flJAPFb6ZDVaeUkFTYHrxjzUjXJW8rEsOv1PM9axo9YttKbQrHaCkIjNrmGWgYTMXZ2n', data.response.searchUrl);
                assert.equal(true, data.response.favourite);
                assert.equal(-28272996, data.response.favouritedCount);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('object', typeof data.response.sharedUsers);
                assert.equal('object', typeof data.response.subscriptions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filteridpermission - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2filteridpermission('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2filteridpermission - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2filteridpermission('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filteridpermissionpermissionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2filteridpermissionpermissionId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('projectRole', data.response.type);
                assert.equal(-12689625, data.response.id);
                assert.equal('object', typeof data.response.project);
                assert.equal('object', typeof data.response.role);
                assert.equal('object', typeof data.response.group);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filteridpermissionpermissionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2filteridpermissionpermissionId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2settingscolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2settingscolumns((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2settingscolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2settingscolumns('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2notificationscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2notificationscheme('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://rb.odzpgVd+bGH4eCAeypiRNGd,dJ9fndqxprzqXwH', data.response.self);
                assert.equal('https://DoZNlFUjhR.ubwtfshlxwcpUTEpvrVgB', data.response.nextPage);
                assert.equal(-82072756, data.response.maxResults);
                assert.equal(5294211, data.response.startAt);
                assert.equal(99204221, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2notificationschemeid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2notificationschemeid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('minim exerc', data.response.expand);
                assert.equal(46477409, data.response.id);
                assert.equal('enim consequat', data.response.self);
                assert.equal('fugiat sint', data.response.name);
                assert.equal('tempor in', data.response.description);
                assert.equal(true, Array.isArray(data.response.notificationSchemeEvents));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2issuepicker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2issuepicker('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.sections));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2expressioneval - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2expressioneval('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.value);
                assert.equal('object', typeof data.response.meta);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2applicationProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2applicationProperties('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2applicationPropertiesadvancedSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2applicationPropertiesadvancedSettings((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2applicationPropertiesid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2applicationPropertiesid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('sit aute incididunt', data.response.id);
                assert.equal('aliquip irure', data.response.key);
                assert.equal('magna ut', data.response.value);
                assert.equal('qui', data.response.name);
                assert.equal('dolor quis', data.response.desc);
                assert.equal('Ut mollit do ut occaecat', data.response.type);
                assert.equal('in consequat pariatur', data.response.defaultValue);
                assert.equal('commodo ad exercitation adipisicing', data.response.example);
                assert.equal(true, Array.isArray(data.response.allowedValues));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configuration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2configuration((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.votingEnabled);
                assert.equal(false, data.response.watchingEnabled);
                assert.equal(true, data.response.unassignedIssuesAllowed);
                assert.equal(true, data.response.subTasksEnabled);
                assert.equal(false, data.response.issueLinkingEnabled);
                assert.equal(false, data.response.timeTrackingEnabled);
                assert.equal(true, data.response.attachmentsEnabled);
                assert.equal('object', typeof data.response.timeTrackingConfiguration);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2jqlautocompletedata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2jqlautocompletedata((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.visibleFieldNames));
                assert.equal(true, Array.isArray(data.response.visibleFunctionNames));
                assert.equal(true, Array.isArray(data.response.jqlReservedWords));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2jqlautocompletedatasuggestions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2jqlautocompletedatasuggestions('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2jqlpdcleaner - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2jqlpdcleaner('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.queryStrings));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2mypreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2mypreferences('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2mypreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2mypreferences('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2mypreferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2mypreferences('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2mypreferenceslocale - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2mypreferenceslocale((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('dolore fugiat mollit', data.response.locale);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2mypreferenceslocale - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2mypreferenceslocale('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2mypreferenceslocale - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2mypreferenceslocale((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2mypermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2mypermissions('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.permissions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2permissions((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.permissions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionscheck - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2permissionscheck('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.globalPermissions));
                assert.equal(true, Array.isArray(data.response.projectPermissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionsproject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2permissionsproject('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.projects));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2permissionscheme('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.permissionSchemes));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2permissionscheme('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('deserunt', data.response.name);
                assert.equal('in pariatur esse occaecat in', data.response.expand);
                assert.equal(-64508683, data.response.id);
                assert.equal('https://WeJt.cxtC2p6L4r8k95hTKmFcQajFM.45PQlkgTQdXQb.-H0bK7,O2n', data.response.self);
                assert.equal('eu in', data.response.description);
                assert.equal('object', typeof data.response.scope);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionschemeschemeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2permissionschemeschemeId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('consectetur do enim non velit', data.response.name);
                assert.equal('quis commodo sit', data.response.expand);
                assert.equal(-35357867, data.response.id);
                assert.equal('https://qrf.wqrQSO6Xu2RRQSWR', data.response.self);
                assert.equal('pariatur cillum', data.response.description);
                assert.equal('object', typeof data.response.scope);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2permissionschemeschemeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2permissionschemeschemeId('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Excepteur', data.response.name);
                assert.equal('anim Duis dolore', data.response.expand);
                assert.equal(37386883, data.response.id);
                assert.equal('http://eJpefxCZEOpuDCOLVABvD.rpoOu3O-Z6z70QZLX5T++6mOPL9eyKuqDBElTW,fhPD74CeMVyWgxda1,DtoXs', data.response.self);
                assert.equal('proident cupidatat in enim', data.response.description);
                assert.equal('object', typeof data.response.scope);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2permissionschemeschemeId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2permissionschemeschemeId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionschemeschemeIdpermission - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2permissionschemeschemeIdpermission('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.permissions));
                assert.equal('ut minim culpa in', data.response.expand);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionschemeschemeIdpermission - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2permissionschemeschemeIdpermission('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(28424868, data.response.id);
                assert.equal('http://GttJCD.xqRrEOIyGcR5OpCKGoDX-qyXwlz', data.response.self);
                assert.equal('object', typeof data.response.holder);
                assert.equal('ut dolore in', data.response.permission);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2permissionschemeschemeIdpermissionpermissionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2permissionschemeschemeIdpermissionpermissionId('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-75138000, data.response.id);
                assert.equal('http://fyhioKOwbMzQSDrTiYt.our9mqpl0BndnNLRQUP8rRk.kr3yiFnZ+8SLIIwCrXbs3d+', data.response.self);
                assert.equal('object', typeof data.response.holder);
                assert.equal('quis minim', data.response.permission);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2permissionschemeschemeIdpermissionpermissionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2permissionschemeschemeIdpermissionpermissionId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdnotificationscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdnotificationscheme('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('amet ad incididunt Ut in', data.response.expand);
                assert.equal(-59793793, data.response.id);
                assert.equal('in irure dolore ut', data.response.self);
                assert.equal('ad amet occaecat fugiat', data.response.name);
                assert.equal('pariatur veniam', data.response.description);
                assert.equal(true, Array.isArray(data.response.notificationSchemeEvents));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeytypenewProjectTypeKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeytypenewProjectTypeKey('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('eiusmod consequat aliquip magna do', data.response.expand);
                assert.equal('https://mJspyVxcHFMXyCSneRmHCMuhIXFUULXmV.airprOQpiAjW+qA+sijwsfC5wfYCpE.lWn,1H+ICeWJ', data.response.self);
                assert.equal('laborum ut eu', data.response.id);
                assert.equal('laborum incididunt commodo consequat elit', data.response.key);
                assert.equal('i', data.response.description);
                assert.equal('object', typeof data.response.lead);
                assert.equal(true, Array.isArray(data.response.components));
                assert.equal(true, Array.isArray(data.response.issueTypes));
                assert.equal('aliqua velit', data.response.url);
                assert.equal('veniam pariatur in', data.response.email);
                assert.equal('PROJECT_LEAD', data.response.assigneeType);
                assert.equal(true, Array.isArray(data.response.versions));
                assert.equal('et tempor', data.response.name);
                assert.equal('object', typeof data.response.roles);
                assert.equal('object', typeof data.response.avatarUrls);
                assert.equal('object', typeof data.response.projectCategory);
                assert.equal('ops', data.response.projectTypeKey);
                assert.equal(false, data.response.simplified);
                assert.equal('classic', data.response.style);
                assert.equal(false, data.response.isPrivate);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdpermissionscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdpermissionscheme('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('deserunt dolore nulla culpa elit', data.response.name);
                assert.equal('magna consequat velit aute', data.response.expand);
                assert.equal(52503739, data.response.id);
                assert.equal('http://lOKGSVIMNfoQkBDADDQppyZCKXZiOov.cchhjjHV8JtMFas0dqhSu.po7upNMcIi8cp9dE2dGzbe.cdqUvtOhQ+WGgPHStwW.SEVjPuB.2p1VPl', data.response.self);
                assert.equal('laborum', data.response.description);
                assert.equal('object', typeof data.response.scope);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectKeyOrIdpermissionscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2projectprojectKeyOrIdpermissionscheme('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('ex amet sed Excepteur', data.response.name);
                assert.equal('et mollit minim officia', data.response.expand);
                assert.equal(-75049527, data.response.id);
                assert.equal('http://jWqOsHOIodMrmivIuZNbTv.psmYTJTclrX9aoJEIOFk7R6PlRL.yiwAsiGHNwZJGwd', data.response.self);
                assert.equal('ipsum ut Lorem', data.response.description);
                assert.equal('object', typeof data.response.scope);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdsecuritylevel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdsecuritylevel('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.levels));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('laboris', data.response.self);
                assert.equal(75018521, data.response.id);
                assert.equal('in nulla nost', data.response.name);
                assert.equal('ea nisi officia', data.response.description);
                assert.equal(58530132, data.response.defaultSecurityLevelId);
                assert.equal(true, Array.isArray(data.response.levels));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projecttype - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projecttype((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projecttypeprojectTypeKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projecttypeprojectTypeKey('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('ame', data.response.key);
                assert.equal('ea laborum cupidatat', data.response.formattedKey);
                assert.equal('officia deserunt occaecat', data.response.descriptionI18nKey);
                assert.equal('ullamco dolor', data.response.icon);
                assert.equal('reprehenderit est irure mollit', data.response.color);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projecttypeprojectTypeKeyaccessible - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projecttypeprojectTypeKeyaccessible('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('et occaecat', data.response.key);
                assert.equal('reprehenderit ut minim ad', data.response.formattedKey);
                assert.equal('Excepteur est reprehenderit consectetur ', data.response.descriptionI18nKey);
                assert.equal('minim', data.response.icon);
                assert.equal('aute', data.response.color);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2serverInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2serverInfo((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('irure', data.response.baseUrl);
                assert.equal('m', data.response.version);
                assert.equal(true, Array.isArray(data.response.versionNumbers));
                assert.equal('non ea', data.response.deploymentType);
                assert.equal(-95208472, data.response.buildNumber);
                assert.equal('2012-05-13T22:10:48.477Z', data.response.buildDate);
                assert.equal('1976-11-01T18:39:23.642Z', data.response.serverTime);
                assert.equal('dolore laboris id', data.response.scmInfo);
                assert.equal('nostrud sit', data.response.serverTitle);
                assert.equal(true, Array.isArray(data.response.healthChecks));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configurationtimetracking - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2configurationtimetracking((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('in Excepteur mollit en', data.response.key);
                assert.equal('cillum cupidatat id mollit consectetur', data.response.name);
                assert.equal('sed tempor', data.response.url);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2configurationtimetracking - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2configurationtimetracking('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2configurationtimetracking - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2configurationtimetracking((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configurationtimetrackinglist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2configurationtimetrackinglist((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2configurationtimetrackingoptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2configurationtimetrackingoptions((data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('day', data.response.defaultUnit);
                assert.equal('days', data.response.timeFormat);
                assert.equal(64083539.56986424, data.response.workingDaysPerWeek);
                assert.equal(-76364630.13302004, data.response.workingHoursPerDay);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2configurationtimetrackingoptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2configurationtimetrackingoptions('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('week', data.response.defaultUnit);
                assert.equal('pretty', data.response.timeFormat);
                assert.equal(-39086218.07190524, data.response.workingDaysPerWeek);
                assert.equal(-58014794.46343905, data.response.workingHoursPerDay);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userbulkmigration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2userbulkmigration('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2usercolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2usercolumns('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2usercolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2usercolumns('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2usercolumns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2usercolumns('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userpropertiespropertyKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2userpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Lorem exercitation', data.response.key);
                assert.equal('object', typeof data.response.value);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2userpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2userpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2userpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2userpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userpicker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2userpicker('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(46382440, data.response.total);
                assert.equal('elit ipsum esse est commodo', data.response.header);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2userviewissuesearch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2userviewissuesearch('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2workflow('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workflowscheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2workflowscheme('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-61154472, data.response.id);
                assert.equal('pariatur irure cupidatat laborum non', data.response.name);
                assert.equal('ea', data.response.description);
                assert.equal('id minim sed est exercitation', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('adipisicing irure laboris ea', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('voluptate exercitation culpa qui', data.response.lastModified);
                assert.equal('http://TezPUmHqxVimuRukcOGuZvNVWoVNK.wwvqXIhs-ujEy3rzLgubDGj1', data.response.self);
                assert.equal(false, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(82447186, data.response.id);
                assert.equal('sint ex sed', data.response.name);
                assert.equal('aute officia', data.response.description);
                assert.equal('cupidatat consequat consectetur', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('sit esse eiusmod', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('et', data.response.lastModified);
                assert.equal('http://PnLYx.qxrZWuaOMtw.c,xN7l3DWfhM7oZAvMGSOLQ2g41uNnyIo4Lzn9D8jFTt', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-77903605, data.response.id);
                assert.equal('elit culpa voluptate ut', data.response.name);
                assert.equal('pariatur Ut aliqua magna occaecat', data.response.description);
                assert.equal('laboris irure ali', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('pariatur elit', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('do incididunt', data.response.lastModified);
                assert.equal('https://sihN.cqQcTIeig1EujC5FV5jJVj8kczjfyYicJ+OYA8mYv,oebeXtkMwKXnOZERbFOShwJ,3', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2workflowschemeid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeiddefault('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('dolor nisi pariatur', data.response.workflow);
                assert.equal(false, data.response.updateDraftIfNeeded);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeiddefault('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-45178564, data.response.id);
                assert.equal('officia ut nulla aliqua', data.response.name);
                assert.equal('culpa Excepteur fugiat velit', data.response.description);
                assert.equal('occae', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('ut sed incididunt', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('ipsum', data.response.lastModified);
                assert.equal('http://uslExRDfGqbmVxTRECXBjloKcKtcajc.ekjnkmx8OdfNvMjktqLGnFOgOvjZ3Nc2w', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddefault('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-81588268, data.response.id);
                assert.equal('exercitation incididunt fugiat', data.response.name);
                assert.equal('enim ut laborum', data.response.description);
                assert.equal('dolore', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('velit Dui', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('', data.response.lastModified);
                assert.equal('http://CnhNihzPPjlopaer.dsg5mvF9F4y2Js4h9+', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeidissuetypeissueType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeidissuetypeissueType('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('non', data.response.issueType);
                assert.equal('veniam Ut nul', data.response.workflow);
                assert.equal(false, data.response.updateDraftIfNeeded);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeidissuetypeissueType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeidissuetypeissueType('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-51419152, data.response.id);
                assert.equal('reprehenderit culpa Duis', data.response.name);
                assert.equal('non nulla in quis', data.response.description);
                assert.equal('proident', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('laborum nostrud', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('eu adipisicing irure', data.response.lastModified);
                assert.equal('http://ERrWoKTNOdkQpIRTcsduBHOBoPdbYsPB.bqbuAeL6cNV4aVqnBLqcOqib4L+FnozkhsYG', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeidissuetypeissueType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRestapi2workflowschemeidissuetypeissueType('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(56716545, data.response.id);
                assert.equal('non id', data.response.name);
                assert.equal('fugiat deserunt aute', data.response.description);
                assert.equal('elit', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('Lorem fugiat exercitation', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('Ut', data.response.lastModified);
                assert.equal('https://uhopVvIxBnGRwWsmKjbsAVEosjedTYniR.xmvCqfVi0xNd', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeidworkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeidworkflow('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('nisi sed', data.response.workflow);
                assert.equal(true, Array.isArray(data.response.issueTypes));
                assert.equal(false, data.response.defaultMapping);
                assert.equal(true, data.response.updateDraftIfNeeded);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeidworkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeidworkflow('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-91588604, data.response.id);
                assert.equal('ex dolor consequat', data.response.name);
                assert.equal('consectetur dolore Excepteur sit', data.response.description);
                assert.equal('culpa Excepteur nisi magna sed', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('est tempor nulla dolore', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('enim commodo aliquip cillum', data.response.lastModified);
                assert.equal('http://Qnxfm.drkjkeE2mPPUKnB6-Zn7jwNn1vLAUIkIfudMBkNdOTiwwvZvQ', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeidworkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2workflowschemeidworkflow('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workflowschemeidcreatedraft - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2workflowschemeidcreatedraft('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-68857735, data.response.id);
                assert.equal('occaecat', data.response.name);
                assert.equal('', data.response.description);
                assert.equal('pariatur exercitation proident minim', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('nulla', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('nisi aute eiusmod', data.response.lastModified);
                assert.equal('https://MVauRWbqSgsjpOMl.fwvUzrODuPeJGH,JMwh6bwJpsMfWuu-xvkC4JvWKaURHDtH', data.response.self);
                assert.equal(false, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraft - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeiddraft('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(43278529, data.response.id);
                assert.equal('aute quis laboris', data.response.name);
                assert.equal('consectet', data.response.description);
                assert.equal('anim elit eiusmod', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('off', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('enim Lorem', data.response.lastModified);
                assert.equal('https://HrphvpSyLjZaEZZwlMPAIsBdSs.qypnd6kK3X4SIdJOasv', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraft - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeiddraft('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-33159986, data.response.id);
                assert.equal('do enim Ut officia dolore', data.response.name);
                assert.equal('eu Lorem', data.response.description);
                assert.equal('qui quis ullamco in', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('ut in fugiat', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('laborum', data.response.lastModified);
                assert.equal('https://VwvWRyecAXGHBevoywoqGiCi.trARXjtMQRkQ3KaSs-yNrS3,mghW3It.nks3nmK9pq+GDwJ4fJAK,pm', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraft - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraft('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraftdefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftdefault('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('deserunt dolor Ut et', data.response.workflow);
                assert.equal(true, data.response.updateDraftIfNeeded);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraftdefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftdefault('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(68715583, data.response.id);
                assert.equal('elit', data.response.name);
                assert.equal('officia aute ut consectetur', data.response.description);
                assert.equal('ullamco nisi laborum consequat', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('ex consectetur id', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('in adipisicing aliqua', data.response.lastModified);
                assert.equal('http://dtE.iaatNAVhRMcyDuvhDHVwCS52A', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraftdefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftdefault('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(43214452, data.response.id);
                assert.equal('esse', data.response.name);
                assert.equal('est Duis', data.response.description);
                assert.equal('minim culpa', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('dolor esse laborum', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(false, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('aliquip Excepteur', data.response.lastModified);
                assert.equal('https://MBya.fitu8pMhRQh7T', data.response.self);
                assert.equal(false, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraftissuetypeissueType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftissuetypeissueType('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('incid', data.response.issueType);
                assert.equal('officia', data.response.workflow);
                assert.equal(false, data.response.updateDraftIfNeeded);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraftissuetypeissueType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftissuetypeissueType('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-92566214, data.response.id);
                assert.equal('do nostrud Excepteur occaecat', data.response.name);
                assert.equal('elit pariatur in', data.response.description);
                assert.equal('veniam sint irure ea dolor', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('eiusmod proident', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('fugiat Ut sunt ex commodo', data.response.lastModified);
                assert.equal('http://qTpCLlWuOHQYKtnuLFWYleUsVhP.xjnY6HoMnwfyzMZz1hfoy1zOBPiIeBsDqJu1hhlLD', data.response.self);
                assert.equal(false, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraftissuetypeissueType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftissuetypeissueType('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(66512285, data.response.id);
                assert.equal('sunt est dolor', data.response.name);
                assert.equal('Excepteur', data.response.description);
                assert.equal('mollit nisi tempor', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('dolor in laborum consequat incididunt', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('nostrud veniam', data.response.lastModified);
                assert.equal('https://rlEQqRyAILcczn.lspbCdd-Sqlicdech0h,fwylsOlp', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowschemeiddraftworkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowschemeiddraftworkflow('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('culpa ex ut sunt et', data.response.workflow);
                assert.equal(true, Array.isArray(data.response.issueTypes));
                assert.equal(true, data.response.defaultMapping);
                assert.equal(false, data.response.updateDraftIfNeeded);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowschemeiddraftworkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowschemeiddraftworkflow('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(59010217, data.response.id);
                assert.equal('enim nostrud consequat', data.response.name);
                assert.equal('eu id culpa laboris', data.response.description);
                assert.equal('amet sit ut', data.response.defaultWorkflow);
                assert.equal('object', typeof data.response.issueTypeMappings);
                assert.equal('magna dolore eu occaecat exercitati', data.response.originalDefaultWorkflow);
                assert.equal('object', typeof data.response.originalIssueTypeMappings);
                assert.equal(true, data.response.draft);
                assert.equal('object', typeof data.response.lastModifiedUser);
                assert.equal('dolore', data.response.lastModified);
                assert.equal('http://eBhMwFBqR.yiriSr7', data.response.self);
                assert.equal(true, data.response.updateDraftIfNeeded);
                assert.equal('object', typeof data.response.issueTypes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowschemeiddraftworkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2workflowschemeiddraftworkflow('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2workflowtransitionstransitionIdproperties('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('dolore irure est', data.response.value);
                assert.equal('Excepteur', data.response.key);
                assert.equal('aliquip sed nostrud', data.response.id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2workflowtransitionstransitionIdproperties('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Lorem voluptate', data.response.value);
                assert.equal('consequat sunt deserunt commodo', data.response.key);
                assert.equal('adipisicing', data.response.id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2workflowtransitionstransitionIdproperties('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('ut sed eu tempor', data.response.value);
                assert.equal('elit in qui quis', data.response.key);
                assert.equal('mollit voluptate anim', data.response.id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2workflowtransitionstransitionIdproperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2workflowtransitionstransitionIdproperties('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2avatartypesystem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2avatartypesystem('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.system));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2universalAvatartypetypeownerentityId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2universalAvatartypetypeownerentityId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.system));
                assert.equal(true, Array.isArray(data.response.custom));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2universalAvatartypetypeownerentityId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2universalAvatartypetypeownerentityId('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('in tempor amet veniam est', data.response.id);
                assert.equal('ullamco', data.response.owner);
                assert.equal(false, data.response.isSystemAvatar);
                assert.equal(false, data.response.isSelected);
                assert.equal(false, data.response.isDeletable);
                assert.equal('labore elit tempor', data.response.fileName);
                assert.equal('object', typeof data.response.urls);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2commentcommentIdpropertiespropertyKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2commentcommentIdpropertiespropertyKey('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('est officia ut', data.response.key);
                assert.equal('object', typeof data.response.value);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2commentcommentIdpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2commentcommentIdpropertiespropertyKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2commentcommentIdpropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2commentcommentIdpropertiespropertyKey('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2component - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2component('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://zciunzdcmKV.rmhzSf8byQ,PyLqFRm0r0lRGU44337nNnarpxLDADXXjYsFftl1i,uDqiEO', data.response.self);
                assert.equal('voluptate adipisi', data.response.id);
                assert.equal('veniam Ut Lorem', data.response.name);
                assert.equal('fugiat labore amet', data.response.description);
                assert.equal('object', typeof data.response.lead);
                assert.equal('exercitation laboris', data.response.leadUserName);
                assert.equal('dolor proident consectetur in', data.response.leadAccountId);
                assert.equal('COMPONENT_LEAD', data.response.assigneeType);
                assert.equal('object', typeof data.response.assignee);
                assert.equal('PROJECT_LEAD', data.response.realAssigneeType);
                assert.equal('object', typeof data.response.realAssignee);
                assert.equal(false, data.response.isAssigneeTypeValid);
                assert.equal('sint ut', data.response.project);
                assert.equal(-6118233, data.response.projectId);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2componentid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2componentid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://PRCIyifPOWjdBpbfiFHOBvyaJ.dajfD+Emxh4EOQLoiDAKBPG55xsBawLRX6nV4sjxh8jeGg1aciZ+GYG+Xam', data.response.self);
                assert.equal('ea ', data.response.id);
                assert.equal('r', data.response.name);
                assert.equal('nulla', data.response.description);
                assert.equal('object', typeof data.response.lead);
                assert.equal('aute nisi', data.response.leadUserName);
                assert.equal('ea magna tempor', data.response.leadAccountId);
                assert.equal('PROJECT_LEAD', data.response.assigneeType);
                assert.equal('object', typeof data.response.assignee);
                assert.equal('PROJECT_LEAD', data.response.realAssigneeType);
                assert.equal('object', typeof data.response.realAssignee);
                assert.equal(true, data.response.isAssigneeTypeValid);
                assert.equal('irure ', data.response.project);
                assert.equal(38128131, data.response.projectId);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2componentid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2componentid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://nwIZVjgtFCUBNTxtG.vbgXLhG0uKXVSqXMbOuaJuZOkaRD.D0.wKIVKkylT.DmaoHyb', data.response.self);
                assert.equal('laborum enim', data.response.id);
                assert.equal('proident pariatur', data.response.name);
                assert.equal('qui id sint', data.response.description);
                assert.equal('object', typeof data.response.lead);
                assert.equal('esse', data.response.leadUserName);
                assert.equal('qui dolor sed mini', data.response.leadAccountId);
                assert.equal('PROJECT_LEAD', data.response.assigneeType);
                assert.equal('object', typeof data.response.assignee);
                assert.equal('UNASSIGNED', data.response.realAssigneeType);
                assert.equal('object', typeof data.response.realAssignee);
                assert.equal(true, data.response.isAssigneeTypeValid);
                assert.equal('Lorem enim', data.response.project);
                assert.equal(86509250, data.response.projectId);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2componentid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2componentid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2componentidrelatedIssueCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2componentidrelatedIssueCounts('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://nzaMBpGrq.uueizWDqs3dqrHrkTSd', data.response.self);
                assert.equal(78956431, data.response.issueCount);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeycomponent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeycomponent('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://AYqpzyFbQxVpNgavJARQzreHdZTY.dbqygFQwlNg', data.response.self);
                assert.equal('http://HToCqgJHeQkbFpBQgkml.zwjfcvcuXetVkkedwO+MUg3N5t5pvxIMsw1y3LCubCC8zT8J3tU,HXsaEp9sh0KxghB0.4', data.response.nextPage);
                assert.equal(-12514298, data.response.maxResults);
                assert.equal(38497899, data.response.startAt);
                assert.equal(-23210478, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeycomponents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeycomponents('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2customFieldOptionid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2customFieldOptionid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://feWxsHRWvYnelbCoWEhqXLiJTjbTQrw.slztJB0D3vg-k4ustH.cmuhiWw5IBnyd3BDdRjkqlj', data.response.self);
                assert.equal('voluptate dolore ullamco ex tempor', data.response.value);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2fieldfieldKeyoption - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2fieldfieldKeyoption('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-65944199, data.response.id);
                assert.equal('dolore', data.response.value);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.config);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoptionsuggestionsedit - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionsuggestionsedit('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://ZFagTfcFNkjhqFjU.dwmrbOJmhP,-WyZo+G3fNmBc31W53tJ6yj+8.i.sdeBCVjzfgyb.ForAK', data.response.self);
                assert.equal('http://Daxv.hxwcWOXNYtMFRyTpfKzP7CoxfCp8a6SWMprmJvHgpzwtDJO2Bak,kMfo3FV', data.response.nextPage);
                assert.equal(-41563129, data.response.maxResults);
                assert.equal(-16847825, data.response.startAt);
                assert.equal(62902198, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoptionsuggestionssearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionsuggestionssearch('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://kkracSvIGGiOULFKUXslyTyTpzGmDxpVF.hhlwm3XNvZFZOI9,2XCogeIHOI4Bsa72tI8iwsq0zfqrUSUz', data.response.self);
                assert.equal('https://VQtzZJzGja.wibcrdLwNtLPuIee,lP2C4XqjV3+aSWapgoLDsE22luXG', data.response.nextPage);
                assert.equal(-57065706, data.response.maxResults);
                assert.equal(-99000408, data.response.startAt);
                assert.equal(42720242, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoptionoptionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoptionoptionId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-55539526, data.response.id);
                assert.equal('consectetur sunt pariatur', data.response.value);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.config);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2fieldfieldKeyoptionoptionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2fieldfieldKeyoptionoptionId('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(19803860, data.response.id);
                assert.equal('laborum', data.response.value);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.config);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2fieldfieldKeyoptionoptionId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2fieldfieldKeyoptionoptionId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2fieldfieldKeyoptionoptionIdissue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2fieldfieldKeyoptionoptionIdissue('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2groupspicker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2groupspicker('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Duis ullamco', data.response.header);
                assert.equal(83549202, data.response.total);
                assert.equal(true, Array.isArray(data.response.groups));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2groupuserpicker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2groupuserpicker('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.users);
                assert.equal('object', typeof data.response.groups);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2priority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2priority((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2priorityid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2priorityid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('Ut ipsum proident cupidatat ullamco', data.response.self);
                assert.equal('id ullamco laboris', data.response.statusColor);
                assert.equal('Ut occaecat voluptate', data.response.description);
                assert.equal('tempor sed amet', data.response.iconUrl);
                assert.equal('anim', data.response.name);
                assert.equal('proident exercitation elit aliqua', data.response.id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeyavatar - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyavatar('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectprojectIdOrKeyavatarid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeyavatarid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2projectprojectIdOrKeyavatar2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2projectprojectIdOrKeyavatar2('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('nulla', data.response.id);
                assert.equal('proident fugiat', data.response.owner);
                assert.equal(false, data.response.isSystemAvatar);
                assert.equal(false, data.response.isSelected);
                assert.equal(true, data.response.isDeletable);
                assert.equal('pariatur officia sed deserunt occaecat', data.response.fileName);
                assert.equal('object', typeof data.response.urls);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyavatars - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyavatars('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.system));
                assert.equal(true, Array.isArray(data.response.custom));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyroleid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://ZtJaekVVaSeJdwhQHdFlkZBETk.kdsSABzDjl7z9Fea.NETHEOjGwiFtFZcReDD1kRTxFY', data.response.self);
                assert.equal('quis ut voluptate in', data.response.name);
                assert.equal(-20320490, data.response.id);
                assert.equal('in pariatur esse aliquip', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyroledetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyroledetails('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2role - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2role((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2role - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2role('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://XsneQinZpfXlndZVP.sqByyV2', data.response.self);
                assert.equal('irure', data.response.name);
                assert.equal(38487746, data.response.id);
                assert.equal('ex ad', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2roleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2roleid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://OpjSoFynhwHxZTfw.ayjvsV4N66qT5UTdvDH4DHi-H4ei,OH,rlq11MPBmM1faIT6Nep-S5ncYphjJWRdM', data.response.self);
                assert.equal('veniam exercitation ipsum est elit', data.response.name);
                assert.equal(60978589, data.response.id);
                assert.equal('sint', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2roleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2roleid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://qVENeahqYUJs.fbqpoiSqQ3de+Sus1sXc93wwY1dsFa3qniPHaEtrJqAgc+', data.response.self);
                assert.equal('in', data.response.name);
                assert.equal(65267686, data.response.id);
                assert.equal('eu elit adipisicing sit', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2roleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2roleid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://mkWIFbxgedWcbjweKBFNfgVK.lfkwXRduU.mSRRrAwHExfbdbmUal3OllIfm', data.response.self);
                assert.equal('pariatur dolore sed veniam', data.response.name);
                assert.equal(-16465440, data.response.id);
                assert.equal('sed quis aliquip', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2roleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2roleid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2projectprojectIdOrKeyroleid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://LYvDIIyMSfPuGEPEaQqbNQmlWnUSQXi.dcocjVHM+jzQElc7YcIfFu+vYKM5JImhjlr9InVVqoi+2ou,rW3hDoW.Ki0F6waVe8qEgXGF', data.response.self);
                assert.equal('non sit nulla in minim', data.response.name);
                assert.equal(94163128, data.response.id);
                assert.equal('quis eu', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2projectprojectIdOrKeyroleid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://uerXqyEIBhQRVIdXVEXdxJuN.yhrqh+K+xDxY1svPG-A+on', data.response.self);
                assert.equal('occaecat Ut dolor pariatur', data.response.name);
                assert.equal(-97175938, data.response.id);
                assert.equal('et aute eiusmod', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectprojectIdOrKeyroleid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2projectprojectIdOrKeyroleid('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2roleidactors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2roleidactors('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://eILtuUu.yvffEZwOlGYdc4SC2iBnFeYpslbRNg2FB-g+KJnmYjYkDxflyyWRp', data.response.self);
                assert.equal('commodo', data.response.name);
                assert.equal(20414195, data.response.id);
                assert.equal('consequat do irure', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2roleidactors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2roleidactors('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://QSkHCnovjIcuVeFANbB.snmfE69OuRCx2,3LEtzh5GIxfB6hmkc2uWyKTrA', data.response.self);
                assert.equal('culpa aliquip', data.response.name);
                assert.equal(-22148862, data.response.id);
                assert.equal('en', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2roleidactors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRestapi2roleidactors('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://sEPFCYKSFVUpGOvefRzweGdV.zrpiSRr8BWDInoJVqn+CX6,a0o8eLgKgqlAaHMqGGZ2e,', data.response.self);
                assert.equal('sint sunt incididunt', data.response.name);
                assert.equal(71815962, data.response.id);
                assert.equal('occaecat consequat anim', data.response.description);
                assert.equal(true, Array.isArray(data.response.actors));
                assert.equal('object', typeof data.response.scope);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyversion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyversion('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://wgqvBrvPFXUJCWXw.vgtF', data.response.self);
                assert.equal('http://zfveWzEwFCdUgsJtIaFmJWGYgnhrWr.erhC.Ukx9x+NbLqULIAMLruQpQpwN', data.response.nextPage);
                assert.equal(7684420, data.response.maxResults);
                assert.equal(34540188, data.response.startAt);
                assert.equal(64334685, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyversions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projectprojectIdOrKeyversions('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2version - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2version('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('aliqua ullamco', data.response.expand);
                assert.equal('http://coJQJEXhzhQfQsPUCV.ffwRaihZi+B2iDxF+vBpDeWuiQiUOAEWIvosEOnrU,9PwyTj+Vnks', data.response.self);
                assert.equal('deserunt ipsum adipisicing reprehenderit temp', data.response.id);
                assert.equal('culpa Lorem deserunt cillum', data.response.description);
                assert.equal('occaecat incididunt anim', data.response.name);
                assert.equal(false, data.response.archived);
                assert.equal(false, data.response.released);
                assert.equal('1981-07-23', data.response.startDate);
                assert.equal('1981-11-07', data.response.releaseDate);
                assert.equal(true, data.response.overdue);
                assert.equal('labore in tempor', data.response.userStartDate);
                assert.equal('amet velit', data.response.userReleaseDate);
                assert.equal('mollit in elit', data.response.project);
                assert.equal(-60881251, data.response.projectId);
                assert.equal('https://Hs.dscfP,IZXZgJH', data.response.moveUnfixedIssuesTo);
                assert.equal(true, Array.isArray(data.response.operations));
                assert.equal('object', typeof data.response.issuesStatusForFixVersion);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2versionid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2versionid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('deserunt aliquip labore', data.response.expand);
                assert.equal('https://iXRyWZYlDMAoufEcaOiPj.icreBCVDBvDdBLg8', data.response.self);
                assert.equal('Ut commodo', data.response.id);
                assert.equal('sit non cupidatat', data.response.description);
                assert.equal('nisi dolor aliqua', data.response.name);
                assert.equal(false, data.response.archived);
                assert.equal(true, data.response.released);
                assert.equal('1963-11-04', data.response.startDate);
                assert.equal('1967-12-26', data.response.releaseDate);
                assert.equal(true, data.response.overdue);
                assert.equal('dolor Excepteur irure in', data.response.userStartDate);
                assert.equal('consectetur minim', data.response.userReleaseDate);
                assert.equal('do dolore', data.response.project);
                assert.equal(82411738, data.response.projectId);
                assert.equal('http://Gztz.qixlO.SLBZh2OjZV,p9I', data.response.moveUnfixedIssuesTo);
                assert.equal(true, Array.isArray(data.response.operations));
                assert.equal('object', typeof data.response.issuesStatusForFixVersion);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2versionid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2versionid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('quis enim commodo labore', data.response.expand);
                assert.equal('https://ZceaQbongmCVnfQvFGzEHaaGRJ.msk-XQ3zuzEAz+qic8gKjkgof.l5', data.response.self);
                assert.equal('dese', data.response.id);
                assert.equal('cillum ea in', data.response.description);
                assert.equal('deserunt aliquip minim veniam', data.response.name);
                assert.equal(false, data.response.archived);
                assert.equal(false, data.response.released);
                assert.equal('1987-11-03', data.response.startDate);
                assert.equal('1986-02-27', data.response.releaseDate);
                assert.equal(false, data.response.overdue);
                assert.equal('dolore pariatur aute in', data.response.userStartDate);
                assert.equal('sit proident elit', data.response.userReleaseDate);
                assert.equal('eu', data.response.project);
                assert.equal(78329980, data.response.projectId);
                assert.equal('https://xyYrXWPFFLtJN.rsZv8ZJjBp63zvArcvc0', data.response.moveUnfixedIssuesTo);
                assert.equal(true, Array.isArray(data.response.operations));
                assert.equal('object', typeof data.response.issuesStatusForFixVersion);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2versionid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2versionid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2versionidmergetomoveIssuesTo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRestapi2versionidmergetomoveIssuesTo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2versionidmove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2versionidmove('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('et ipsum commodo laborum', data.response.expand);
                assert.equal('http://LvCasnZvQFPJkrKTez.vnglceNi.NUnsmsY5xjraVIDDxkPOmWboVWMcuKgefPLDCo,fO', data.response.self);
                assert.equal('dolore id ut adipisicing', data.response.id);
                assert.equal('est ut', data.response.description);
                assert.equal('non aliqua in', data.response.name);
                assert.equal(true, data.response.archived);
                assert.equal(true, data.response.released);
                assert.equal('1997-11-11', data.response.startDate);
                assert.equal('2013-11-22', data.response.releaseDate);
                assert.equal(true, data.response.overdue);
                assert.equal('labore ad fugiat in dolore', data.response.userStartDate);
                assert.equal('eu aliquip ex', data.response.userReleaseDate);
                assert.equal('dolor qui ullamco', data.response.project);
                assert.equal(93236524, data.response.projectId);
                assert.equal('https://gvYqKldDBBnZoVrsSPH.dwclDuVp4eK-aR2lnmVHJ4naDvrod79OGlA5.3XaNJBlmK', data.response.moveUnfixedIssuesTo);
                assert.equal(true, Array.isArray(data.response.operations));
                assert.equal('object', typeof data.response.issuesStatusForFixVersion);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2versionidrelatedIssueCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2versionidrelatedIssueCounts('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://ERHGIQvGDilDLXYeHvyNfnsqGBhqloKO.tjgX3,r,CvkNlg,O+2O+', data.response.self);
                assert.equal(-93163933, data.response.issuesFixedCount);
                assert.equal(11667112, data.response.issuesAffectedCount);
                assert.equal(23444759, data.response.issueCountWithCustomFieldsShowingVersion);
                assert.equal(true, Array.isArray(data.response.customFieldUsage));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2versionidremoveAndSwap - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2versionidremoveAndSwap('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2versionidunresolvedIssueCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2versionidunresolvedIssueCount('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://KHlzkJLOViomCas.jfhvNK7Bmu,ECjy.El8PVwLrzukPzE6qiwRD2GWcKO9PLDx76.CB1QE6N.DeeUxoPrW', data.response.self);
                assert.equal(54179602, data.response.issuesUnresolvedCount);
                assert.equal(36580316, data.response.issuesCount);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projectCategory((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2projectCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2projectCategory('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('http://IqZM.xxfJmBhYyOsjvFtRc+r,IZRhJpn3Ll6DVSao6k0wBnjRjpbd.8', data.response.self);
                assert.equal('reprehenderit laboris', data.response.id);
                assert.equal('aliqua laboris aliquip', data.response.name);
                assert.equal('sit', data.response.description);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectCategoryid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectCategoryid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://uY.dgmbaBuQIdpHaDl6YZSjQ0btTwNy.fxxXohB0R3Qs-DOGTMrce9oaOS4JgiWBM.dgAVgaHaZpOsAddhD4gg', data.response.self);
                assert.equal('exercitation', data.response.id);
                assert.equal('officia cupid', data.response.name);
                assert.equal('do ut', data.response.description);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2projectCategoryid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2projectCategoryid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('magna eiusmod do', data.response.self);
                assert.equal('ea dolor ut ullamco c', data.response.id);
                assert.equal('laborum irure velit', data.response.description);
                assert.equal('in elit', data.response.name);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2projectCategoryid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2projectCategoryid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectvalidatekey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2projectvalidatekey('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.errorMessages));
                assert.equal('object', typeof data.response.errors);
                assert.equal(-55680971, data.response.status);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectvalidatevalidProjectKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projectvalidatevalidProjectKey('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectvalidatevalidProjectName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2projectvalidatevalidProjectName('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screens - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2screens('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://YWbTVhsnXLnzF.cfxr', data.response.self);
                assert.equal('https://xdfMUfcHAoTZozlOwTblJqjDavPeUqGi.ilFYw6HexHU9JuZqFoDJ.ApVa-M9,OYms', data.response.nextPage);
                assert.equal(29892004, data.response.maxResults);
                assert.equal(37440970, data.response.startAt);
                assert.equal(18850689, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensaddToDefaultfieldId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2screensaddToDefaultfieldId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screensscreenIdavailableFields - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2screensscreenIdavailableFields('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screensscreenIdtabs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2screensscreenIdtabs('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2screensscreenIdtabs('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('veniam in tempor nisi proident', data.response.name);
                assert.equal(-64046407, data.response.id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2screensscreenIdtabstabId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRestapi2screensscreenIdtabstabId('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('veniam culpa eiusmod incididunt tempor', data.response.name);
                assert.equal(8701013, data.response.id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2screensscreenIdtabstabId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabId('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2screensscreenIdtabstabIdfields - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2screensscreenIdtabstabIdfields('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabstabIdfields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfields('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('fugiat id', data.response.id);
                assert.equal('nisi consequat exercitation', data.response.name);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2screensscreenIdtabstabIdfieldsid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2screensscreenIdtabstabIdfieldsid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabstabIdfieldsidmove - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdfieldsidmove('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2screensscreenIdtabstabIdmovepos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2screensscreenIdtabstabIdmovepos('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2tasktaskId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2tasktaskId('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-98380002, data.response.elapsedRuntime);
                assert.equal('dolore', data.response.id);
                assert.equal(-23954113, data.response.lastUpdate);
                assert.equal(-16239853, data.response.progress);
                assert.equal('http://jOhjYBibbIZLSMqmjRvFmzzHl.phcClQ0JkgWvganOA6ulaoWeG+gNr6J,RyER0WORtng7c1ljkKp2G', data.response.self);
                assert.equal('COMPLETE', data.response.status);
                assert.equal(-65836364, data.response.submitted);
                assert.equal(-77325462, data.response.submittedBy);
                assert.equal('Excepteur enim in ali', data.response.description);
                assert.equal('do ipsum laborum', data.response.message);
                assert.equal('object', typeof data.response.result);
                assert.equal(48376487, data.response.started);
                assert.equal(-45486877, data.response.finished);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2tasktaskIdcancel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRestapi2tasktaskIdcancel('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2permissionscheck - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          let body;
          if (stub) {
            body = Consts.fake;
          }
          a.postRestapi2permissionscheck(body, (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.globalPermissions));
                assert.equal(true, Array.isArray(data.response.projectPermissions));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2projectprojectIdOrKeyrole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          let projectId;
          if (stub) {
            projectId = Consts.fake;
          }
          a.getRestapi2projectprojectIdOrKeyrole(projectId, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2issuepropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2issuepropertiespropertyKey('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2issuepropertiespropertyKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          let propertyKey;
          let body;
          if (stub) {
            propertyKey = Consts.fake;
            body = Consts.fake;
          }
          a.putRestapi2issuepropertiespropertyKey(propertyKey, body, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2field - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRestapi2field('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('aliquip', data.response.id);
                assert.equal('dolor', data.response.key);
                assert.equal('quis amet', data.response.name);
                assert.equal(true, data.response.custom);
                assert.equal(false, data.response.orderable);
                assert.equal(false, data.response.navigable);
                assert.equal(true, data.response.searchable);
                assert.equal(true, Array.isArray(data.response.clauseNames));
                assert.equal('object', typeof data.response.scope);
                assert.equal('object', typeof data.response.schema);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2fieldfieldKeyoption - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2fieldfieldKeyoption('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://gVruWZdQgqsFFSNCgfcJnXeBy.tgfiDvlznkOYmKRrtbEFtq3', data.response.self);
                assert.equal('http://yDwtzqwXehto.udkgrZ23B3pi3QBR5TdfK.xoT.mjc,b+M9,S1tVLHxJkAbuIazTqJzqZucx.eJ', data.response.nextPage);
                assert.equal(-8797969, data.response.maxResults);
                assert.equal(-71590853, data.response.startAt);
                assert.equal(33982979, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2issueissueIdOrKeyattachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          let issueIdOrKey;
          let body;
          if (stub) {
            issueIdOrKey = Consts.fake;
            body = Consts.fake;
          }
          a.postRestapi2issueissueIdOrKeyattachments(issueIdOrKey, body, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentidexpandhuman - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2attachmentidexpandhuman('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(-84191010, data.response.id);
                assert.equal('Excepteur quis ut consequat', data.response.name);
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal(-19207362, data.response.totalEntryCount);
                assert.equal('ea in dolore sunt', data.response.mediaType);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2attachmentid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestapi2attachmentid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(9269860, data.response.id);
                assert.equal('https://ldJdwduKeUBfucIyOlLvywVaskFLVG.anxU2CF', data.response.self);
                assert.equal('id', data.response.filename);
                assert.equal('object', typeof data.response.author);
                assert.equal('1980-05-02T05:23:45.157Z', data.response.created);
                assert.equal(4570281, data.response.size);
                assert.equal('occaecat Duis cupidatat', data.response.mimeType);
                assert.equal('object', typeof data.response.properties);
                assert.equal('culpa sit adipisicing id', data.response.content);
                assert.equal('sunt', data.response.thumbnail);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2attachmentid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRestapi2attachmentid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRestapi2filter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          let expand;
          let body;
          if (stub) {
            expand = Consts.fake;
            body = Consts.fake;
          }
          a.postRestapi2filter(expand, body, (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('sunt proident', data.response.name);
                assert.equal('https://nZaDksxncqlIerW.kgZQPlemHiEgR9Csx9EoLKhSthqSg', data.response.self);
                assert.equal('Ut sint', data.response.id);
                assert.equal('et ut fugiat Excepteur velit', data.response.description);
                assert.equal('object', typeof data.response.owner);
                assert.equal('in labore velit sunt nulla', data.response.jql);
                assert.equal('https://uAqqIqmLVQJbcPXRPtKPJ.amlxHklyEJ058rFF', data.response.viewUrl);
                assert.equal('http://ZLvDLowiSJtkMf.ayoZaqpFhOHjoU3iP.7pScf3AcX6A,rhRe3,NofjzAwrl-Y-pbG1', data.response.searchUrl);
                assert.equal(false, data.response.favourite);
                assert.equal(-61773425, data.response.favouritedCount);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('object', typeof data.response.sharedUsers);
                assert.equal('object', typeof data.response.subscriptions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filter - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let expand;
        if (stub) {
          expand = Consts.fake;
        }
        try {
          a.getRestapi2filter(expand, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filtersearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        let filterName;
        let accountId;
        let owner;
        let groupname;
        let projectId;
        let orderBy;
        let startAt;
        let maxResults;
        let expand;

        if (stub) {
          filterName = Consts.fake;
          accountId = Consts.fake;
          owner = Consts.fake;
          groupname = Consts.fake;
          projectId = Consts.fake;
          orderBy = Consts.fake;
          startAt = Consts.fake;
          maxResults = Consts.fake;
          expand = Consts.fake;
        }
        try {
          a.getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('https://DRiEbNPUnK.isYPfTV7CArLGXEgPCCu-f1UiroMJiMrU6Cjuk,VXHnNgfPylw1xNNjbyJzMiT6', data.response.self);
                assert.equal('https://vHritGnqgSLxIDsqLXayqpMJquNrG.ldrniG4YtPP', data.response.nextPage);
                assert.equal(81385001, data.response.maxResults);
                assert.equal(-87662561, data.response.startAt);
                assert.equal(69747235, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRestapi2filterid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          let id;
          let expand;
          let body;
          const updatedDescription = 'brand new description';
          if (stub) {
            id = Consts.fake;
            expand = Consts.fake;
            body = Consts.fake;
          }
          a.putRestapi2filterid(id, expand, body, (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('sint', data.response.name);
                assert.equal('http://tN.dkiuhZhufcf2ovDGfvVqZh1jgmTLtYPMP-', data.response.self);
                assert.equal('nisi dolor ', data.response.id);
                assert.equal('est do', data.response.description);
                assert.equal('object', typeof data.response.owner);
                assert.equal('ipsum', data.response.jql);
                assert.equal('http://urzMnyJzeCTnIowgSlxTmDweDTPcKL.ykvpYmg8vNkOYb9e', data.response.viewUrl);
                assert.equal('https://khOVvnvlxxqPsFFzunJnKhKcDb.vnhMjlEu.JAQbZRHyAIv1gPdj+dX5BNo7Y1', data.response.searchUrl);
                assert.equal(false, data.response.favourite);
                assert.equal(10379856, data.response.favouritedCount);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('object', typeof data.response.sharedUsers);
                assert.equal('object', typeof data.response.subscriptions);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
                // confirm the filter description has been updated.
                assert.equal(data.response.description, updatedDescription);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRestapi2filterid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        let id;
        if (stub) {
          id = Consts.fake;
        }
        try {
          a.deleteRestapi2filterid(id, (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filterfavourite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2filterfavourite('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestapi2filtermy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRestapi2filtermy('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldValuesAppsUpdateMultipleCustomFieldValuesBodyParam = {
      updates: [
        {
          customField: 'string',
          issueIds: [
            4
          ],
          value: null
        }
      ]
    };
    describe('#updateMultipleCustomFieldValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMultipleCustomFieldValues(null, issueCustomFieldValuesAppsUpdateMultipleCustomFieldValuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldValuesApps', 'updateMultipleCustomFieldValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldValuesAppsUpdateCustomFieldValueBodyParam = {
      updates: [
        {
          issueIds: [
            9
          ],
          value: null
        }
      ]
    };
    describe('#updateCustomFieldValue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCustomFieldValue('fakedata', null, issueCustomFieldValuesAppsUpdateCustomFieldValueBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldValuesApps', 'updateCustomFieldValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomFieldConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomFieldConfiguration('fakedata', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1000, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldConfigurationApps', 'getCustomFieldConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldConfigurationAppsUpdateCustomFieldConfigurationBodyParam = {
      configurations: [
        {}
      ]
    };
    describe('#updateCustomFieldConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCustomFieldConfiguration('fakedata', issueCustomFieldConfigurationAppsUpdateCustomFieldConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldConfigurationApps', 'updateCustomFieldConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachmentContent('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueAttachments', 'getAttachmentContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentThumbnail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachmentThumbnail('fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueAttachments', 'getAttachmentThumbnail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsCreateDashboardBodyParam = {
      name: 'string',
      sharePermissions: [
        {}
      ],
      editPermissions: [
        {}
      ]
    };
    describe('#createDashboard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDashboard(dashboardsCreateDashboardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10000', data.response.id);
                assert.equal(false, data.response.isFavourite);
                assert.equal('System Dashboard', data.response.name);
                assert.equal(1, data.response.popularity);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/dashboard/10000', data.response.self);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('https://your-domain.atlassian.net/secure/Dashboard.jspa?selectPageId=10000', data.response.view);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'createDashboard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsUpdateDashboardBodyParam = {
      name: 'string',
      sharePermissions: [
        {}
      ],
      editPermissions: [
        {}
      ]
    };
    describe('#updateDashboard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDashboard('fakedata', dashboardsUpdateDashboardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'updateDashboard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDashboard - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDashboard('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'deleteDashboard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsCopyDashboardBodyParam = {
      name: 'string',
      sharePermissions: [
        {}
      ],
      editPermissions: [
        {}
      ]
    };
    describe('#copyDashboard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.copyDashboard('fakedata', dashboardsCopyDashboardBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10000', data.response.id);
                assert.equal(false, data.response.isFavourite);
                assert.equal('System Dashboard', data.response.name);
                assert.equal(1, data.response.popularity);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/dashboard/10000', data.response.self);
                assert.equal(true, Array.isArray(data.response.sharePermissions));
                assert.equal('https://your-domain.atlassian.net/secure/Dashboard.jspa?selectPageId=10000', data.response.view);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'copyDashboard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEvents((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issues', 'getEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuesGetChangeLogsByIdsBodyParam = {
      changelogIds: [
        7
      ]
    };
    describe('#getChangeLogsByIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChangeLogsByIds('fakedata', issuesGetChangeLogsByIdsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.maxResults);
                assert.equal(2, data.response.total);
                assert.equal(true, Array.isArray(data.response.histories));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issues', 'getChangeLogsByIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jiraExpressionsAnalyseExpressionBodyParam = {
      expressions: 'issues.map(issue => issue.properties[\'property_key\'])'
    };
    describe('#analyseExpression - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.analyseExpression(null, jiraExpressionsAnalyseExpressionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JiraExpressions', 'analyseExpression', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldsPaginated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFieldsPaginated(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFields', 'getFieldsPaginated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldsUpdateCustomFieldBodyParam = {
      name: 'string',
      description: 'string',
      searcherKey: 'com.atlassian.jira.plugin.system.customfieldtypes:multiselectsearcher'
    };
    describe('#updateCustomField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCustomField('fakedata', issueFieldsUpdateCustomFieldBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFields', 'updateCustomField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContextsForFieldDeprecated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContextsForFieldDeprecated('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(5, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFields', 'getContextsForFieldDeprecated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomField('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFields', 'deleteCustomField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreCustomField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restoreCustomField('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFields', 'restoreCustomField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trashCustomField - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.trashCustomField('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFields', 'trashCustomField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContextsForField - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContextsForField('fakedata', null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'getContextsForField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsCreateCustomFieldContextBodyParam = {
      name: 'string'
    };
    describe('#createCustomFieldContext - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomFieldContext('fakedata', issueCustomFieldContextsCreateCustomFieldContextBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10025', data.response.id);
                assert.equal('Bug fields context', data.response.name);
                assert.equal('A context used to define the custom field options for bugs.', data.response.description);
                assert.equal(true, Array.isArray(data.response.projectIds));
                assert.equal(true, Array.isArray(data.response.issueTypeIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'createCustomFieldContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultValues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefaultValues('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'getDefaultValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsSetDefaultValuesBodyParam = {
      defaultValues: [
        {}
      ]
    };
    describe('#setDefaultValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDefaultValues('fakedata', issueCustomFieldContextsSetDefaultValuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'setDefaultValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeMappingsForContexts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypeMappingsForContexts('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'getIssueTypeMappingsForContexts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsGetCustomFieldContextsForProjectsAndIssueTypesBodyParam = {
      mappings: [
        {}
      ]
    };
    describe('#getCustomFieldContextsForProjectsAndIssueTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomFieldContextsForProjectsAndIssueTypes('fakedata', null, null, issueCustomFieldContextsGetCustomFieldContextsForProjectsAndIssueTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'getCustomFieldContextsForProjectsAndIssueTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectContextMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProjectContextMapping('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'getProjectContextMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsUpdateCustomFieldContextBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#updateCustomFieldContext - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCustomFieldContext('fakedata', 555, issueCustomFieldContextsUpdateCustomFieldContextBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'updateCustomFieldContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomFieldContext - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomFieldContext('fakedata', 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'deleteCustomFieldContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsAddIssueTypesToContextBodyParam = {
      issueTypeIds: [
        'string'
      ]
    };
    describe('#addIssueTypesToContext - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addIssueTypesToContext('fakedata', 555, issueCustomFieldContextsAddIssueTypesToContextBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'addIssueTypesToContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsRemoveIssueTypesFromContextBodyParam = {
      issueTypeIds: [
        'string'
      ]
    };
    describe('#removeIssueTypesFromContext - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeIssueTypesFromContext('fakedata', 555, issueCustomFieldContextsRemoveIssueTypesFromContextBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'removeIssueTypesFromContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsAssignProjectsToCustomFieldContextBodyParam = {
      projectIds: [
        'string'
      ]
    };
    describe('#assignProjectsToCustomFieldContext - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignProjectsToCustomFieldContext('fakedata', 555, issueCustomFieldContextsAssignProjectsToCustomFieldContextBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'assignProjectsToCustomFieldContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldContextsRemoveCustomFieldContextFromProjectsBodyParam = {
      projectIds: [
        'string'
      ]
    };
    describe('#removeCustomFieldContextFromProjects - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeCustomFieldContextFromProjects('fakedata', 555, issueCustomFieldContextsRemoveCustomFieldContextFromProjectsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldContexts', 'removeCustomFieldContextFromProjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOptionsForContext - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOptionsForContext('fakedata', 555, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(4, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldOptions', 'getOptionsForContext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldOptionsUpdateCustomFieldOptionBodyParam = {
      options: [
        {
          id: 'string',
          value: 'string',
          disabled: false
        }
      ]
    };
    describe('#updateCustomFieldOption - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCustomFieldOption('fakedata', 555, issueCustomFieldOptionsUpdateCustomFieldOptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldOptions', 'updateCustomFieldOption', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldOptionsCreateCustomFieldOptionBodyParam = {
      options: [
        {
          value: 'string',
          optionId: 'string',
          disabled: false
        }
      ]
    };
    describe('#createCustomFieldOption - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomFieldOption('fakedata', 555, issueCustomFieldOptionsCreateCustomFieldOptionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.options));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldOptions', 'createCustomFieldOption', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueCustomFieldOptionsReorderCustomFieldOptionsBodyParam = {
      customFieldOptionIds: [
        'string'
      ]
    };
    describe('#reorderCustomFieldOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reorderCustomFieldOptions('fakedata', 555, issueCustomFieldOptionsReorderCustomFieldOptionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldOptions', 'reorderCustomFieldOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomFieldOption - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomFieldOption('fakedata', 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueCustomFieldOptions', 'deleteCustomFieldOption', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScreensForField - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScreensForField('fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(5, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Screens', 'getScreensForField', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const screensCreateScreenBodyParam = {
      name: 'string'
    };
    describe('#createScreen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createScreen(screensCreateScreenBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10005, data.response.id);
                assert.equal('Resolve Security Issue Screen', data.response.name);
                assert.equal('Enables changes to resolution and linked issues.', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Screens', 'createScreen', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const screensUpdateScreenBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#updateScreen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateScreen(555, screensUpdateScreenBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Screens', 'updateScreen', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScreen - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteScreen(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Screens', 'deleteScreen', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFieldConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFieldConfigurations(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'getAllFieldConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsCreateFieldConfigurationBodyParam = {
      name: 'string'
    };
    describe('#createFieldConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFieldConfiguration(issueFieldConfigurationsCreateFieldConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10001, data.response.id);
                assert.equal('My Field Configuration', data.response.name);
                assert.equal('My field configuration description', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'createFieldConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsUpdateFieldConfigurationBodyParam = {
      name: 'string'
    };
    describe('#updateFieldConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateFieldConfiguration(555, issueFieldConfigurationsUpdateFieldConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'updateFieldConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFieldConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFieldConfiguration(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'deleteFieldConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldConfigurationItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFieldConfigurationItems(555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'getFieldConfigurationItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsUpdateFieldConfigurationItemsBodyParam = {
      fieldConfigurationItems: [
        {}
      ]
    };
    describe('#updateFieldConfigurationItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateFieldConfigurationItems(555, issueFieldConfigurationsUpdateFieldConfigurationItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'updateFieldConfigurationItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFieldConfigurationSchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFieldConfigurationSchemes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'getAllFieldConfigurationSchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsCreateFieldConfigurationSchemeBodyParam = {
      name: 'string'
    };
    describe('#createFieldConfigurationScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFieldConfigurationScheme(issueFieldConfigurationsCreateFieldConfigurationSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10002', data.response.id);
                assert.equal('Field Configuration Scheme for software related projects', data.response.name);
                assert.equal('We can use this one for software projects.', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'createFieldConfigurationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldConfigurationSchemeMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFieldConfigurationSchemeMappings(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(5, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'getFieldConfigurationSchemeMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldConfigurationSchemeProjectMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFieldConfigurationSchemeProjectMapping(null, null, [], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(5, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'getFieldConfigurationSchemeProjectMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsAssignFieldConfigurationSchemeToProjectBodyParam = {
      projectId: 'string'
    };
    describe('#assignFieldConfigurationSchemeToProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignFieldConfigurationSchemeToProject(issueFieldConfigurationsAssignFieldConfigurationSchemeToProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'assignFieldConfigurationSchemeToProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsUpdateFieldConfigurationSchemeBodyParam = {
      name: 'string'
    };
    describe('#updateFieldConfigurationScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateFieldConfigurationScheme(555, issueFieldConfigurationsUpdateFieldConfigurationSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'updateFieldConfigurationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFieldConfigurationScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFieldConfigurationScheme(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'deleteFieldConfigurationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsSetFieldConfigurationSchemeMappingBodyParam = {
      mappings: [
        {}
      ]
    };
    describe('#setFieldConfigurationSchemeMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setFieldConfigurationSchemeMapping(555, issueFieldConfigurationsSetFieldConfigurationSchemeMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'setFieldConfigurationSchemeMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueFieldConfigurationsRemoveIssueTypesFromGlobalFieldConfigurationSchemeBodyParam = {
      issueTypeIds: [
        'string'
      ]
    };
    describe('#removeIssueTypesFromGlobalFieldConfigurationScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeIssueTypesFromGlobalFieldConfigurationScheme(555, issueFieldConfigurationsRemoveIssueTypesFromGlobalFieldConfigurationSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueFieldConfigurations', 'removeIssueTypesFromGlobalFieldConfigurationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkGetGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bulkGetGroups(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'bulkGetGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicense((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.applications));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceInformation', 'getLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuePropertiesBulkSetIssuesPropertiesListBodyParam = {
      entitiesIds: [
        3
      ],
      properties: {}
    };
    describe('#bulkSetIssuesPropertiesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkSetIssuesPropertiesList(issuePropertiesBulkSetIssuesPropertiesListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueProperties', 'bulkSetIssuesPropertiesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuePropertiesBulkSetIssuePropertiesByIssueBodyParam = {
      issues: [
        {
          issueID: 4,
          properties: {}
        }
      ]
    };
    describe('#bulkSetIssuePropertiesByIssue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkSetIssuePropertiesByIssue(issuePropertiesBulkSetIssuePropertiesByIssueBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueProperties', 'bulkSetIssuePropertiesByIssue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueWatchersGetIsWatchingIssueBulkBodyParam = {
      issueIds: [
        'string'
      ]
    };
    describe('#getIsWatchingIssueBulk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIsWatchingIssueBulk(issueWatchersGetIsWatchingIssueBulkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.issuesIsWatching);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueWatchers', 'getIsWatchingIssueBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueSecurityLevelMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueSecurityLevelMembers(555, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueSecurityLevel', 'getIssueSecurityLevelMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypesForProject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypesForProject(555, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypes', 'getIssueTypesForProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIssueTypeSchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIssueTypeSchemes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'getAllIssueTypeSchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeSchemesCreateIssueTypeSchemeBodyParam = {
      name: 'string',
      issueTypeIds: [
        'string'
      ]
    };
    describe('#createIssueTypeScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIssueTypeScheme(issueTypeSchemesCreateIssueTypeSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10010', data.response.issueTypeSchemeId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'createIssueTypeScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeSchemesMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypeSchemesMapping(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(4, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'getIssueTypeSchemesMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeSchemeForProjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypeSchemeForProjects(null, null, [], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'getIssueTypeSchemeForProjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeSchemesAssignIssueTypeSchemeToProjectBodyParam = {
      issueTypeSchemeId: 'string',
      projectId: 'string'
    };
    describe('#assignIssueTypeSchemeToProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignIssueTypeSchemeToProject(issueTypeSchemesAssignIssueTypeSchemeToProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'assignIssueTypeSchemeToProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeSchemesUpdateIssueTypeSchemeBodyParam = {
      name: 'string',
      description: 'string',
      defaultIssueTypeId: 'string'
    };
    describe('#updateIssueTypeScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateIssueTypeScheme(555, issueTypeSchemesUpdateIssueTypeSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'updateIssueTypeScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIssueTypeScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIssueTypeScheme(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'deleteIssueTypeScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeSchemesAddIssueTypesToIssueTypeSchemeBodyParam = {
      issueTypeIds: [
        'string'
      ]
    };
    describe('#addIssueTypesToIssueTypeScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addIssueTypesToIssueTypeScheme(555, issueTypeSchemesAddIssueTypesToIssueTypeSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'addIssueTypesToIssueTypeScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeSchemesReorderIssueTypesInIssueTypeSchemeBodyParam = {
      issueTypeIds: [
        'string'
      ]
    };
    describe('#reorderIssueTypesInIssueTypeScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reorderIssueTypesInIssueTypeScheme(555, issueTypeSchemesReorderIssueTypesInIssueTypeSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'reorderIssueTypesInIssueTypeScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIssueTypeFromIssueTypeScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeIssueTypeFromIssueTypeScheme(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeSchemes', 'removeIssueTypeFromIssueTypeScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeScreenSchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypeScreenSchemes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'getIssueTypeScreenSchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeScreenSchemesCreateIssueTypeScreenSchemeBodyParam = {
      name: 'string',
      issueTypeMappings: [
        {}
      ]
    };
    describe('#createIssueTypeScreenScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIssueTypeScreenScheme(issueTypeScreenSchemesCreateIssueTypeScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'createIssueTypeScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeScreenSchemeMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypeScreenSchemeMappings(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(4, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'getIssueTypeScreenSchemeMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIssueTypeScreenSchemeProjectAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIssueTypeScreenSchemeProjectAssociations(null, null, [], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'getIssueTypeScreenSchemeProjectAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeScreenSchemesAssignIssueTypeScreenSchemeToProjectBodyParam = {
      issueTypeScreenSchemeId: 'string',
      projectId: 'string'
    };
    describe('#assignIssueTypeScreenSchemeToProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignIssueTypeScreenSchemeToProject(issueTypeScreenSchemesAssignIssueTypeScreenSchemeToProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'assignIssueTypeScreenSchemeToProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeScreenSchemesUpdateIssueTypeScreenSchemeBodyParam = {
      name: 'string',
      description: 'string'
    };
    describe('#updateIssueTypeScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateIssueTypeScreenScheme('fakedata', issueTypeScreenSchemesUpdateIssueTypeScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'updateIssueTypeScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIssueTypeScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIssueTypeScreenScheme('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'deleteIssueTypeScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeScreenSchemesAppendMappingsForIssueTypeScreenSchemeBodyParam = {
      issueTypeMappings: [
        {}
      ]
    };
    describe('#appendMappingsForIssueTypeScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appendMappingsForIssueTypeScreenScheme('fakedata', issueTypeScreenSchemesAppendMappingsForIssueTypeScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'appendMappingsForIssueTypeScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeScreenSchemesUpdateDefaultScreenSchemeBodyParam = {
      screenSchemeId: 'string'
    };
    describe('#updateDefaultScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDefaultScreenScheme('fakedata', issueTypeScreenSchemesUpdateDefaultScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'updateDefaultScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTypeScreenSchemesRemoveMappingsFromIssueTypeScreenSchemeBodyParam = {
      issueTypeIds: [
        'string'
      ]
    };
    describe('#removeMappingsFromIssueTypeScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeMappingsFromIssueTypeScreenScheme('fakedata', issueTypeScreenSchemesRemoveMappingsFromIssueTypeScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'removeMappingsFromIssueTypeScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectsForIssueTypeScreenScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProjectsForIssueTypeScreenScheme(555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTypeScreenSchemes', 'getProjectsForIssueTypeScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jQLGetAutoCompletePostBodyParam = {
      projectIds: [
        9
      ],
      includeCollapsedFields: false
    };
    describe('#getAutoCompletePost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAutoCompletePost(jQLGetAutoCompletePostBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.visibleFieldNames));
                assert.equal(true, Array.isArray(data.response.visibleFunctionNames));
                assert.equal(true, Array.isArray(data.response.jqlReservedWords));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JQL', 'getAutoCompletePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jQLParseJqlQueriesBodyParam = {
      queries: [
        'string'
      ]
    };
    describe('#parseJqlQueries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.parseJqlQueries(null, jQLParseJqlQueriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.queries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JQL', 'parseJqlQueries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSearchMatchIssuesBodyParam = {
      jqls: [
        'string'
      ],
      issueIds: [
        1
      ]
    };
    describe('#matchIssues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.matchIssues(issueSearchMatchIssuesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.matches));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueSearch', 'matchIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllLabels(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(100, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Labels', 'getAllLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRecent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRecent(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'getRecent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#archiveProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.archiveProject('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'archiveProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProjectAsynchronously - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteProjectAsynchronously('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'deleteProjectAsynchronously', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.restore('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/project/EX', data.response.self);
                assert.equal('10000', data.response.id);
                assert.equal('EX', data.response.key);
                assert.equal('This project was created as an example for REST.', data.response.description);
                assert.equal('object', typeof data.response.lead);
                assert.equal(true, Array.isArray(data.response.components));
                assert.equal(true, Array.isArray(data.response.issueTypes));
                assert.equal('https://www.example.com', data.response.url);
                assert.equal('from-jira@example.com', data.response.email);
                assert.equal('PROJECT_LEAD', data.response.assigneeType);
                assert.equal(true, Array.isArray(data.response.versions));
                assert.equal('Example', data.response.name);
                assert.equal('object', typeof data.response.roles);
                assert.equal('object', typeof data.response.avatarUrls);
                assert.equal('object', typeof data.response.projectCategory);
                assert.equal(false, data.response.simplified);
                assert.equal('classic', data.response.style);
                assert.equal('object', typeof data.response.properties);
                assert.equal('object', typeof data.response.insight);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'restore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHierarchy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHierarchy(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10030, data.response.projectId);
                assert.equal(true, Array.isArray(data.response.hierarchy));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'getHierarchy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAccessibleProjectTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAccessibleProjectTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectTypes', 'getAllAccessibleProjectTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFeaturesForProject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFeaturesForProject('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.features));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectFeatures', 'getFeaturesForProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectFeaturesToggleFeatureForProjectBodyParam = {
      state: 'COMING_SOON'
    };
    describe('#toggleFeatureForProject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.toggleFeatureForProject('fakedata', 'fakedata', projectFeaturesToggleFeatureForProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectFeatures', 'toggleFeatureForProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProjectEmail(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('jira@example.customdomain.com', data.response.emailAddress);
                assert.equal(true, Array.isArray(data.response.emailAddressStatus));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectEmail', 'getProjectEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectEmailUpdateProjectEmailBodyParam = {
      emailAddress: 'string',
      emailAddressStatus: [
        'string'
      ]
    };
    describe('#updateProjectEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateProjectEmail(555, projectEmailUpdateProjectEmailBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectEmail', 'updateProjectEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScreenSchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScreenSchemes(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/screenscheme?maxResults=25&startAt=0', data.response.self);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScreenSchemes', 'getScreenSchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const screenSchemesCreateScreenSchemeBodyParam = {
      name: 'string',
      screens: {}
    };
    describe('#createScreenScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createScreenScheme(screenSchemesCreateScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10001, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScreenSchemes', 'createScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const screenSchemesUpdateScreenSchemeBodyParam = {
      name: 'string',
      description: 'string',
      screens: {
        edit: 'string',
        create: 'string',
        view: 'string',
        default: 'string'
      }
    };
    describe('#updateScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateScreenScheme('fakedata', screenSchemesUpdateScreenSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScreenSchemes', 'updateScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScreenScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteScreenScheme('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ScreenSchemes', 'deleteScreenScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatarImageByType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAvatarImageByType('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Avatars', 'getAvatarImageByType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatarImageByID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAvatarImageByID('fakedata', 555, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Avatars', 'getAvatarImageByID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatarImageByOwner - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAvatarImageByOwner('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Avatars', 'getAvatarImageByOwner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserEmail('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmailBulk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserEmailBulk([], (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserEmailBulk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsersDefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUsersDefault(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getAllUsersDefault', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllUsers(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getAllUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicWebhooksForApp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDynamicWebhooksForApp(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'getDynamicWebhooksForApp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksRegisterDynamicWebhooksBodyParam = {
      webhooks: [
        {}
      ],
      url: 'string'
    };
    describe('#registerDynamicWebhooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerDynamicWebhooks(webhooksRegisterDynamicWebhooksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.webhookRegistrationResult));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'registerDynamicWebhooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksDeleteWebhookByIdBodyParam = {
      webhookIds: [
        4
      ]
    };
    describe('#deleteWebhookById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWebhookById(webhooksDeleteWebhookByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'deleteWebhookById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFailedWebhooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFailedWebhooks(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.values));
                assert.equal(100, data.response.maxResults);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/webhook/failed?failedAfter=1573540473480&maxResults=100', data.response.next);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'getFailedWebhooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksRefreshWebhooksBodyParam = {
      webhookIds: [
        8
      ]
    };
    describe('#refreshWebhooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refreshWebhooks(webhooksRefreshWebhooksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'refreshWebhooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowsCreateWorkflowBodyParam = {
      name: 'string',
      transitions: [
        {
          name: 'string'
        }
      ],
      statuses: [
        {}
      ]
    };
    describe('#createWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWorkflow(workflowsCreateWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Workflow 1', data.response.name);
                assert.equal('fb8731c5-aa05-4453-bf79-b583a1c87736', data.response.entityId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'createWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowsPaginated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowsPaginated(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(5, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'getWorkflowsPaginated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInactiveWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInactiveWorkflow('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflows', 'deleteInactiveWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowTransitionRuleConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowTransitionRuleConfigurations(null, null, [], null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowTransitionRules', 'getWorkflowTransitionRuleConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTransitionRulesUpdateWorkflowTransitionRuleConfigurationsBodyParam = {
      workflows: [
        {}
      ]
    };
    describe('#updateWorkflowTransitionRuleConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWorkflowTransitionRuleConfigurations(workflowTransitionRulesUpdateWorkflowTransitionRuleConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowTransitionRules', 'updateWorkflowTransitionRuleConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTransitionRulesDeleteWorkflowTransitionRuleConfigurationsBodyParam = {
      workflows: [
        {}
      ]
    };
    describe('#deleteWorkflowTransitionRuleConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWorkflowTransitionRuleConfigurations(workflowTransitionRulesDeleteWorkflowTransitionRuleConfigurationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowTransitionRules', 'deleteWorkflowTransitionRuleConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllWorkflowSchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllWorkflowSchemes(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowSchemes', 'getAllWorkflowSchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowSchemeProjectAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowSchemeProjectAssociations([], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowSchemeProjectAssociations', 'getWorkflowSchemeProjectAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowSchemeProjectAssociationsAssignSchemeToProjectBodyParam = {
      projectId: 'string'
    };
    describe('#assignSchemeToProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignSchemeToProject(workflowSchemeProjectAssociationsAssignSchemeToProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowSchemeProjectAssociations', 'assignSchemeToProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowSchemeDraftsPublishDraftWorkflowSchemeBodyParam = {
      statusMappings: [
        {
          issueTypeId: 'string',
          statusId: 'string',
          newStatusId: 'string'
        }
      ]
    };
    describe('#publishDraftWorkflowScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.publishDraftWorkflowScheme(555, null, workflowSchemeDraftsPublishDraftWorkflowSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowSchemeDrafts', 'publishDraftWorkflowScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBanner - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBanner((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('This is a public, enabled, non-dismissible banner, set using the API', data.response.message);
                assert.equal(false, data.response.isDismissible);
                assert.equal(true, data.response.isEnabled);
                assert.equal('9HN2FJK9DM8BHRWERVW3RRTGDJ4G4D5C', data.response.hashId);
                assert.equal('public', data.response.visibility);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnnouncementBanner', 'getBanner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const announcementBannerSetBannerBodyParam = {
      isDismissible: false,
      isEnabled: true,
      message: 'string',
      visibility: 'string'
    };
    describe('#setBanner - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setBanner(announcementBannerSetBannerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AnnouncementBanner', 'setBanner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAvailableDashboardGadgets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAvailableDashboardGadgets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.gadgets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'getAllAvailableDashboardGadgets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGadgets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllGadgets(555, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.gadgets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'getAllGadgets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsAddGadgetBodyParam = {
      color: 'string',
      ignoreUriAndModuleKeyValidation: true,
      moduleKey: 'string',
      position: {
        'The column position of the gadget.': 10,
        'The row position of the gadget.': 6
      },
      title: 'string',
      uri: 'string'
    };
    describe('#addGadget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addGadget(555, dashboardsAddGadgetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10001, data.response.id);
                assert.equal('com.atlassian.plugins.atlassian-connect-plugin:com.atlassian.connect.node.sample-addon__sample-dashboard-item', data.response.moduleKey);
                assert.equal('blue', data.response.color);
                assert.equal('object', typeof data.response.position);
                assert.equal('Issue statistics', data.response.title);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'addGadget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardsUpdateGadgetBodyParam = {
      color: 'string',
      position: {
        'The column position of the gadget.': 8,
        'The row position of the gadget.': 3
      },
      title: 'string'
    };
    describe('#updateGadget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateGadget(555, 555, dashboardsUpdateGadgetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'updateGadget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGadget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeGadget(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboards', 'removeGadget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrashedFieldsPaginated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrashedFieldsPaginated(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(false, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuefields', 'getTrashedFieldsPaginated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filtersChangeFilterOwnerBodyParam = {
      accountId: 'string'
    };
    describe('#changeFilterOwner - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeFilterOwner(555, filtersChangeFilterOwnerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Filters', 'changeFilterOwner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSecuritySchemesCreateIssueSecuritySchemeBodyParam = {
      name: 'string'
    };
    describe('#createIssueSecurityScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIssueSecurityScheme(issueSecuritySchemesCreateIssueSecuritySchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'createIssueSecurityScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityLevels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityLevels(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'getSecurityLevels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSecuritySchemesSetDefaultLevelsBodyParam = {
      defaultValues: [
        {}
      ]
    };
    describe('#setDefaultLevels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDefaultLevels(issueSecuritySchemesSetDefaultLevelsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'setDefaultLevels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityLevelMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityLevelMembers(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'getSecurityLevelMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchProjectsUsingSecuritySchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchProjectsUsingSecuritySchemes(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10000', data.response.issueSecuritySchemeId);
                assert.equal('10000', data.response.projectId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'searchProjectsUsingSecuritySchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchSecuritySchemes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchSecuritySchemes(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10000, data.response.id);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/issuesecurityscheme/10000', data.response.self);
                assert.equal('Default scheme', data.response.name);
                assert.equal('Default scheme description', data.response.description);
                assert.equal(10001, data.response.defaultLevel);
                assert.equal(true, Array.isArray(data.response.projectIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'searchSecuritySchemes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSecuritySchemesUpdateIssueSecuritySchemeBodyParam = {
      description: 'string',
      name: 'string'
    };
    describe('#updateIssueSecurityScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateIssueSecurityScheme('fakedata', issueSecuritySchemesUpdateIssueSecuritySchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'updateIssueSecurityScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityScheme('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'deleteSecurityScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSecuritySchemesAddSecurityLevelBodyParam = {
      levels: [
        {
          description: 'string',
          isDefault: false,
          members: [
            {
              parameter: 'string',
              type: 'string'
            }
          ],
          name: 'string'
        }
      ]
    };
    describe('#addSecurityLevel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSecurityLevel('fakedata', issueSecuritySchemesAddSecurityLevelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'addSecurityLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSecuritySchemesUpdateSecurityLevelBodyParam = {
      description: 'string',
      name: 'string'
    };
    describe('#updateSecurityLevel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSecurityLevel('fakedata', 'fakedata', issueSecuritySchemesUpdateSecurityLevelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'updateSecurityLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeLevel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeLevel('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'removeLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueSecuritySchemesAddSecurityLevelMembersBodyParam = {};
    describe('#addSecurityLevelMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSecurityLevelMembers('fakedata', 'fakedata', issueSecuritySchemesAddSecurityLevelMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'addSecurityLevelMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeMemberFromSecurityLevel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeMemberFromSecurityLevel('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuesecurityschemes', 'removeMemberFromSecurityLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jQLSanitiseJqlQueriesBodyParam = {
      queries: [
        {}
      ]
    };
    describe('#sanitiseJqlQueries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sanitiseJqlQueries(jQLSanitiseJqlQueriesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.queries));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JQL', 'sanitiseJqlQueries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrecomputations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrecomputations(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JQLFunctionsApps', 'getPrecomputations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jQLFunctionsAppsUpdatePrecomputationsBodyParam = {
      values: [
        {
          id: 1,
          value: 'string'
        }
      ]
    };
    describe('#updatePrecomputations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePrecomputations(jQLFunctionsAppsUpdatePrecomputationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JQLFunctionsApps', 'updatePrecomputations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApproximateLicenseCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApproximateLicenseCount((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseMetrics', 'getApproximateLicenseCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApproximateApplicationLicenseCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApproximateApplicationLicenseCount('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseMetrics', 'getApproximateApplicationLicenseCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueNotificationSchemesCreateNotificationSchemeBodyParam = {
      name: 'string'
    };
    describe('#createNotificationScheme - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNotificationScheme(issueNotificationSchemesCreateNotificationSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuenotificationschemes', 'createNotificationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotificationSchemeToProjectMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNotificationSchemeToProjectMappings(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(4, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuenotificationschemes', 'getNotificationSchemeToProjectMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueNotificationSchemesUpdateNotificationSchemeBodyParam = {
      description: 'string',
      name: 'string'
    };
    describe('#updateNotificationScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNotificationScheme('fakedata', issueNotificationSchemesUpdateNotificationSchemeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuenotificationschemes', 'updateNotificationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueNotificationSchemesAddNotificationsBodyParam = {
      notificationSchemeEvents: [
        {}
      ]
    };
    describe('#addNotifications - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNotifications('fakedata', issueNotificationSchemesAddNotificationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuenotificationschemes', 'addNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNotificationScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNotificationScheme('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuenotificationschemes', 'deleteNotificationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNotificationFromNotificationScheme - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeNotificationFromNotificationScheme('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuenotificationschemes', 'removeNotificationFromNotificationScheme', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuePrioritiesCreatePriorityBodyParam = {
      name: 'string',
      statusColor: 'string'
    };
    describe('#createPriority - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPriority(issuePrioritiesCreatePriorityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuepriorities', 'createPriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuePrioritiesSetDefaultPriorityBodyParam = {
      id: 'string'
    };
    describe('#setDefaultPriority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDefaultPriority(issuePrioritiesSetDefaultPriorityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuepriorities', 'setDefaultPriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuePrioritiesMovePrioritiesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#movePriorities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.movePriorities(issuePrioritiesMovePrioritiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuepriorities', 'movePriorities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchPriorities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchPriorities(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(2, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuepriorities', 'searchPriorities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issuePrioritiesUpdatePriorityBodyParam = {
      description: 'string',
      iconUrl: '/images/icons/priorities/medium.png',
      name: 'string',
      statusColor: 'string'
    };
    describe('#updatePriority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePriority('fakedata', issuePrioritiesUpdatePriorityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuepriorities', 'updatePriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePriority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePriority('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issuepriorities', 'deletePriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueResolutionsCreateResolutionBodyParam = {
      name: 'string'
    };
    describe('#createResolution - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createResolution(issueResolutionsCreateResolutionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('10001', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issueresolutions', 'createResolution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueResolutionsSetDefaultResolutionBodyParam = {
      id: 'string'
    };
    describe('#setDefaultResolution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setDefaultResolution(issueResolutionsSetDefaultResolutionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issueresolutions', 'setDefaultResolution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueResolutionsMoveResolutionsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#moveResolutions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.moveResolutions(issueResolutionsMoveResolutionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issueresolutions', 'moveResolutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchResolutions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchResolutions(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(50, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(1, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issueresolutions', 'searchResolutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueResolutionsUpdateResolutionBodyParam = {
      name: 'string'
    };
    describe('#updateResolution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateResolution('fakedata', issueResolutionsUpdateResolutionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issueresolutions', 'updateResolution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResolution - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteResolution('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Issueresolutions', 'deleteResolution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusesById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusesById(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'getStatusesById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusUpdateStatusesBodyParam = {
      statuses: [
        {
          description: 'string',
          id: 'string',
          name: 'string',
          statusCategory: 'DONE'
        }
      ]
    };
    describe('#updateStatuses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateStatuses(statusUpdateStatusesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'updateStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusCreateStatusesBodyParam = {
      scope: {},
      statuses: [
        {}
      ]
    };
    describe('#createStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createStatuses(statusCreateStatusesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'createStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteStatusesById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteStatusesById(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'deleteStatusesById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.search(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/statuses/search?startAt=0&maxResults=2', data.response.self);
                assert.equal('https://your-domain.atlassian.net/rest/api/3/statuses/search?startAt=2&maxResults=2', data.response.nextPage);
                assert.equal(2, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(5, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'search', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUiModifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUiModifications(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response.maxResults);
                assert.equal(0, data.response.startAt);
                assert.equal(3, data.response.total);
                assert.equal(true, data.response.isLast);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UIModificationsApps', 'getUiModifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uIModificationsAppsCreateUiModificationBodyParam = {
      name: 'string'
    };
    describe('#createUiModification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUiModification(uIModificationsAppsCreateUiModificationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('d7dbda8a-6239-4b63-8e13-a5ef975c8e61', data.response.id);
                assert.equal('https://api.atlassian.com/ex/jira/{cloudid}/rest/api/2/uiModifications/d7dbda8a-6239-4b63-8e13-a5ef975c8e61', data.response.self);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UIModificationsApps', 'createUiModification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uIModificationsAppsUpdateUiModificationBodyParam = {
      contexts: [
        {
          id: 'string',
          isAvailable: true,
          issueTypeId: 'string',
          projectId: 'string',
          viewType: 'string'
        }
      ],
      data: 'string',
      description: 'string',
      name: 'string'
    };
    describe('#updateUiModification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUiModification('fakedata', uIModificationsAppsUpdateUiModificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UIModificationsApps', 'updateUiModification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUiModification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUiModification('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UIModificationsApps', 'deleteUiModification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceGetAddonPropertiesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addonPropertiesResourceGetAddonPropertiesGet('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.keys));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppProperties', 'addonPropertiesResourceGetAddonPropertiesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceGetAddonPropertyGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addonPropertiesResourceGetAddonPropertyGet('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('https://your-domain.atlassian.net/jira/rest/atlassian-connect/1/addon/example.app.key/properties/propertyKey', data.response.self);
                assert.equal('propertyKey', data.response.key);
                assert.equal('propertyValue', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppProperties', 'addonPropertiesResourceGetAddonPropertyGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appPropertiesAddonPropertiesResourcePutAddonPropertyPutBodyParam = {};
    describe('#addonPropertiesResourcePutAddonPropertyPut - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addonPropertiesResourcePutAddonPropertyPut('fakedata', 'fakedata', appPropertiesAddonPropertiesResourcePutAddonPropertyPutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppProperties', 'addonPropertiesResourcePutAddonPropertyPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceDeleteAddonPropertyDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addonPropertiesResourceDeleteAddonPropertyDelete('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppProperties', 'addonPropertiesResourceDeleteAddonPropertyDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appPropertiesAddonPropertiesResourcePutAppPropertyPutBodyParam = {};
    describe('#addonPropertiesResourcePutAppPropertyPut - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addonPropertiesResourcePutAppPropertyPut('fakedata', appPropertiesAddonPropertiesResourcePutAppPropertyPutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppProperties', 'addonPropertiesResourcePutAppPropertyPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addonPropertiesResourceDeleteAppPropertyDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addonPropertiesResourceDeleteAppPropertyDelete('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppProperties', 'addonPropertiesResourceDeleteAppPropertyDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dynamicModulesResourceGetModulesGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dynamicModulesResourceGetModulesGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DynamicModules', 'dynamicModulesResourceGetModulesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dynamicModulesDynamicModulesResourceRegisterModulesPostBodyParam = {
      modules: [
        {
          description: {
            value: 'field with team'
          },
          type: 'single_select',
          extractions: [
            {
              path: 'category',
              type: 'text',
              name: 'categoryName'
            }
          ],
          name: {
            value: 'Team'
          },
          key: 'team-field'
        }
      ]
    };
    describe('#dynamicModulesResourceRegisterModulesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dynamicModulesResourceRegisterModulesPost(dynamicModulesDynamicModulesResourceRegisterModulesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DynamicModules', 'dynamicModulesResourceRegisterModulesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dynamicModulesResourceRemoveModulesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dynamicModulesResourceRemoveModulesDelete(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DynamicModules', 'dynamicModulesResourceRemoveModulesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appMigrationAppIssueFieldValueUpdateResourceUpdateIssueFieldsPutBodyParam = {
      updateValueList: [
        {
          _type: 'StringIssueField',
          fieldID: 1,
          issueID: 9,
          number: 1,
          optionID: 'string',
          richText: 'string',
          string: 'string',
          text: 'string'
        }
      ]
    };
    describe('#appIssueFieldValueUpdateResourceUpdateIssueFieldsPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appIssueFieldValueUpdateResourceUpdateIssueFieldsPut(appMigrationAppIssueFieldValueUpdateResourceUpdateIssueFieldsPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppMigration', 'appIssueFieldValueUpdateResourceUpdateIssueFieldsPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appMigrationMigrationResourceUpdateEntityPropertiesValuePutBodyParam = [
      {}
    ];
    describe('#migrationResourceUpdateEntityPropertiesValuePut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.migrationResourceUpdateEntityPropertiesValuePut('fakedata', appMigrationMigrationResourceUpdateEntityPropertiesValuePutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-jira-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppMigration', 'migrationResourceUpdateEntityPropertiesValuePut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appMigrationMigrationResourceWorkflowRuleSearchPostBodyParam = {
      ruleIds: [
        '55d44f1d-c859-42e5-9c27-2c5ec3f340b1'
      ],
      workflowEntityId: 'a498d711-685d-428d-8c3e-bc03bb450ea7'
    };
    describe('#migrationResourceWorkflowRuleSearchPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.migrationResourceWorkflowRuleSearchPost(appMigrationMigrationResourceWorkflowRuleSearchPostBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('a498d711-685d-428d-8c3e-bc03bb450ea7', data.response.workflowEntityId);
                assert.equal(true, Array.isArray(data.response.invalidRules));
                assert.equal(true, Array.isArray(data.response.validRules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppMigration', 'migrationResourceWorkflowRuleSearchPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
