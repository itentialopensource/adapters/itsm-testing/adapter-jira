## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Atlassian Jira. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Atlassian Jira.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Atlassian Jira. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.issuenav.criteria.autoupdate</code></td> 
   <td>Supports instant updates to search criteria.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

*Note: Be careful when changing [application properties and advanced settings](https://confluence.atlassian.com/x/vYXKM).*

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Returns the [global settings](https://confluence.atlassian.com/x/qYXKM) in Jira. These settings determine whether optional features (for example, subtasks, time tracking, and others) are enabled. If time tracking is enabled, this operation also returns the time tracking configuration.

**[Permissions](#permissions) required:** Permission to access Jira .</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Returns reference data for JQL searches. This is a downloadable version of the documentation provided in [Advanced searching - fields reference](https://confluence.atlassian.com/x/gwORLQ) and [Advanced searching - functions reference](https://confluence.atlassian.com/x/hgORLQ), along with a list of JQL-reserved words. Use this information to assist with the programmatic creation of JQL queries or the validation of queries built in a custom query builder.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Returns the JQL search auto complete suggestions for a field.

Suggestions can be obtained by providing:

 *  `fieldName` to get a list of all values for the field.
 *  `fieldName` and `fieldValue` to get a list of values containing the text in `fieldValue`.
 *  `fieldName` and `predicateName` to get a list of all predicate values for the field.
 *  `fieldName`, `predicateName`, and `predicateValue` to get a list of predicate values containing the text in `predicateValue`.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Converts one or more JQL queries with user identifiers (username or user key) to equivalent JQL queries with account IDs.

You may wish to use this operation if your system stores JQL queries and you want to make them GDPR-compliant. For more information about GDPR-related changes, see the [migration guide](https://developer.atlassian.com/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/).

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Returns the value of a preference of the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Creates a preference for the user or updates a preference's value by sending a plain text string. For example, `false`. An arbitrary preference can be created with the value containing up to 255 characters. In addition, the following keys define system preferences that can be set or created:

 *  *user.notifications.mimetype* The mime type used in notifications sent to the user. Defaults to `html`.
 *  *user.notify.own.changes* Indicates whether the user gets notified of their own changes. Defaults to `false`.
 *  *jira.user.locale* The locale of the user. By default, not set: the user takes the instance locale. See also, [Set locale](#api-api-2-mypreferences-locale-put).
 *  *jira.user.timezone* The time zone of the user. By default, not set, the user takes the instance time zone.
 *  *user.default.share.private* Indicates whether new [ filters](https://confluence.atlassian.com/x/eQiiLQ) are set to private. Defaults to `true`.
 *  *user.keyboard.shortcuts.disabled* Indicates whether keyboard shortcuts are disabled. Defaults to `false`.
 *  *user.autowatch.disabled* Indicates whether the user automatically watches issues they create or add a comment to. By default, not set: the user takes the instance autowatch setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Deletes a preference of the user, which restores the default value of system defined settings.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Returns the locale for the user.

If the user has no language preference set (which is the default setting) or this resource is accessed anonymous, the browser locale detected by Jira is returned. Jira detects the browser locale using the *Accept-Language* header in the request. However, if this doesn't match a locale available Jira, the site default locale is returned.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Sets the locale of the user. The locale must be one supported by the instance of Jira.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Deletes the locale of the user, which restores the default setting.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Returns details for the current user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Returns a list of permissions indicating which permissions the user has. Details of the user's permissions can be obtained in a global, project, or issue context.

The user is reported as having a project permission:

 *  in the global context, if the user has the project permission in any project.
 *  for a project, where the project permission is determined using issue data, if the user meets the permission's criteria for any issue in the project. Otherwise, if the user has the project permission in the project.
 *  for an issue, where a project permission is determined using issue data, if the user has the permission in the issue. Otherwise, if the user has the project permission in the project containing the issue.

This means that users may be shown as having an issue permission (such as EDIT\_ISSUE) in the global context or a project context but may not have the permission for any or all issues. For example, if Reporters have the EDIT\_ISSUE permission a user would be shown as having this permission in the global context or the context of a project, because any user can be a reporter. However, if they are not the user who reported the issue queried they would not have EDIT\_ISSUE permission for that issue.

Global permissions are unaffected by context.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Returns all permissions, including:

 *  global permissions.
 *  project permissions.
 *  global permissions added by plugins.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Returns:

 *  for a list of global permissions, the global permissions granted to the user.
 *  for a list of project permissions and lists of projects and issues, for each project permission a list of the projects and issues the user can access or manipulate.

Note that:

 *  Invalid project and issue IDs are ignored.
 *  A maximum of 1000 projects and 1000 issues can be checked.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Returns all the projects where the user is granted a list of project permissions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Returns all permission schemes.

### About permission schemes and grants ###

A permission scheme is a collection of permission grants. A permission grant consists of a `holder` and a `permission`.

#### Holder ####

The `holder` object contains information about the user or group being granted the permission. For example, the *Administer projects* permission is granted to a group named *Teams in space administrators*. In this case, the type is `"type": "group"`, and the parameter is the group name, `"parameter": "Teams in space administrators"`. The `holder` object is defined by the following properties:

 *  `type` Identifies the user or group (see the list of types below).
 *  `parameter` The value of this property depends on the `type`. For example, if the `type` is a group, then you need to specify the group name.

The following `types` are available. The expected values for the `parameter` are given in parenthesis (some `types` may not have a `parameter`):

 *  `anyone` Grant for anonymous users.
 *  `applicationRole` Grant for users with access to the specified application (application name). See [Manage application access](https://confluence.atlassian.com/cloud/manage-application-access-744721629.html) for more information.
 *  `assignee` Grant for the user currently assigned to an issue.
 *  `group` Grant for the specified group (group name).
 *  `groupCustomField` Grant for a user in the group selected in the specified custom field (custom field ID).
 *  `projectLead` Grant for a project lead.
 *  `projectRole` Grant for the specified project role (project role ID).
 *  `reporter` Grant for the user who reported the issue.
 *  `sd.customer.portal.only` Jira Service Desk only. Grants customers permission to access the customer portal but not Jira. See [Customizing Jira Service Desk permissions](https://confluence.atlassian.com/x/24dKLg) for more information.
 *  `user` Grant for the specified user (user ID - historically this was the userkey but that is deprecated and the account ID should be used).
 *  `userCustomField` Grant for a user selected in the specified custom field (custom field ID).

#### Permissions ####

The [built-in Jira permissions](https://confluence.atlassian.com/x/yodKLg) are listed below. Apps can also define custom permissions. See the [project permission](https://developer.atlassian.com/cloud/jira/platform/modules/project-permission/) and [global permission](https://developer.atlassian.com/cloud/jira/platform/modules/global-permission/) module documentation for more information.

**Project permissions**

 *  `ADMINISTER_PROJECTS`
 *  `BROWSE_PROJECTS`
 *  `MANAGE_SPRINTS_PERMISSION` (Jira Software only)
 *  `SERVICEDESK_AGENT` (Jira Service Desk only)
 *  `VIEW_DEV_TOOLS` (Jira Software only)
 *  `VIEW_READONLY_WORKFLOW`

**Issue permissions**

 *  `ASSIGNABLE_USER`
 *  `ASSIGN_ISSUES`
 *  `CLOSE_ISSUES`
 *  `CREATE_ISSUES`
 *  `DELETE_ISSUES`
 *  `EDIT_ISSUES`
 *  `LINK_ISSUES`
 *  `MODIFY_REPORTER`
 *  `MOVE_ISSUES`
 *  `RESOLVE_ISSUES`
 *  `SCHEDULE_ISSUES`
 *  `SET_ISSUE_SECURITY`
 *  `TRANSITION_ISSUES`

**Voters and watchers permissions**

 *  `MANAGE_WATCHERS`
 *  `VIEW_VOTERS_AND_WATCHERS`

**Comments permissions**

 *  `ADD_COMMENTS`
 *  `DELETE_ALL_COMMENTS`
 *  `DELETE_OWN_COMMENTS`
 *  `EDIT_ALL_COMMENTS`
 *  `EDIT_OWN_COMMENTS`

**Attachments permissions**

 *  `CREATE_ATTACHMENTS`
 *  `DELETE_ALL_ATTACHMENTS`
 *  `DELETE_OWN_ATTACHMENTS`

**Time tracking permissions**

 *  `DELETE_ALL_WORKLOGS`
 *  `DELETE_OWN_WORKLOGS`
 *  `EDIT_ALL_WORKLOGS`
 *  `EDIT_OWN_WORKLOGS`
 *  `WORK_ON_ISSUES`

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Creates a new permission scheme. You can create a permission scheme with or without defining a set of permission grants.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Updates a permission scheme. Below are some important things to note when using this resource:

 *  If a permissions list is present in the request, then it is set in the permission scheme, overwriting *all existing* grants.
 *  If you want to update only the name and description, then do not send a permissions list in the request.
 *  Sending an empty list will remove all permission grants from the permission scheme.

If you want to add or delete a permission grant instead of updating the whole list, see [Create permission grant](#api-api-2-permissionscheme-schemeId-permission-post) or [Delete permission scheme entity](#api-api-2-permissionscheme-schemeId-permission-permissionId-delete).

See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Deletes a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Returns all permission grants for a permission scheme.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Creates a permission grant in a permission scheme.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Returns a permission grant.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Deletes a permission grant from a permission scheme. See [About permission schemes and grants](#about-permission-schemes) for more details.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Gets a [notification scheme](https://confluence.atlassian.com/x/8YdKLg) associated with the project. See the [Get notification scheme](#api-api-2-notificationscheme-id-get) resource for more information about notification schemes.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) or *Administer Projects* [project permission](https://confluence.atlassian.com/x/yodKLg).</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Returns all projects visible to the user. Deprecated, use [ Get projects paginated](#api-api-2-project-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** None, however, projects are returned only where:

 *  the user has *Browse Projects* or *Administer projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project.
 *  the project is public.</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Creates a project based on a project type template, as shown in the following table:

<table> 
 <thead> 
  <tr> 
   <th>Project Type Key</th> 
   <th>Project Template Key</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Returns all application roles. In Jira, application roles are managed using the [Application access configuration](https://confluence.atlassian.com/x/3YxjL) page.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Returns an application role.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Returns a list of audit records. The list can be filtered to include items:

 *  containing a string in at least one field. For example, providing *up* will return all audit records where one or more fields contains words such as *update*.
 *  created on or after a date and time.
 *  created or or before a date and time.
 *  created during a time period.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Returns a list of dashboards owned by or shared with the user. The list may be filtered to include only favorite or owned dashboards.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for dashboards. This operation is similar to [Get dashboards](#api-api-2-dashboard-get) except that the results can be refined to include dashboards that have specific attributes. For example, dashboards with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** None, however, only the following dashboards that match the query parameters are returned:

 *  Dashboards owned by the user. Not returned for anonymous users.
 *  Dashboards shared with a group that the user is a member of. Not returned for anonymous users.
 *  Dashboards shared with a private project that the user can browse. Not returned for anonymous users.
 *  Dashboards shared with a public project.
 *  Dashboards shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Returns the keys of all properties for a dashboard item.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get the property keys the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of a dashboard item property.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard item property the user must be the owner of the dashboard or be shared the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of a dashboard item property. Use this resource in apps to store custom data against a dashboard item.

A dashboard item enables an app to add user-specific information to a user dashboard. Dashboard items are exposed to users as gadgets that users can add to their dashboards. For more information on how users do this, see [Adding and customizing gadgets](https://confluence.atlassian.com/x/7AeiLQ).

When an app creates a dashboard item it registers a callback to receive the dashboard item ID. The callback fires whenever the item is rendered or, where the item is configurable, the user edits the item. The app then uses this resource to store the item's content or configuration details. For more information on working with dashboard items, see [ Building a dashboard item for a JIRA Connect add-on](https://developer.atlassian.com/server/jira/platform/guide-building-a-dashboard-item-for-a-jira-connect-add-on-33746254/) and the [Dashboard Item](https://developer.atlassian.com/cloud/jira/platform/modules/dashboard-item/) documentation.

There is no resource to set or get dashboard items.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:** Permission to access Jira. However, to set a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Deletes a dashboard item property.

**[Permissions](#permissions) required:** Permission to access Jira. However, to delete a dashboard item property the user must be the owner of the dashboard. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Returns a dashboard.

**[Permissions](#permissions) required:** Permission to access Jira. However, to get a dashboard, the dashboard must be shared with the user or the user must own it. Note, users with the *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg) are considered owners of the System dashboard. The System dashboard is considered to be shared with all other users.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Returns all filters. Deprecated, use [ Search for filters](#api-api-2-filter-search-get) that supports search and pagination.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Creates a filter. The filter is shared according to the [default share scope](#api-api-2-filter-post). The filter is not selected as a favorite.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Returns the visible favorite filters of the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Returns the filters owned by the user. If `includeFavourites` is `true`, the user's visible favorite filters are also returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, a favorite filters is only visible to the user where the filter is:

 *  owned by the user.
 *  shared with a group that the user is a member of.
 *  shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  shared with a public project.
 *  shared with the public.

For example, if the user favorites a public filter that is subsequently made private that filter is not returned by this operation.</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Searches for filters. This operation is similar to [Get filters](#api-api-2-filter-get) except that the results can be refined to include filters that have specific attributes. For example, filters with a particular name. When multiple attributes are specified only filters matching all attributes are returned.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following filters that match the query parameters are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Returns a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Updates a filter. Use this operation to update a filter's name, description, JQL, or sharing.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete a filter.

**[Permissions](#permissions) required:** Permission to access Jira, however filters can only be deleted by the creator of the filter or a user with *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Returns the columns configured for a filter. The column configuration is used when the filter's results are viewed in *List View* with the *Columns* set to *Filter*.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Sets the columns for a filter. Only navigable fields can be set as columns. Use [Get fields](#api-api-2-field-get) to get the list fields in Jira. A navigable field has `navigable` set to `true`.

The parameters for this resource are expressed as HTML form data. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/filter/10000/columns`

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only set for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset the user's column configuration for the filter to the default.

**[Permissions](#permissions) required:** Permission to access Jira, however, columns are only reset for:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add a filter as a favorite for the user.

**[Permissions](#permissions) required:** Permission to access Jira, however, the user can only favorite:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Removes a filter as a favorite for the user. Note that this operation only removes filters visible to the user from the user's favorites list. For example, if the user favorites a public filter that is subsequently made private (and is therefore no longer visible on their favorites list) they cannot remove it from their favorites list.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Returns the default sharing settings for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Sets the default sharing for new filters and dashboards for a user.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Returns the share permissions for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add a share permissions to a filter. If you add a global share permission (one for all logged-in users or the public) it will overwrite all share permissions for the filter.

Be aware that this operation uses different objects for updating share permissions compared to [Update filter](#api-api-2-filter-id-put).

**[Permissions](#permissions) required:** *Share dashboards and filters* [global permission](https://confluence.atlassian.com/x/x4dKLg) and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Returns a share permission for a filter. A filter can be shared with groups, projects, all logged-in users, or the public. Sharing with all logged-in users or the public is known as a global share permission.

**[Permissions](#permissions) required:** Permission to access Jira, however, only the following are returned:

 *  filters owned by the user.
 *  filters shared with a group that the user is a member of.
 *  filters shared with a private project that the user has *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for.
 *  filters shared with a public project.
 *  filters shared with the public.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Deletes a share permission from a filter.

**[Permissions](#permissions) required:** Permission to access Jira and the user must own the filter.</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Returns the default issue navigator columns.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Sets the default issue navigator columns.

The `columns` parameter accepts a navigable field value and is expressed as HTML form data. To specify multiple columns, pass multiple `columns` parameters. For example, in curl:

`curl -X PUT -d columns=summary -d columns=description https://your-domain.atlassian.net/rest/api/2/settings/columns`

If no column details are sent, then all default columns are removed.

A navigable field is one that can be used as a column on the issue navigator. Find details of navigable issue columns using [Get fields](#api-api-2-field-get).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Returns a [paginated](#pagination) list of [notification schemes](https://confluence.atlassian.com/x/8YdKLg) in order by display name.

### About notification schemes ###

A notification scheme is a list of events and recipients who will receive notifications for those events. The list is contained within the `notificationSchemeEvents` object and contains pairs of `events` and `notifications`:

 *  `event` Identifies the type of event. The events can be [Jira system events](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-eventsEvents) or [custom events](https://confluence.atlassian.com/x/AIlKLg).
 *  `notifications` Identifies the [recipients](https://confluence.atlassian.com/x/8YdKLg#Creatinganotificationscheme-recipientsRecipients) of notifications for each event. Recipients can be any of the following types:
    
     *  `CurrentAssignee`
     *  `Reporter`
     *  `CurrentUser`
     *  `ProjectLead`
     *  `ComponentLead`
     *  `User` (the `parameter` is the user key)
     *  `Group` (the `parameter` is the group name)
     *  `ProjectRole` (the `parameter` is the project role ID)
     *  `EmailAddress`
     *  `AllWatchers`
     *  `UserCustomField` (the `parameter` is the ID of the custom field)
     *  `GroupCustomField`(the `parameter` is the ID of the custom field)

*Note that you should allow for events without recipients to appear in responses.*

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with a notification scheme for it to be returned.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Returns a [notification scheme](https://confluence.atlassian.com/x/8YdKLg), including the list of events and the recipients who will receive notifications for those events.

**[Permissions](#permissions) required:** Permission to access Jira, however the user must have permission to administer at least one project associated with the notification scheme.</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Sets a property value on multiple issues. The issues to be updated can be specified by a filter.

The filter identifies issues eligible for update using these criteria:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.
 *  `hasProperty`:
    
     *  If *true*, only issues with the property are eligible.
     *  If *false*, only issues without the property are eligible.

If more than one criteria is specified, they are joined with the logical *AND*: only issues that satisfy all criteria are eligible.

If an invalid combination of criteria is provided, an error is returned. For example, specifying a `currentValue` and `hasProperty` as *false* would not match any issues (because without the property the property cannot have a value).

The filter is optional. Without the filter all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either all eligible issues are updated or, when errors occur, none are updated.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Deletes a property value from multiple issues. The issues to be updated can be specified by filter criteria.

The criteria the filter used to identify eligible issues are:

 *  `entityIds` Only issues from this list are eligible.
 *  `currentValue` Only issues with the property set to this value are eligible.

If both criteria is specified, they are joined with the logical *AND*: only issues that satisfy both criteria are considered eligible.

If no filter criteria are specified, all the issues visible to the user and where the user has the EDIT\_ISSUES permission for the issue are considered eligible.

This operation is:

 *  transactional, either the property is deleted from all eligible issues or, when errors occur, no properties are deleted.
 *  [asynchronous](#async). Follow the `location` link in the response to determine the status of the task and use [Get task](#api-api-2-task-taskId-get) to obtain subsequent updates.

**[Permissions](#permissions) required:**

 *  *Browse projects* [ project permission](https://confluence.atlassian.com/x/yodKLg) for each project containing issues.
 *  If configured, permission to view granted by [issue-level security](https://confluence.atlassian.com/x/J4lKLg) for each issue.
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for each issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Returns the URLs and keys of an issue's properties.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Returns the key and value of an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Sets the value of an issue's property. Use this resource to store custom data against an issue.

The value of the request body must be a [valid](http://tools.ietf.org/html/rfc4627), non-empty JSON blob. The maximum length is 32768 characters.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Deletes an issue's property.

**[Permissions](#permissions) required:**

 *  *Browse projects* [project permission](https://confluence.atlassian.com/x/yodKLg) for the project that the issue is in.
 *  If configured, permission to view the issue granted by [ issue-level security](https://confluence.atlassian.com/x/J4lKLg).
 *  *Edit issues* [project permission](https://confluence.atlassian.com/x/yodKLg) for the issue.</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

If the JQL query expression is too large to be encoded as a query parameter, use the [POST](#api-api-2-search-post) version of this resource.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Searches for issues using [JQL](https://confluence.atlassian.com/x/egORLQ).

There is a [GET](#api-api-2-search-get) version of this resource that can be used for smaller JQL query expressions.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Returns lists of issues matching a query string. Use this resource to provide auto-completion suggestions when the user is looking for an issue using a word or string.

This operation returns two lists:

 *  `History Search` which includes issues from the user's history of created, edited, or viewed issues that contain the string in the `query` parameter.
 *  `Current Search` which includes issues that match the JQL expression in `currentJQL` and contain the string in the `query` parameter.

**[Permissions](#permissions) required:** Permission to access Jira.</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluates a Jira expression and returns its value.

This resource can be used to test Jira expressions that you plan to use elsewhere, or to fetch data in a flexible way. Consult the [Jira expressions documentation](https://developer.atlassian.com/cloud/jira/platform/jira-expressions/) for more details.

#### Context variables ####

The following context variables are available to Jira expressions evaluated by this resource. Their presence depends on various factors; usually you need to manually request them in the context object sent in the payload, but some of them are added automatically under certain conditions.

 *  `user` ([User](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#user)): The current user. Always available and equal to `null` if the request is anonymous.
 *  `app` ([App](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#app)): The Connect app that made the request. Available only for authenticated requests made by Connect Apps (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)).
 *  `issue` ([Issue](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#issue)): The current issue. Available only when the issue is provided in the request context object.
 *  `project` ([Project](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#project)): The current project. Available only when the project is provided in the request context object.
 *  `sprint` ([Sprint](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#sprint)): The current sprint. Available only when the sprint is provided in the request context object.
 *  `board` ([Board](https://developer.atlassian.com/cloud/jira/platform/jira-expressions-type-reference#board)): The current board. Available only when the board is provided in the request context object.

**[Permissions](#permissions) required**: Permission to access Jira. However, an expression may return different results for different users depending on their permissions. For example, different users may see different comments on the same issue.  
Permission to access Jira Software is required to access Jira Software context variables (`board` and `sprint`) or fields (for example, `issue.sprint`).</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Returns all application properties or an application property.

If you specify a value for the `key` parameter, then an application property is returned as an object (not in an array). Otherwise, an array of all editable application properties is returned. See [Set application property](#api-api-2-application-properties-id-put) for descriptions of editable properties.

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Returns the application properties that are accessible on the *Advanced Settings* page. To navigate to the *Advanced Settings* page in Jira, choose the Jira icon > **Jira settings** > **System**, **General Configuration** and then click **Advanced Settings** (in the upper right).

**[Permissions](#permissions) required:** *Administer Jira* [global permission](https://confluence.atlassian.com/x/x4dKLg).</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Changes the value of an application property. For example, you can change the value of the `jira.clone.prefix` from its default value of *CLONE -* to *Clone -* if you prefer sentence case capitalization. Editable properties are described below along with their default values.

#### Advanced settings ####

The advanced settings below are also accessible in [Jira](https://confluence.atlassian.com/x/vYXKM).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.clone.prefix</code></td> 
   <td>A string of text that automatically precedes the title of a cloned issue.</td> 
   <td><code>CLONE -</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated dates. This must be the same as the <code>jira.date.picker.javascript.format</code> format setting.</td> 
   <td><code>d/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated dates. This must be the same as the <code>jira.date.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.java.format</code></td> 
   <td>The date format for the Java (server-side) generated date times. This must be the same as the <code>jira.date.time.picker.javascript.format</code> format setting.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.javascript.format</code></td> 
   <td>This date format is for the JavaScript (client-side) generated date times. This must be the same as the <code>jira.date.time.picker.java.format</code> format setting.</td> 
   <td><code>%e/%b/%y %I:%M %p</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.issue.actions.order</code></td> 
   <td>The default order of actions (such as <em>Comments</em> or <em>Change history</em>) displayed on the issue view.</td> 
   <td><code>asc</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.table.cols.subtasks</code></td> 
   <td>The columns to show while viewing subtask issues in a table. For example, a list of subtasks on an issue.</td> 
   <td><code>issuetype, status, assignee, progress</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.view.issue.links.sort.order</code></td> 
   <td>The sort order of the list of issue links on the issue view.</td> 
   <td><code>type, status, priority</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.comment.collapsing.minimum.hidden</code></td> 
   <td>The minimum number of comments required for comment collapsing to occur. A value of <code>0</code> disables comment collapsing.</td> 
   <td><code>4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.newsletter.tip.delay.days</code></td> 
   <td>The number of days before a prompt to sign up to the Jira Insiders newsletter is shown. A value of <code>-1</code> disables this functionality.</td> 
   <td><code>7</code></td> 
  </tr> 
 </tbody> 
</table>

#### Look and feel ####

The settings listed below adjust the [look and feel](https://confluence.atlassian.com/x/VwCLLg).

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr> 
 </thead> 
 <tbody> 
  <tr> 
   <td><code>jira.lf.date.time</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">time format</a>.</td> 
   <td><code>h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.day</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">day format</a>.</td> 
   <td><code>EEEE h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.complete</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date and time format</a>.</td> 
   <td><code>dd/MMM/yy h:mm a</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.date.dmy</code></td> 
   <td>Look and feel of the <a href="https://docs.oracle.com/javase/6/docs/api/index.html?java/text/SimpleDateFormat.html" rel="nofollow">date format</a>.</td> 
   <td><code>dd/MMM/yy</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.date.time.picker.use.iso8061</code></td> 
   <td>When enabled, sets Monday as the first day of the week in the date picker, as specified by the ISO8601 standard.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.url</code></td> 
   <td>The URL of the logo image file.</td> 
   <td><code>/images/icon-jira-logo.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.logo.show.application.title</code></td> 
   <td>Controls the visibility of the application title on the sidebar.</td> 
   <td><code>false</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.url</code></td> 
   <td>The URL of the favicon.</td> 
   <td><code>/favicon.ico</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.favicon.hires.url</code></td> 
   <td>The URL of the high resolution favicon.</td> 
   <td><code>/images/64jira.png</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.bgcolour</code></td> 
   <td>The background color of the sidebar.</td> 
   <td><code>#0747A6</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.top.adg3.textcolour</code></td> 
   <td>The color of the text and logo of the sidebar.</td> 
   <td><code>#DEEBFF</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.lf.hero.button.base.bg.colour</code></td> 
   <td></td> 
   <td><code>#3b7fc4</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.title</code></td> 
   <td>The text for the application title. The application title can also be set in <em>General settings</em>.</td> 
   <td><code>Jira</code></td> 
  </tr> 
  <tr> 
   <td><code>jira.option.globalsharing</code></td> 
   <td>boolean</td> 
   <td><code>true</code></td> 
  </tr> 
  <tr> 
   <td><code>xflow.product.suggestions.enabled</code></td> 
   <td>Indicates whether to expose product suggestions for other Atlassian products within Jira.</td> 
   <td><code>true</code></td> 
  </tr> 
 </tbody> 
</table>

#### Other settings ####

<table> 
 <thead> 
  <tr> 
   <th>Key</th> 
   <th>Description</th> 
   <th>Default value</th> 
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrole(callback)</td>
    <td style="padding:15px">Get all application roles</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationrolekey(key, callback)</td>
    <td style="padding:15px">Get application role</td>
    <td style="padding:15px">{base_path}/{version}/applicationrole/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2auditingrecord(offset, limit, filter, from, to, callback)</td>
    <td style="padding:15px">Get audit records</td>
    <td style="padding:15px">{base_path}/{version}/auditing/record?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboard(filter, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get all dashboards</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardsearch(dashboardName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Search for dashboards</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdproperties(dashboardId, itemId, callback)</td>
    <td style="padding:15px">Get dashboard item property keys</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Get dashboard item property</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, body, callback)</td>
    <td style="padding:15px">Set dashboard item property</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2dashboarddashboardIditemsitemIdpropertiespropertyKey(dashboardId, itemId, propertyKey, callback)</td>
    <td style="padding:15px">Delete dashboard item property</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/items/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2dashboardid(id, callback)</td>
    <td style="padding:15px">Get dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filter(expand, callback)</td>
    <td style="padding:15px">Get filters</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filter(expand, body, callback)</td>
    <td style="padding:15px">Create filter</td>
    <td style="padding:15px">{base_path}/{version}/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterfavourite(expand, callback)</td>
    <td style="padding:15px">Get favorite filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtermy(expand, includeFavourites, callback)</td>
    <td style="padding:15px">Get my filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/my?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filtersearch(filterName, accountId, owner, groupname, projectId, orderBy, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Search for filters</td>
    <td style="padding:15px">{base_path}/{version}/filter/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterid(id, expand, callback)</td>
    <td style="padding:15px">Get filter</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterid(id, expand, body, callback)</td>
    <td style="padding:15px">Update filter</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filterid(id, callback)</td>
    <td style="padding:15px">Delete filter</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Get columns</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridcolumns(id, body, callback)</td>
    <td style="padding:15px">Set columns</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridcolumns(id, callback)</td>
    <td style="padding:15px">Reset columns</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Add filter as favorite</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridfavourite(id, expand, callback)</td>
    <td style="padding:15px">Remove filter as favorite</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/favourite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filterdefaultShareScope(callback)</td>
    <td style="padding:15px">Get default share scope</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2filterdefaultShareScope(body, callback)</td>
    <td style="padding:15px">Set default share scope</td>
    <td style="padding:15px">{base_path}/{version}/filter/defaultShareScope?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermission(id, callback)</td>
    <td style="padding:15px">Get share permissions</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2filteridpermission(id, body, callback)</td>
    <td style="padding:15px">Add share permission</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Get share permission</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2filteridpermissionpermissionId(id, permissionId, callback)</td>
    <td style="padding:15px">Delete share permission</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2settingscolumns(callback)</td>
    <td style="padding:15px">Get issue navigator default columns</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2settingscolumns(body, callback)</td>
    <td style="padding:15px">Set issue navigator default columns</td>
    <td style="padding:15px">{base_path}/{version}/settings/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationscheme(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Get notification schemes paginated</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2notificationschemeid(id, expand, callback)</td>
    <td style="padding:15px">Get notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Bulk set issue property</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuepropertiespropertyKey(propertyKey, body, callback)</td>
    <td style="padding:15px">Bulk delete issue property</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyproperties(issueIdOrKey, callback)</td>
    <td style="padding:15px">Get issue property keys</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Get issue property</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Set issue property</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeypropertiespropertyKey(issueIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Delete issue property</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2search(jql, startAt, maxResults, validateQuery, fields, expand, properties, fieldsByKeys, callback)</td>
    <td style="padding:15px">Search for issues using JQL (GET)</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2search(body, callback)</td>
    <td style="padding:15px">Search for issues using JQL (POST)</td>
    <td style="padding:15px">{base_path}/{version}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuepicker(query, currentJQL, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent, callback)</td>
    <td style="padding:15px">Get issue picker suggestions</td>
    <td style="padding:15px">{base_path}/{version}/issue/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2expressioneval(expand, body, callback)</td>
    <td style="padding:15px">Evaluate Jira expression</td>
    <td style="padding:15px">{base_path}/{version}/expression/eval?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationProperties(key, permissionLevel, keyFilter, callback)</td>
    <td style="padding:15px">Get application property</td>
    <td style="padding:15px">{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2applicationPropertiesadvancedSettings(callback)</td>
    <td style="padding:15px">Get advanced settings</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/advanced-settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2applicationPropertiesid(id, body, callback)</td>
    <td style="padding:15px">Set application property</td>
    <td style="padding:15px">{base_path}/{version}/application-properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configuration(callback)</td>
    <td style="padding:15px">Get global settings</td>
    <td style="padding:15px">{base_path}/{version}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedata(callback)</td>
    <td style="padding:15px">Get field reference data</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2jqlautocompletedatasuggestions(fieldName, fieldValue, predicateName, predicateValue, callback)</td>
    <td style="padding:15px">Get field auto complete suggestions</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata/suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2jqlpdcleaner(body, callback)</td>
    <td style="padding:15px">Convert user identifiers to account IDs in JQL queries</td>
    <td style="padding:15px">{base_path}/{version}/jql/pdcleaner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Get preference</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferences(key, body, callback)</td>
    <td style="padding:15px">Set preference</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferences(key, callback)</td>
    <td style="padding:15px">Delete preference</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Get locale</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2mypreferenceslocale(body, callback)</td>
    <td style="padding:15px">Set locale</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2mypreferenceslocale(callback)</td>
    <td style="padding:15px">Delete locale</td>
    <td style="padding:15px">{base_path}/{version}/mypreferences/locale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2myself(expand, callback)</td>
    <td style="padding:15px">Get current user</td>
    <td style="padding:15px">{base_path}/{version}/myself?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2mypermissions(projectKey, projectId, issueKey, issueId, permissions, callback)</td>
    <td style="padding:15px">Get my permissions</td>
    <td style="padding:15px">{base_path}/{version}/mypermissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissions(callback)</td>
    <td style="padding:15px">Get all permissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheck(body, callback)</td>
    <td style="padding:15px">Get bulk permissions</td>
    <td style="padding:15px">{base_path}/{version}/permissions/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionsproject(body, callback)</td>
    <td style="padding:15px">Get permitted projects</td>
    <td style="padding:15px">{base_path}/{version}/permissions/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionscheme(expand, callback)</td>
    <td style="padding:15px">Get all permission schemes</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionscheme(expand, body, callback)</td>
    <td style="padding:15px">Create permission scheme</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeId(schemeId, expand, callback)</td>
    <td style="padding:15px">Get permission scheme</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2permissionschemeschemeId(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Update permission scheme</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeId(schemeId, callback)</td>
    <td style="padding:15px">Delete permission scheme</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermission(schemeId, expand, callback)</td>
    <td style="padding:15px">Get permission scheme grants</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2permissionschemeschemeIdpermission(schemeId, expand, body, callback)</td>
    <td style="padding:15px">Create permission grant</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, expand, callback)</td>
    <td style="padding:15px">Get permission scheme grant</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2permissionschemeschemeIdpermissionpermissionId(schemeId, permissionId, callback)</td>
    <td style="padding:15px">Delete permission scheme grant</td>
    <td style="padding:15px">{base_path}/{version}/permissionscheme/{pathv1}/permission/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdnotificationscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Get project notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2project(expand, recent, callback)</td>
    <td style="padding:15px">Get all projects</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2project(body, callback)</td>
    <td style="padding:15px">Create project</td>
    <td style="padding:15px">{base_path}/{version}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectsearch(startAt, maxResults, orderBy, query, typeKey, categoryId, action, expand, callback)</td>
    <td style="padding:15px">Get projects paginated</td>
    <td style="padding:15px">{base_path}/{version}/project/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKey(projectIdOrKey, expand, callback)</td>
    <td style="padding:15px">Get project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectprojectIdOrKey(projectIdOrKey, expand, body, callback)</td>
    <td style="padding:15px">Update project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2projectprojectIdOrKey(projectIdOrKey, callback)</td>
    <td style="padding:15px">Delete project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeystatuses(projectIdOrKey, callback)</td>
    <td style="padding:15px">Get all statuses for project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectprojectIdOrKeytypenewProjectTypeKey(projectIdOrKey, newProjectTypeKey, callback)</td>
    <td style="padding:15px">Update project type</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/type/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdpermissionscheme(projectKeyOrId, expand, callback)</td>
    <td style="padding:15px">Get assigned permission scheme</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectprojectKeyOrIdpermissionscheme(projectKeyOrId, expand, body, callback)</td>
    <td style="padding:15px">Assign permission scheme</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/permissionscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdsecuritylevel(projectKeyOrId, callback)</td>
    <td style="padding:15px">Get project issue security levels</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/securitylevel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectKeyOrIdissuesecuritylevelscheme(projectKeyOrId, callback)</td>
    <td style="padding:15px">Get project issue security scheme</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/issuesecuritylevelscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projecttype(callback)</td>
    <td style="padding:15px">Get all project types</td>
    <td style="padding:15px">{base_path}/{version}/project/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projecttypeprojectTypeKey(projectTypeKey, callback)</td>
    <td style="padding:15px">Get project type by key</td>
    <td style="padding:15px">{base_path}/{version}/project/type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projecttypeprojectTypeKeyaccessible(projectTypeKey, callback)</td>
    <td style="padding:15px">Get accessible project type by key</td>
    <td style="padding:15px">{base_path}/{version}/project/type/{pathv1}/accessible?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2serverInfo(callback)</td>
    <td style="padding:15px">Get Jira instance info</td>
    <td style="padding:15px">{base_path}/{version}/serverInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2tasktaskId(taskId, callback)</td>
    <td style="padding:15px">Get task</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2tasktaskIdcancel(taskId, callback)</td>
    <td style="padding:15px">Cancel task</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configurationtimetracking(callback)</td>
    <td style="padding:15px">Get selected time tracking provider</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timetracking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2configurationtimetracking(body, callback)</td>
    <td style="padding:15px">Select time tracking provider</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timetracking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2configurationtimetracking(callback)</td>
    <td style="padding:15px">Disable time tracking</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timetracking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configurationtimetrackinglist(callback)</td>
    <td style="padding:15px">Get all time tracking providers</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timetracking/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2configurationtimetrackingoptions(callback)</td>
    <td style="padding:15px">Get time tracking settings</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timetracking/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2configurationtimetrackingoptions(body, callback)</td>
    <td style="padding:15px">Set time tracking settings</td>
    <td style="padding:15px">{base_path}/{version}/configuration/timetracking/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userbulk(startAt, maxResults, username, key, accountId, callback)</td>
    <td style="padding:15px">Bulk get users</td>
    <td style="padding:15px">{base_path}/{version}/user/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userbulkmigration(startAt, maxResults, username, key, callback)</td>
    <td style="padding:15px">Bulk get users migration</td>
    <td style="padding:15px">{base_path}/{version}/user/bulk/migration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2user(accountId, username, key, expand, callback)</td>
    <td style="padding:15px">Get user</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2user(body, callback)</td>
    <td style="padding:15px">Create user</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2user(accountId, username, key, callback)</td>
    <td style="padding:15px">Delete user</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2usercolumns(accountId, username, callback)</td>
    <td style="padding:15px">Get user default columns</td>
    <td style="padding:15px">{base_path}/{version}/user/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2usercolumns(accountId, body, callback)</td>
    <td style="padding:15px">Set user default columns</td>
    <td style="padding:15px">{base_path}/{version}/user/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2usercolumns(accountId, username, callback)</td>
    <td style="padding:15px">Reset user default columns</td>
    <td style="padding:15px">{base_path}/{version}/user/columns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2usergroups(accountId, username, key, callback)</td>
    <td style="padding:15px">Get user groups</td>
    <td style="padding:15px">{base_path}/{version}/user/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userproperties(accountId, userKey, username, callback)</td>
    <td style="padding:15px">Get user property keys</td>
    <td style="padding:15px">{base_path}/{version}/user/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userpropertiespropertyKey(accountId, userKey, username, propertyKey, callback)</td>
    <td style="padding:15px">Get user property</td>
    <td style="padding:15px">{base_path}/{version}/user/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2userpropertiespropertyKey(accountId, userKey, username, propertyKey, body, callback)</td>
    <td style="padding:15px">Set user property</td>
    <td style="padding:15px">{base_path}/{version}/user/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2userpropertiespropertyKey(accountId, userKey, username, propertyKey, callback)</td>
    <td style="padding:15px">Delete user property</td>
    <td style="padding:15px">{base_path}/{version}/user/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2usersearchquery(query, startAt, maxResults, includeInactive, callback)</td>
    <td style="padding:15px">Find users by query</td>
    <td style="padding:15px">{base_path}/{version}/user/search/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2usersearchquerykey(query, startAt, maxResults, includeInactive, callback)</td>
    <td style="padding:15px">Find user keys by query</td>
    <td style="padding:15px">{base_path}/{version}/user/search/query/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userassignablemultiProjectSearch(query, username, accountId, projectKeys, startAt, maxResults, callback)</td>
    <td style="padding:15px">Find users assignable to projects</td>
    <td style="padding:15px">{base_path}/{version}/user/assignable/multiProjectSearch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userassignablesearch(query, username, accountId, project, issueKey, startAt, maxResults, actionDescriptorId, callback)</td>
    <td style="padding:15px">Find users assignable to issues</td>
    <td style="padding:15px">{base_path}/{version}/user/assignable/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userpermissionsearch(query, username, accountId, permissions, issueKey, projectKey, startAt, maxResults, callback)</td>
    <td style="padding:15px">Find users with permissions</td>
    <td style="padding:15px">{base_path}/{version}/user/permission/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userpicker(query, maxResults, showAvatar, exclude, excludeAccountIds, callback)</td>
    <td style="padding:15px">Find users for picker</td>
    <td style="padding:15px">{base_path}/{version}/user/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2usersearch(query, username, accountId, startAt, maxResults, includeActive, includeInactive, property, callback)</td>
    <td style="padding:15px">Find users</td>
    <td style="padding:15px">{base_path}/{version}/user/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2userviewissuesearch(query, username, accountId, issueKey, projectKey, startAt, maxResults, callback)</td>
    <td style="padding:15px">Find users with browse permission</td>
    <td style="padding:15px">{base_path}/{version}/user/viewissue/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflow(workflowName, callback)</td>
    <td style="padding:15px">Get all workflows</td>
    <td style="padding:15px">{base_path}/{version}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2workflowscheme(body, callback)</td>
    <td style="padding:15px">Create workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeid(id, returnDraftIfExists, callback)</td>
    <td style="padding:15px">Get workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeid(id, body, callback)</td>
    <td style="padding:15px">Update workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeid(id, callback)</td>
    <td style="padding:15px">Delete workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeiddefault(id, returnDraftIfExists, callback)</td>
    <td style="padding:15px">Get default workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeiddefault(id, body, callback)</td>
    <td style="padding:15px">Update default workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeiddefault(id, updateDraftIfNeeded, callback)</td>
    <td style="padding:15px">Delete default workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeidissuetypeissueType(id, issueType, returnDraftIfExists, callback)</td>
    <td style="padding:15px">Get workflow for issue type in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeidissuetypeissueType(id, issueType, body, callback)</td>
    <td style="padding:15px">Set workflow for issue type in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeidissuetypeissueType(id, issueType, updateDraftIfNeeded, callback)</td>
    <td style="padding:15px">Delete workflow for issue type in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeidworkflow(id, workflowName, returnDraftIfExists, callback)</td>
    <td style="padding:15px">Get issue types for workflows in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeidworkflow(id, workflowName, body, callback)</td>
    <td style="padding:15px">Set issue types for workflow in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeidworkflow(id, workflowName, updateDraftIfNeeded, callback)</td>
    <td style="padding:15px">Delete issue types for workflow in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2workflowschemeidcreatedraft(id, callback)</td>
    <td style="padding:15px">Create draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/createdraft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeiddraft(id, callback)</td>
    <td style="padding:15px">Get draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeiddraft(id, body, callback)</td>
    <td style="padding:15px">Update draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeiddraft(id, callback)</td>
    <td style="padding:15px">Delete draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeiddraftdefault(id, callback)</td>
    <td style="padding:15px">Get draft default workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeiddraftdefault(id, body, callback)</td>
    <td style="padding:15px">Update draft default workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeiddraftdefault(id, callback)</td>
    <td style="padding:15px">Delete draft default workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeiddraftissuetypeissueType(id, issueType, callback)</td>
    <td style="padding:15px">Get workflow for issue type in draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeiddraftissuetypeissueType(id, issueType, body, callback)</td>
    <td style="padding:15px">Set workflow for issue type in draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeiddraftissuetypeissueType(id, issueType, callback)</td>
    <td style="padding:15px">Delete workflow for issue type in draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowschemeiddraftworkflow(id, workflowName, callback)</td>
    <td style="padding:15px">Get issue types for workflows in draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowschemeiddraftworkflow(id, workflowName, body, callback)</td>
    <td style="padding:15px">Set issue types for workflow in workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowschemeiddraftworkflow(id, workflowName, callback)</td>
    <td style="padding:15px">Delete issue types for workflow in draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2workflowtransitionstransitionIdproperties(transitionId, includeReservedKeys, key, workflowName, workflowMode, callback)</td>
    <td style="padding:15px">Get workflow transition properties</td>
    <td style="padding:15px">{base_path}/{version}/workflow/transitions/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2workflowtransitionstransitionIdproperties(transitionId, key, workflowName, workflowMode, body, callback)</td>
    <td style="padding:15px">Update workflow transition property</td>
    <td style="padding:15px">{base_path}/{version}/workflow/transitions/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2workflowtransitionstransitionIdproperties(transitionId, key, workflowName, workflowMode, body, callback)</td>
    <td style="padding:15px">Create workflow transition property</td>
    <td style="padding:15px">{base_path}/{version}/workflow/transitions/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2workflowtransitionstransitionIdproperties(transitionId, key, workflowName, workflowMode, callback)</td>
    <td style="padding:15px">Delete workflow transition property</td>
    <td style="padding:15px">{base_path}/{version}/workflow/transitions/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2attachmentmeta(callback)</td>
    <td style="padding:15px">Get Jira attachment settings</td>
    <td style="padding:15px">{base_path}/{version}/attachment/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2attachmentid(id, callback)</td>
    <td style="padding:15px">Get attachment metadata</td>
    <td style="padding:15px">{base_path}/{version}/attachment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2attachmentid(id, callback)</td>
    <td style="padding:15px">Delete attachment</td>
    <td style="padding:15px">{base_path}/{version}/attachment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2attachmentidexpandhuman(id, callback)</td>
    <td style="padding:15px">Get all metadata for an expanded attachment</td>
    <td style="padding:15px">{base_path}/{version}/attachment/{pathv1}/expand/human?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2attachmentidexpandraw(id, callback)</td>
    <td style="padding:15px">Get contents metadata for an expanded attachment</td>
    <td style="padding:15px">{base_path}/{version}/attachment/{pathv1}/expand/raw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeyattachments(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Add attachment</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2avatartypesystem(type, callback)</td>
    <td style="padding:15px">Get system avatars by type</td>
    <td style="padding:15px">{base_path}/{version}/avatar/{pathv1}/system?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2universalAvatartypetypeownerentityId(type, entityId, callback)</td>
    <td style="padding:15px">Get avatars</td>
    <td style="padding:15px">{base_path}/{version}/universal_avatar/type/{pathv1}/owner/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2universalAvatartypetypeownerentityId(type, entityId, x, y, size, body, callback)</td>
    <td style="padding:15px">Load avatar</td>
    <td style="padding:15px">{base_path}/{version}/universal_avatar/type/{pathv1}/owner/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2universalAvatartypetypeownerowningObjectIdavatarid(type, owningObjectId, id, callback)</td>
    <td style="padding:15px">Delete avatar</td>
    <td style="padding:15px">{base_path}/{version}/universal_avatar/type/{pathv1}/owner/{pathv2}/avatar/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2commentlist(expand, body, callback)</td>
    <td style="padding:15px">Get comments by IDs</td>
    <td style="padding:15px">{base_path}/{version}/comment/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeycomment(issueIdOrKey, startAt, maxResults, orderBy, expand, callback)</td>
    <td style="padding:15px">Get comments</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/comment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeycomment(issueIdOrKey, expand, body, callback)</td>
    <td style="padding:15px">Add comment</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/comment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeycommentid(issueIdOrKey, id, expand, callback)</td>
    <td style="padding:15px">Get comment</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/comment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeycommentid(issueIdOrKey, id, expand, body, callback)</td>
    <td style="padding:15px">Update comment</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/comment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeycommentid(issueIdOrKey, id, callback)</td>
    <td style="padding:15px">Delete comment</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/comment/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2commentcommentIdproperties(commentId, callback)</td>
    <td style="padding:15px">Get comment property keys</td>
    <td style="padding:15px">{base_path}/{version}/comment/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2commentcommentIdpropertiespropertyKey(commentId, propertyKey, callback)</td>
    <td style="padding:15px">Get comment property</td>
    <td style="padding:15px">{base_path}/{version}/comment/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2commentcommentIdpropertiespropertyKey(commentId, propertyKey, body, callback)</td>
    <td style="padding:15px">Set comment property</td>
    <td style="padding:15px">{base_path}/{version}/comment/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2commentcommentIdpropertiespropertyKey(commentId, propertyKey, callback)</td>
    <td style="padding:15px">Delete comment property</td>
    <td style="padding:15px">{base_path}/{version}/comment/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2component(body, callback)</td>
    <td style="padding:15px">Create component</td>
    <td style="padding:15px">{base_path}/{version}/component?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2componentid(id, callback)</td>
    <td style="padding:15px">Get component</td>
    <td style="padding:15px">{base_path}/{version}/component/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2componentid(id, body, callback)</td>
    <td style="padding:15px">Update component</td>
    <td style="padding:15px">{base_path}/{version}/component/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2componentid(id, moveIssuesTo, callback)</td>
    <td style="padding:15px">Delete component</td>
    <td style="padding:15px">{base_path}/{version}/component/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2componentidrelatedIssueCounts(id, callback)</td>
    <td style="padding:15px">Get component issues count</td>
    <td style="padding:15px">{base_path}/{version}/component/{pathv1}/relatedIssueCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeycomponent(projectIdOrKey, startAt, maxResults, orderBy, query, callback)</td>
    <td style="padding:15px">Get project components paginated</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/component?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeycomponents(projectIdOrKey, callback)</td>
    <td style="padding:15px">Get project components</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/components?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2customFieldOptionid(id, callback)</td>
    <td style="padding:15px">Get custom field option</td>
    <td style="padding:15px">{base_path}/{version}/customFieldOption/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2field(callback)</td>
    <td style="padding:15px">Get fields</td>
    <td style="padding:15px">{base_path}/{version}/field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2field(body, callback)</td>
    <td style="padding:15px">Create custom field</td>
    <td style="padding:15px">{base_path}/{version}/field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2fieldfieldKeyoption(startAt, maxResults, fieldKey, callback)</td>
    <td style="padding:15px">Get all issue field options</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2fieldfieldKeyoption(fieldKey, body, callback)</td>
    <td style="padding:15px">Create issue field option</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2fieldfieldKeyoptionsuggestionsedit(startAt, maxResults, projectId, fieldKey, callback)</td>
    <td style="padding:15px">Get selectable issue field options</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option/suggestions/edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2fieldfieldKeyoptionsuggestionssearch(startAt, maxResults, projectId, fieldKey, callback)</td>
    <td style="padding:15px">Get visible issue field options</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option/suggestions/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2fieldfieldKeyoptionoptionId(fieldKey, optionId, callback)</td>
    <td style="padding:15px">Get issue field option</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2fieldfieldKeyoptionoptionId(fieldKey, optionId, body, callback)</td>
    <td style="padding:15px">Update issue field option</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2fieldfieldKeyoptionoptionId(fieldKey, optionId, callback)</td>
    <td style="padding:15px">Delete issue field option</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2fieldfieldKeyoptionoptionIdissue(replaceWith, jql, fieldKey, optionId, callback)</td>
    <td style="padding:15px">Replace issue field option</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/option/{pathv2}/issue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2group(groupname, expand, callback)</td>
    <td style="padding:15px">Get group</td>
    <td style="padding:15px">{base_path}/{version}/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2group(body, callback)</td>
    <td style="padding:15px">Create group</td>
    <td style="padding:15px">{base_path}/{version}/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2group(groupname, swapGroup, callback)</td>
    <td style="padding:15px">Remove group</td>
    <td style="padding:15px">{base_path}/{version}/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2groupmember(groupname, includeInactiveUsers, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get users from group</td>
    <td style="padding:15px">{base_path}/{version}/group/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2groupuser(groupname, body, callback)</td>
    <td style="padding:15px">Add user to group</td>
    <td style="padding:15px">{base_path}/{version}/group/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2groupuser(groupname, username, accountId, callback)</td>
    <td style="padding:15px">Remove user from group</td>
    <td style="padding:15px">{base_path}/{version}/group/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2groupspicker(accountId, query, exclude, maxResults, userName, callback)</td>
    <td style="padding:15px">Find groups</td>
    <td style="padding:15px">{base_path}/{version}/groups/picker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2groupuserpicker(query, maxResults, showAvatar, fieldId, projectId, issueTypeId, avatarSize, caseInsensitive, excludeConnectAddons, callback)</td>
    <td style="padding:15px">Find users and groups</td>
    <td style="padding:15px">{base_path}/{version}/groupuserpicker?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issue(updateHistory, body, callback)</td>
    <td style="padding:15px">Create issue</td>
    <td style="padding:15px">{base_path}/{version}/issue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issuebulk(body, callback)</td>
    <td style="padding:15px">Bulk issue create</td>
    <td style="padding:15px">{base_path}/{version}/issue/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuecreatemeta(projectIds, projectKeys, issuetypeIds, issuetypeNames, expand, callback)</td>
    <td style="padding:15px">Get create issue metadata</td>
    <td style="padding:15px">{base_path}/{version}/issue/createmeta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKey(issueIdOrKey, fields, fieldsByKeys, expand, properties, updateHistory, callback)</td>
    <td style="padding:15px">Get issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKey(issueIdOrKey, notifyUsers, overrideScreenSecurity, overrideEditableFlag, body, callback)</td>
    <td style="padding:15px">Edit issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKey(issueIdOrKey, deleteSubtasks, callback)</td>
    <td style="padding:15px">Delete issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeyassignee(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Assign issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/assignee?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeychangelog(issueIdOrKey, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get change logs</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/changelog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyeditmeta(issueIdOrKey, overrideScreenSecurity, overrideEditableFlag, callback)</td>
    <td style="padding:15px">Get edit issue metadata</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/editmeta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeynotify(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Send notification for issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/notify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeytransitions(issueIdOrKey, expand, transitionId, skipRemoteOnlyCondition, callback)</td>
    <td style="padding:15px">Get transitions</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/transitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeytransitions(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Transition issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/transitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyremotelink(issueIdOrKey, globalId, callback)</td>
    <td style="padding:15px">Get remote issue links</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/remotelink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeyremotelink(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Create or update remote issue link</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/remotelink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeyremotelink(issueIdOrKey, globalId, callback)</td>
    <td style="padding:15px">Delete remote issue link by global id</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/remotelink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyremotelinklinkId(issueIdOrKey, linkId, callback)</td>
    <td style="padding:15px">Get remote issue link by id</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/remotelink/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeyremotelinklinkId(issueIdOrKey, linkId, body, callback)</td>
    <td style="padding:15px">Update remote issue link</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/remotelink/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeyremotelinklinkId(issueIdOrKey, linkId, callback)</td>
    <td style="padding:15px">Delete remote issue link by id</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/remotelink/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyvotes(issueIdOrKey, callback)</td>
    <td style="padding:15px">Get votes</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/votes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeyvotes(issueIdOrKey, callback)</td>
    <td style="padding:15px">Add vote</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/votes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeyvotes(issueIdOrKey, callback)</td>
    <td style="padding:15px">Delete vote</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/votes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeywatchers(issueIdOrKey, callback)</td>
    <td style="padding:15px">Get issue watchers</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeywatchers(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Add watcher</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeywatchers(issueIdOrKey, username, accountId, callback)</td>
    <td style="padding:15px">Delete watcher</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyworklog(issueIdOrKey, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Get issue worklogs</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueissueIdOrKeyworklog(issueIdOrKey, notifyUsers, adjustEstimate, newEstimate, reduceBy, expand, overrideEditableFlag, body, callback)</td>
    <td style="padding:15px">Add worklog</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyworklogid(issueIdOrKey, id, expand, callback)</td>
    <td style="padding:15px">Get worklog</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeyworklogid(issueIdOrKey, id, notifyUsers, adjustEstimate, newEstimate, expand, overrideEditableFlag, body, callback)</td>
    <td style="padding:15px">Update worklog</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeyworklogid(issueIdOrKey, id, notifyUsers, adjustEstimate, newEstimate, increaseBy, overrideEditableFlag, callback)</td>
    <td style="padding:15px">Delete worklog</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2worklogdeleted(since, callback)</td>
    <td style="padding:15px">Get IDs of deleted worklogs</td>
    <td style="padding:15px">{base_path}/{version}/worklog/deleted?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2workloglist(expand, body, callback)</td>
    <td style="padding:15px">Get worklogs</td>
    <td style="padding:15px">{base_path}/{version}/worklog/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2worklogupdated(since, expand, callback)</td>
    <td style="padding:15px">Get IDs of updated worklogs</td>
    <td style="padding:15px">{base_path}/{version}/worklog/updated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyworklogworklogIdproperties(issueIdOrKey, worklogId, callback)</td>
    <td style="padding:15px">Get worklog property keys</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey(issueIdOrKey, worklogId, propertyKey, callback)</td>
    <td style="padding:15px">Get worklog property</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey(issueIdOrKey, worklogId, propertyKey, body, callback)</td>
    <td style="padding:15px">Set worklog property</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueissueIdOrKeyworklogworklogIdpropertiespropertyKey(issueIdOrKey, worklogId, propertyKey, callback)</td>
    <td style="padding:15px">Delete worklog property</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/worklog/{pathv2}/properties/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueLink(body, callback)</td>
    <td style="padding:15px">Create issue link</td>
    <td style="padding:15px">{base_path}/{version}/issueLink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueLinklinkId(linkId, callback)</td>
    <td style="padding:15px">Get issue link</td>
    <td style="padding:15px">{base_path}/{version}/issueLink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueLinklinkId(linkId, callback)</td>
    <td style="padding:15px">Delete issue link</td>
    <td style="padding:15px">{base_path}/{version}/issueLink/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueLinkType(callback)</td>
    <td style="padding:15px">Get issue link types</td>
    <td style="padding:15px">{base_path}/{version}/issueLinkType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issueLinkType(body, callback)</td>
    <td style="padding:15px">Create issue link type</td>
    <td style="padding:15px">{base_path}/{version}/issueLinkType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issueLinkTypeissueLinkTypeId(issueLinkTypeId, callback)</td>
    <td style="padding:15px">Get issue link type</td>
    <td style="padding:15px">{base_path}/{version}/issueLinkType/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issueLinkTypeissueLinkTypeId(issueLinkTypeId, body, callback)</td>
    <td style="padding:15px">Update issue link type</td>
    <td style="padding:15px">{base_path}/{version}/issueLinkType/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issueLinkTypeissueLinkTypeId(issueLinkTypeId, callback)</td>
    <td style="padding:15px">Delete issue link type</td>
    <td style="padding:15px">{base_path}/{version}/issueLinkType/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuesecurityschemes(callback)</td>
    <td style="padding:15px">Get issue security schemes</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuesecurityschemesid(id, callback)</td>
    <td style="padding:15px">Get issue security scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuetype(callback)</td>
    <td style="padding:15px">Get all issue types for user</td>
    <td style="padding:15px">{base_path}/{version}/issuetype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issuetype(body, callback)</td>
    <td style="padding:15px">Create issue type</td>
    <td style="padding:15px">{base_path}/{version}/issuetype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuetypeid(id, callback)</td>
    <td style="padding:15px">Get issue type</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuetypeid(id, body, callback)</td>
    <td style="padding:15px">Update issue type</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuetypeid(id, alternativeIssueTypeId, callback)</td>
    <td style="padding:15px">Delete issue type</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuetypeidalternatives(id, callback)</td>
    <td style="padding:15px">Get alternative issue types</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}/alternatives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2issuetypeidavatar2(id, x, y, size, body, callback)</td>
    <td style="padding:15px">Load issue type avatar</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}/avatar2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuetypeissueTypeIdproperties(issueTypeId, callback)</td>
    <td style="padding:15px">Get issue type property keys</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2issuetypeissueTypeIdpropertiespropertyKey(issueTypeId, propertyKey, callback)</td>
    <td style="padding:15px">Get issue type property</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2issuetypeissueTypeIdpropertiespropertyKey(issueTypeId, propertyKey, body, callback)</td>
    <td style="padding:15px">Set issue type property</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2issuetypeissueTypeIdpropertiespropertyKey(issueTypeId, propertyKey, callback)</td>
    <td style="padding:15px">Delete issue type property</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2priority(callback)</td>
    <td style="padding:15px">Get priorities</td>
    <td style="padding:15px">{base_path}/{version}/priority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2priorityid(id, callback)</td>
    <td style="padding:15px">Get priority</td>
    <td style="padding:15px">{base_path}/{version}/priority/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectprojectIdOrKeyavatar(projectIdOrKey, body, callback)</td>
    <td style="padding:15px">Set project avatar</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/avatar?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2projectprojectIdOrKeyavatarid(projectIdOrKey, id, callback)</td>
    <td style="padding:15px">Delete project avatar</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/avatar/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2projectprojectIdOrKeyavatar2(projectIdOrKey, x, y, size, body, callback)</td>
    <td style="padding:15px">Load project avatar</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/avatar2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyavatars(projectIdOrKey, callback)</td>
    <td style="padding:15px">Get all project avatars</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/avatars?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyproperties(projectIdOrKey, callback)</td>
    <td style="padding:15px">Get project property keys</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeypropertiespropertyKey(projectIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Get project property</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectprojectIdOrKeypropertiespropertyKey(projectIdOrKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Set project property</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2projectprojectIdOrKeypropertiespropertyKey(projectIdOrKey, propertyKey, callback)</td>
    <td style="padding:15px">Delete project property</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyrole(projectIdOrKey, callback)</td>
    <td style="padding:15px">Get project roles for project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyroleid(projectIdOrKey, id, callback)</td>
    <td style="padding:15px">Get project role for project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyroledetails(projectIdOrKey, currentMember, callback)</td>
    <td style="padding:15px">Get project role details</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/roledetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2role(callback)</td>
    <td style="padding:15px">Get all project roles</td>
    <td style="padding:15px">{base_path}/{version}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2role(body, callback)</td>
    <td style="padding:15px">Create project role</td>
    <td style="padding:15px">{base_path}/{version}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2roleid(id, callback)</td>
    <td style="padding:15px">Get project role by ID</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2roleid(id, body, callback)</td>
    <td style="padding:15px">Fully update project role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2roleid(id, body, callback)</td>
    <td style="padding:15px">Partial update project role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2roleid(id, swap, callback)</td>
    <td style="padding:15px">Delete project role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectprojectIdOrKeyroleid(projectIdOrKey, id, body, callback)</td>
    <td style="padding:15px">Set actors for project role</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2projectprojectIdOrKeyroleid(projectIdOrKey, id, body, callback)</td>
    <td style="padding:15px">Add actors to project role</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2projectprojectIdOrKeyroleid(projectIdOrKey, id, user, group, callback)</td>
    <td style="padding:15px">Delete actors from project role</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/role/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2roleidactors(id, callback)</td>
    <td style="padding:15px">Get default actors for project role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}/actors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2roleidactors(id, body, callback)</td>
    <td style="padding:15px">Add default actors to project role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}/actors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2roleidactors(id, user, group, callback)</td>
    <td style="padding:15px">Delete default actors from project role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}/actors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyversion(projectIdOrKey, startAt, maxResults, orderBy, query, status, expand, callback)</td>
    <td style="padding:15px">Get project versions paginated</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectprojectIdOrKeyversions(projectIdOrKey, expand, callback)</td>
    <td style="padding:15px">Get project versions</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2version(body, callback)</td>
    <td style="padding:15px">Create version</td>
    <td style="padding:15px">{base_path}/{version}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2versionid(id, expand, callback)</td>
    <td style="padding:15px">Get version</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2versionid(id, body, callback)</td>
    <td style="padding:15px">Update version</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2versionid(id, moveFixIssuesTo, moveAffectedIssuesTo, callback)</td>
    <td style="padding:15px">Delete version</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2versionidmergetomoveIssuesTo(id, moveIssuesTo, callback)</td>
    <td style="padding:15px">Merge versions</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}/mergeto/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2versionidmove(id, body, callback)</td>
    <td style="padding:15px">Move version</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2versionidrelatedIssueCounts(id, callback)</td>
    <td style="padding:15px">Get version's related issues count</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}/relatedIssueCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2versionidremoveAndSwap(id, body, callback)</td>
    <td style="padding:15px">Delete and replace version</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}/removeAndSwap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2versionidunresolvedIssueCount(id, callback)</td>
    <td style="padding:15px">Get version's unresolved issues count</td>
    <td style="padding:15px">{base_path}/{version}/version/{pathv1}/unresolvedIssueCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectCategory(callback)</td>
    <td style="padding:15px">Get all project categories</td>
    <td style="padding:15px">{base_path}/{version}/projectCategory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2projectCategory(body, callback)</td>
    <td style="padding:15px">Create project category</td>
    <td style="padding:15px">{base_path}/{version}/projectCategory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectCategoryid(id, callback)</td>
    <td style="padding:15px">Get project category by id</td>
    <td style="padding:15px">{base_path}/{version}/projectCategory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2projectCategoryid(id, body, callback)</td>
    <td style="padding:15px">Update project category</td>
    <td style="padding:15px">{base_path}/{version}/projectCategory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2projectCategoryid(id, callback)</td>
    <td style="padding:15px">Delete project category</td>
    <td style="padding:15px">{base_path}/{version}/projectCategory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectvalidatekey(key, callback)</td>
    <td style="padding:15px">Validate project key</td>
    <td style="padding:15px">{base_path}/{version}/projectvalidate/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectvalidatevalidProjectKey(key, callback)</td>
    <td style="padding:15px">Get valid project key</td>
    <td style="padding:15px">{base_path}/{version}/projectvalidate/validProjectKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2projectvalidatevalidProjectName(name, callback)</td>
    <td style="padding:15px">Get valid project name</td>
    <td style="padding:15px">{base_path}/{version}/projectvalidate/validProjectName?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2resolution(callback)</td>
    <td style="padding:15px">Get resolutions</td>
    <td style="padding:15px">{base_path}/{version}/resolution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2resolutionid(id, callback)</td>
    <td style="padding:15px">Get resolution</td>
    <td style="padding:15px">{base_path}/{version}/resolution/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2screens(startAt, maxResults, callback)</td>
    <td style="padding:15px">Get all screens</td>
    <td style="padding:15px">{base_path}/{version}/screens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2screensaddToDefaultfieldId(fieldId, callback)</td>
    <td style="padding:15px">Add field to default screen</td>
    <td style="padding:15px">{base_path}/{version}/screens/addToDefault/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2screensscreenIdavailableFields(screenId, callback)</td>
    <td style="padding:15px">Get available screen fields</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/availableFields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2screensscreenIdtabs(screenId, projectKey, callback)</td>
    <td style="padding:15px">Get all screen tabs</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2screensscreenIdtabs(screenId, body, callback)</td>
    <td style="padding:15px">Create screen tab</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRestapi2screensscreenIdtabstabId(screenId, tabId, body, callback)</td>
    <td style="padding:15px">Update screen tab</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2screensscreenIdtabstabId(screenId, tabId, callback)</td>
    <td style="padding:15px">Delete screen tab</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2screensscreenIdtabstabIdfields(screenId, tabId, projectKey, callback)</td>
    <td style="padding:15px">Get all screen tab fields</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2screensscreenIdtabstabIdfields(screenId, tabId, body, callback)</td>
    <td style="padding:15px">Add screen tab field</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRestapi2screensscreenIdtabstabIdfieldsid(screenId, tabId, id, callback)</td>
    <td style="padding:15px">Remove screen tab field</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}/fields/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2screensscreenIdtabstabIdfieldsidmove(screenId, tabId, id, body, callback)</td>
    <td style="padding:15px">Move screen tab field</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}/fields/{pathv3}/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestapi2screensscreenIdtabstabIdmovepos(screenId, tabId, pos, callback)</td>
    <td style="padding:15px">Move screen tab</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}/tabs/{pathv2}/move/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2securitylevelid(id, callback)</td>
    <td style="padding:15px">Get issue security level</td>
    <td style="padding:15px">{base_path}/{version}/securitylevel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2status(callback)</td>
    <td style="padding:15px">Get all statuses</td>
    <td style="padding:15px">{base_path}/{version}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2statusidOrName(idOrName, callback)</td>
    <td style="padding:15px">Get status</td>
    <td style="padding:15px">{base_path}/{version}/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2statuscategory(callback)</td>
    <td style="padding:15px">Get all status categories</td>
    <td style="padding:15px">{base_path}/{version}/statuscategory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestapi2statuscategoryidOrKey(idOrKey, callback)</td>
    <td style="padding:15px">Get status category</td>
    <td style="padding:15px">{base_path}/{version}/statuscategory/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMultipleCustomFieldValues(generateChangelog, body, callback)</td>
    <td style="padding:15px">Update custom fields</td>
    <td style="padding:15px">{base_path}/{version}/app/field/value?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomFieldValue(fieldIdOrKey, generateChangelog, body, callback)</td>
    <td style="padding:15px">Update custom field value</td>
    <td style="padding:15px">{base_path}/{version}/app/field/{pathv1}/value?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomFieldConfiguration(fieldIdOrKey, id, contextId, fieldContextId, issueId, projectKeyOrId, issueTypeId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get custom field configurations</td>
    <td style="padding:15px">{base_path}/{version}/app/field/{pathv1}/context/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomFieldConfiguration(fieldIdOrKey, body, callback)</td>
    <td style="padding:15px">Update custom field configurations</td>
    <td style="padding:15px">{base_path}/{version}/app/field/{pathv1}/context/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachmentContent(id, redirect, callback)</td>
    <td style="padding:15px">Get attachment content</td>
    <td style="padding:15px">{base_path}/{version}/attachment/content/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachmentThumbnail(id, redirect, fallbackToDefault, width, height, callback)</td>
    <td style="padding:15px">Get attachment thumbnail</td>
    <td style="padding:15px">{base_path}/{version}/attachment/thumbnail/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDashboard(body, callback)</td>
    <td style="padding:15px">Create dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDashboard(id, body, callback)</td>
    <td style="padding:15px">Update dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboard(id, callback)</td>
    <td style="padding:15px">Delete dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copyDashboard(id, body, callback)</td>
    <td style="padding:15px">Copy dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/copy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEvents(callback)</td>
    <td style="padding:15px">Get events</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeLogsByIds(issueIdOrKey, body, callback)</td>
    <td style="padding:15px">Get changelogs by IDs</td>
    <td style="padding:15px">{base_path}/{version}/issue/{pathv1}/changelog/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">analyseExpression(check = 'syntax', body, callback)</td>
    <td style="padding:15px">Analyse Jira expression</td>
    <td style="padding:15px">{base_path}/{version}/expression/analyse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFieldsPaginated(startAt, maxResults, type, id, query, orderBy = 'contextsCount', expand, callback)</td>
    <td style="padding:15px">Get fields paginated</td>
    <td style="padding:15px">{base_path}/{version}/field/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomField(fieldId, body, callback)</td>
    <td style="padding:15px">Update custom field</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContextsForFieldDeprecated(fieldId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get contexts for a field</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/contexts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomField(id, callback)</td>
    <td style="padding:15px">Delete custom field</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreCustomField(id, callback)</td>
    <td style="padding:15px">Restore custom field from trash</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trashCustomField(id, callback)</td>
    <td style="padding:15px">Move custom field to trash</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/trash?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContextsForField(fieldId, isAnyIssueType, isGlobalContext, contextId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get custom field contexts</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomFieldContext(fieldId, body, callback)</td>
    <td style="padding:15px">Create custom field context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultValues(fieldId, contextId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get custom field contexts default values</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/defaultValue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultValues(fieldId, body, callback)</td>
    <td style="padding:15px">Set custom field contexts default values</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/defaultValue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypeMappingsForContexts(fieldId, contextId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get issue types for custom field context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/issuetypemapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomFieldContextsForProjectsAndIssueTypes(fieldId, startAt, maxResults, body, callback)</td>
    <td style="padding:15px">Get custom field contexts for projects and issue types</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectContextMapping(fieldId, contextId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get project mappings for custom field context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/projectmapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomFieldContext(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Update custom field context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomFieldContext(fieldId, contextId, callback)</td>
    <td style="padding:15px">Delete custom field context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIssueTypesToContext(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Add issue types to context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/issuetype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIssueTypesFromContext(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Remove issue types from context</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/issuetype/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignProjectsToCustomFieldContext(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Assign custom field context to projects</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeCustomFieldContextFromProjects(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Remove custom field context from projects</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/project/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOptionsForContext(fieldId, contextId, optionId, onlyOptions, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get custom field options (context)</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomFieldOption(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Update custom field options (context)</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomFieldOption(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Create custom field options (context)</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/option?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reorderCustomFieldOptions(fieldId, contextId, body, callback)</td>
    <td style="padding:15px">Reorder custom field options (context)</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/option/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomFieldOption(fieldId, contextId, optionId, callback)</td>
    <td style="padding:15px">Delete custom field options (context)</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/context/{pathv2}/option/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScreensForField(fieldId, startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Get screens for a field</td>
    <td style="padding:15px">{base_path}/{version}/field/{pathv1}/screens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScreen(body, callback)</td>
    <td style="padding:15px">Create screen</td>
    <td style="padding:15px">{base_path}/{version}/screens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScreen(screenId, body, callback)</td>
    <td style="padding:15px">Update screen</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScreen(screenId, callback)</td>
    <td style="padding:15px">Delete screen</td>
    <td style="padding:15px">{base_path}/{version}/screens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFieldConfigurations(startAt, maxResults, id, isDefault, query, callback)</td>
    <td style="padding:15px">Get all field configurations</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFieldConfiguration(body, callback)</td>
    <td style="padding:15px">Create field configuration</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFieldConfiguration(id, body, callback)</td>
    <td style="padding:15px">Update field configuration</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfiguration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFieldConfiguration(id, callback)</td>
    <td style="padding:15px">Delete field configuration</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfiguration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFieldConfigurationItems(id, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get field configuration items</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfiguration/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFieldConfigurationItems(id, body, callback)</td>
    <td style="padding:15px">Update field configuration items</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfiguration/{pathv1}/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFieldConfigurationSchemes(startAt, maxResults, id, callback)</td>
    <td style="padding:15px">Get all field configuration schemes</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFieldConfigurationScheme(body, callback)</td>
    <td style="padding:15px">Create field configuration scheme</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFieldConfigurationSchemeMappings(startAt, maxResults, fieldConfigurationSchemeId, callback)</td>
    <td style="padding:15px">Get field configuration issue type items</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFieldConfigurationSchemeProjectMapping(startAt, maxResults, projectId, callback)</td>
    <td style="padding:15px">Get field configuration schemes for projects</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignFieldConfigurationSchemeToProject(body, callback)</td>
    <td style="padding:15px">Assign field configuration scheme to project</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFieldConfigurationScheme(id, body, callback)</td>
    <td style="padding:15px">Update field configuration scheme</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFieldConfigurationScheme(id, callback)</td>
    <td style="padding:15px">Delete field configuration scheme</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setFieldConfigurationSchemeMapping(id, body, callback)</td>
    <td style="padding:15px">Assign issue types to field configurations</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/{pathv1}/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIssueTypesFromGlobalFieldConfigurationScheme(id, body, callback)</td>
    <td style="padding:15px">Remove issue types from field configuration scheme</td>
    <td style="padding:15px">{base_path}/{version}/fieldconfigurationscheme/{pathv1}/mapping/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkGetGroups(startAt, maxResults, groupId, groupName, callback)</td>
    <td style="padding:15px">Bulk get groups</td>
    <td style="padding:15px">{base_path}/{version}/group/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicense(callback)</td>
    <td style="padding:15px">Get license</td>
    <td style="padding:15px">{base_path}/{version}/instance/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkSetIssuesPropertiesList(body, callback)</td>
    <td style="padding:15px">Bulk set issues properties by list</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkSetIssuePropertiesByIssue(body, callback)</td>
    <td style="padding:15px">Bulk set issue properties by issue</td>
    <td style="padding:15px">{base_path}/{version}/issue/properties/multi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIsWatchingIssueBulk(body, callback)</td>
    <td style="padding:15px">Get is watching issue bulk</td>
    <td style="padding:15px">{base_path}/{version}/issue/watching?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueSecurityLevelMembers(issueSecuritySchemeId, startAt, maxResults, issueSecurityLevelId, expand, callback)</td>
    <td style="padding:15px">Get issue security level members</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypesForProject(projectId, level, callback)</td>
    <td style="padding:15px">Get issue types for project</td>
    <td style="padding:15px">{base_path}/{version}/issuetype/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIssueTypeSchemes(startAt, maxResults, id, callback)</td>
    <td style="padding:15px">Get all issue type schemes</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIssueTypeScheme(body, callback)</td>
    <td style="padding:15px">Create issue type scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypeSchemesMapping(startAt, maxResults, issueTypeSchemeId, callback)</td>
    <td style="padding:15px">Get issue type scheme items</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypeSchemeForProjects(startAt, maxResults, projectId, callback)</td>
    <td style="padding:15px">Get issue type schemes for projects</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignIssueTypeSchemeToProject(body, callback)</td>
    <td style="padding:15px">Assign issue type scheme to project</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIssueTypeScheme(issueTypeSchemeId, body, callback)</td>
    <td style="padding:15px">Update issue type scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIssueTypeScheme(issueTypeSchemeId, callback)</td>
    <td style="padding:15px">Delete issue type scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIssueTypesToIssueTypeScheme(issueTypeSchemeId, body, callback)</td>
    <td style="padding:15px">Add issue types to issue type scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/{pathv1}/issuetype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reorderIssueTypesInIssueTypeScheme(issueTypeSchemeId, body, callback)</td>
    <td style="padding:15px">Change order of issue types</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/{pathv1}/issuetype/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIssueTypeFromIssueTypeScheme(issueTypeSchemeId, issueTypeId, callback)</td>
    <td style="padding:15px">Remove issue type from issue type scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescheme/{pathv1}/issuetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypeScreenSchemes(startAt, maxResults, id, callback)</td>
    <td style="padding:15px">Get issue type screen schemes</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIssueTypeScreenScheme(body, callback)</td>
    <td style="padding:15px">Create issue type screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypeScreenSchemeMappings(startAt, maxResults, issueTypeScreenSchemeId, callback)</td>
    <td style="padding:15px">Get issue type screen scheme items</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIssueTypeScreenSchemeProjectAssociations(startAt, maxResults, projectId, callback)</td>
    <td style="padding:15px">Get issue type screen schemes for projects</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignIssueTypeScreenSchemeToProject(body, callback)</td>
    <td style="padding:15px">Assign issue type screen scheme to project</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIssueTypeScreenScheme(issueTypeScreenSchemeId, body, callback)</td>
    <td style="padding:15px">Update issue type screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIssueTypeScreenScheme(issueTypeScreenSchemeId, callback)</td>
    <td style="padding:15px">Delete issue type screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appendMappingsForIssueTypeScreenScheme(issueTypeScreenSchemeId, body, callback)</td>
    <td style="padding:15px">Append mappings to issue type screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/{pathv1}/mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDefaultScreenScheme(issueTypeScreenSchemeId, body, callback)</td>
    <td style="padding:15px">Update issue type screen scheme default screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/{pathv1}/mapping/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeMappingsFromIssueTypeScreenScheme(issueTypeScreenSchemeId, body, callback)</td>
    <td style="padding:15px">Remove mappings from issue type screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/{pathv1}/mapping/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectsForIssueTypeScreenScheme(issueTypeScreenSchemeId, startAt, maxResults, callback)</td>
    <td style="padding:15px">Get issue type screen scheme projects</td>
    <td style="padding:15px">{base_path}/{version}/issuetypescreenscheme/{pathv1}/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoCompletePost(body, callback)</td>
    <td style="padding:15px">Get field reference data (POST)</td>
    <td style="padding:15px">{base_path}/{version}/jql/autocompletedata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">parseJqlQueries(validation = 'strict', body, callback)</td>
    <td style="padding:15px">Parse JQL query</td>
    <td style="padding:15px">{base_path}/{version}/jql/parse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">matchIssues(body, callback)</td>
    <td style="padding:15px">Check issues against JQL</td>
    <td style="padding:15px">{base_path}/{version}/jql/match?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLabels(startAt, maxResults, callback)</td>
    <td style="padding:15px">Get all labels</td>
    <td style="padding:15px">{base_path}/{version}/label?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecent(expand, properties, callback)</td>
    <td style="padding:15px">Get recent projects</td>
    <td style="padding:15px">{base_path}/{version}/project/recent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">archiveProject(projectIdOrKey, callback)</td>
    <td style="padding:15px">Archive project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectAsynchronously(projectIdOrKey, callback)</td>
    <td style="padding:15px">Delete project asynchronously</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restore(projectIdOrKey, callback)</td>
    <td style="padding:15px">Restore deleted project</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHierarchy(projectId, callback)</td>
    <td style="padding:15px">Get project issue type hierarchy</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/hierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAccessibleProjectTypes(callback)</td>
    <td style="padding:15px">Get licensed project types</td>
    <td style="padding:15px">{base_path}/{version}/project/type/accessible?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFeaturesForProject(projectIdOrKey, callback)</td>
    <td style="padding:15px">Get project features</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">toggleFeatureForProject(projectIdOrKey, featureKey, body, callback)</td>
    <td style="padding:15px">Set project feature state</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/features/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectEmail(projectId, callback)</td>
    <td style="padding:15px">Get project's sender email</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProjectEmail(projectId, body, callback)</td>
    <td style="padding:15px">Set project's sender email</td>
    <td style="padding:15px">{base_path}/{version}/project/{pathv1}/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScreenSchemes(startAt, maxResults, id, callback)</td>
    <td style="padding:15px">Get screen schemes</td>
    <td style="padding:15px">{base_path}/{version}/screenscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScreenScheme(body, callback)</td>
    <td style="padding:15px">Create screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/screenscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScreenScheme(screenSchemeId, body, callback)</td>
    <td style="padding:15px">Update screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/screenscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScreenScheme(screenSchemeId, callback)</td>
    <td style="padding:15px">Delete screen scheme</td>
    <td style="padding:15px">{base_path}/{version}/screenscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvatarImageByType(type = 'issuetype', size = 'xsmall', format = 'png', callback)</td>
    <td style="padding:15px">Get avatar image by type</td>
    <td style="padding:15px">{base_path}/{version}/universal_avatar/view/type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvatarImageByID(type = 'issuetype', id, size = 'xsmall', format = 'png', callback)</td>
    <td style="padding:15px">Get avatar image by ID</td>
    <td style="padding:15px">{base_path}/{version}/universal_avatar/view/type/{pathv1}/avatar/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvatarImageByOwner(type = 'issuetype', entityId, size = 'xsmall', format = 'png', callback)</td>
    <td style="padding:15px">Get avatar image by owner</td>
    <td style="padding:15px">{base_path}/{version}/universal_avatar/view/type/{pathv1}/owner/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserEmail(accountId, callback)</td>
    <td style="padding:15px">Get user email</td>
    <td style="padding:15px">{base_path}/{version}/user/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserEmailBulk(accountId, callback)</td>
    <td style="padding:15px">Get user email bulk</td>
    <td style="padding:15px">{base_path}/{version}/user/email/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsersDefault(startAt, maxResults, callback)</td>
    <td style="padding:15px">Get all users default</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsers(startAt, maxResults, callback)</td>
    <td style="padding:15px">Get all users</td>
    <td style="padding:15px">{base_path}/{version}/users/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicWebhooksForApp(startAt, maxResults, callback)</td>
    <td style="padding:15px">Get dynamic webhooks for app</td>
    <td style="padding:15px">{base_path}/{version}/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDynamicWebhooks(body, callback)</td>
    <td style="padding:15px">Register dynamic webhooks</td>
    <td style="padding:15px">{base_path}/{version}/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebhookById(body, callback)</td>
    <td style="padding:15px">Delete webhooks by ID</td>
    <td style="padding:15px">{base_path}/{version}/webhook?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFailedWebhooks(maxResults, after, callback)</td>
    <td style="padding:15px">Get failed webhooks</td>
    <td style="padding:15px">{base_path}/{version}/webhook/failed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshWebhooks(body, callback)</td>
    <td style="padding:15px">Extend webhook life</td>
    <td style="padding:15px">{base_path}/{version}/webhook/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkflow(body, callback)</td>
    <td style="padding:15px">Create workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsPaginated(startAt, maxResults, workflowName, expand, queryString, orderBy = 'name', isActive, callback)</td>
    <td style="padding:15px">Get workflows paginated</td>
    <td style="padding:15px">{base_path}/{version}/workflow/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInactiveWorkflow(entityId, callback)</td>
    <td style="padding:15px">Delete inactive workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowTransitionRuleConfigurations(startAt, maxResults, types, keys, workflowNames, withTags, draft, expand, callback)</td>
    <td style="padding:15px">Get workflow transition rule configurations</td>
    <td style="padding:15px">{base_path}/{version}/workflow/rule/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWorkflowTransitionRuleConfigurations(body, callback)</td>
    <td style="padding:15px">Update workflow transition rule configurations</td>
    <td style="padding:15px">{base_path}/{version}/workflow/rule/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowTransitionRuleConfigurations(body, callback)</td>
    <td style="padding:15px">Delete workflow transition rule configurations</td>
    <td style="padding:15px">{base_path}/{version}/workflow/rule/config/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllWorkflowSchemes(startAt, maxResults, callback)</td>
    <td style="padding:15px">Get all workflow schemes</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowSchemeProjectAssociations(projectId, callback)</td>
    <td style="padding:15px">Get workflow scheme project associations</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignSchemeToProject(body, callback)</td>
    <td style="padding:15px">Assign workflow scheme to project</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">publishDraftWorkflowScheme(id, validateOnly, body, callback)</td>
    <td style="padding:15px">Publish draft workflow scheme</td>
    <td style="padding:15px">{base_path}/{version}/workflowscheme/{pathv1}/draft/publish?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBanner(callback)</td>
    <td style="padding:15px">Get announcement banner configuration</td>
    <td style="padding:15px">{base_path}/{version}/announcementBanner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBanner(body, callback)</td>
    <td style="padding:15px">Update announcement banner configuration</td>
    <td style="padding:15px">{base_path}/{version}/announcementBanner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAvailableDashboardGadgets(callback)</td>
    <td style="padding:15px">Get available gadgets</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/gadgets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGadgets(dashboardId, moduleKey, uri, gadgetId, callback)</td>
    <td style="padding:15px">Get gadgets</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/gadget?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGadget(dashboardId, body, callback)</td>
    <td style="padding:15px">Add gadget to dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/gadget?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGadget(dashboardId, gadgetId, body, callback)</td>
    <td style="padding:15px">Update gadget on dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/gadget/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGadget(dashboardId, gadgetId, callback)</td>
    <td style="padding:15px">Remove gadget from dashboard</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/gadget/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrashedFieldsPaginated(startAt, maxResults, id, query, expand = 'name', orderBy, callback)</td>
    <td style="padding:15px">Get fields in trash paginated</td>
    <td style="padding:15px">{base_path}/{version}/field/search/trashed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeFilterOwner(id, body, callback)</td>
    <td style="padding:15px">Change filter owner</td>
    <td style="padding:15px">{base_path}/{version}/filter/{pathv1}/owner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIssueSecurityScheme(body, callback)</td>
    <td style="padding:15px">Create issue security scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityLevels(startAt, maxResults, id, schemeId, onlyDefault, callback)</td>
    <td style="padding:15px">Get issue security levels</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultLevels(body, callback)</td>
    <td style="padding:15px">Set default issue security levels</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/level/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityLevelMembers(startAt, maxResults, id, schemeId, levelId, expand, callback)</td>
    <td style="padding:15px">Get issue security level members</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/level/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchProjectsUsingSecuritySchemes(startAt, maxResults, issueSecuritySchemeId, projectId, callback)</td>
    <td style="padding:15px">Get projects using issue security schemes</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchSecuritySchemes(startAt, maxResults, id, projectId, callback)</td>
    <td style="padding:15px">Search issue security schemes</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIssueSecurityScheme(id, body, callback)</td>
    <td style="padding:15px">Update issue security scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityScheme(schemeId, callback)</td>
    <td style="padding:15px">Delete issue security scheme</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSecurityLevel(schemeId, body, callback)</td>
    <td style="padding:15px">Add issue security levels</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityLevel(schemeId, levelId, body, callback)</td>
    <td style="padding:15px">Update issue security level</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}/level/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeLevel(schemeId, levelId, replaceWith, callback)</td>
    <td style="padding:15px">Remove issue security level</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}/level/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSecurityLevelMembers(schemeId, levelId, body, callback)</td>
    <td style="padding:15px">Add issue security level members</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}/level/{pathv2}/member?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeMemberFromSecurityLevel(schemeId, levelId, memberId, callback)</td>
    <td style="padding:15px">Remove member from issue security level</td>
    <td style="padding:15px">{base_path}/{version}/issuesecurityschemes/{pathv1}/level/{pathv2}/member/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sanitiseJqlQueries(body, callback)</td>
    <td style="padding:15px">Sanitize JQL queries</td>
    <td style="padding:15px">{base_path}/{version}/jql/sanitize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrecomputations(functionKey, startAt, maxResults, orderBy, filter, callback)</td>
    <td style="padding:15px">Get precomputations (apps)</td>
    <td style="padding:15px">{base_path}/{version}/jql/function/computation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrecomputations(body, callback)</td>
    <td style="padding:15px">Update precomputations (apps)</td>
    <td style="padding:15px">{base_path}/{version}/jql/function/computation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApproximateLicenseCount(callback)</td>
    <td style="padding:15px">Get approximate license count</td>
    <td style="padding:15px">{base_path}/{version}/license/approximateLicenseCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApproximateApplicationLicenseCount(applicationKey, callback)</td>
    <td style="padding:15px">Get approximate application license count</td>
    <td style="padding:15px">{base_path}/{version}/license/approximateLicenseCount/product/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNotificationScheme(body, callback)</td>
    <td style="padding:15px">Create notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationSchemeToProjectMappings(startAt, maxResults, notificationSchemeId, projectId, callback)</td>
    <td style="padding:15px">Get projects using notification schemes paginated</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNotificationScheme(id, body, callback)</td>
    <td style="padding:15px">Update notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNotifications(id, body, callback)</td>
    <td style="padding:15px">Add notifications to notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNotificationScheme(notificationSchemeId, callback)</td>
    <td style="padding:15px">Delete notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeNotificationFromNotificationScheme(notificationSchemeId, notificationId, callback)</td>
    <td style="padding:15px">Remove notification from notification scheme</td>
    <td style="padding:15px">{base_path}/{version}/notificationscheme/{pathv1}/notification/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPriority(body, callback)</td>
    <td style="padding:15px">Create priority</td>
    <td style="padding:15px">{base_path}/{version}/priority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultPriority(body, callback)</td>
    <td style="padding:15px">Set default priority</td>
    <td style="padding:15px">{base_path}/{version}/priority/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">movePriorities(body, callback)</td>
    <td style="padding:15px">Move priorities</td>
    <td style="padding:15px">{base_path}/{version}/priority/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchPriorities(startAt, maxResults, id, onlyDefault, callback)</td>
    <td style="padding:15px">Search priorities</td>
    <td style="padding:15px">{base_path}/{version}/priority/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePriority(id, body, callback)</td>
    <td style="padding:15px">Update priority</td>
    <td style="padding:15px">{base_path}/{version}/priority/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePriority(id, replaceWith, callback)</td>
    <td style="padding:15px">Delete priority</td>
    <td style="padding:15px">{base_path}/{version}/priority/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResolution(body, callback)</td>
    <td style="padding:15px">Create resolution</td>
    <td style="padding:15px">{base_path}/{version}/resolution?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultResolution(body, callback)</td>
    <td style="padding:15px">Set default resolution</td>
    <td style="padding:15px">{base_path}/{version}/resolution/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveResolutions(body, callback)</td>
    <td style="padding:15px">Move resolutions</td>
    <td style="padding:15px">{base_path}/{version}/resolution/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchResolutions(startAt, maxResults, id, onlyDefault, callback)</td>
    <td style="padding:15px">Search resolutions</td>
    <td style="padding:15px">{base_path}/{version}/resolution/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateResolution(id, body, callback)</td>
    <td style="padding:15px">Update resolution</td>
    <td style="padding:15px">{base_path}/{version}/resolution/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResolution(id, replaceWith, callback)</td>
    <td style="padding:15px">Delete resolution</td>
    <td style="padding:15px">{base_path}/{version}/resolution/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusesById(expand, id, callback)</td>
    <td style="padding:15px">Bulk get statuses</td>
    <td style="padding:15px">{base_path}/{version}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStatuses(body, callback)</td>
    <td style="padding:15px">Bulk update statuses</td>
    <td style="padding:15px">{base_path}/{version}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createStatuses(body, callback)</td>
    <td style="padding:15px">Bulk create statuses</td>
    <td style="padding:15px">{base_path}/{version}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStatusesById(id, callback)</td>
    <td style="padding:15px">Bulk delete Statuses</td>
    <td style="padding:15px">{base_path}/{version}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search(expand, projectId, startAt, maxResults, searchString, statusCategory, callback)</td>
    <td style="padding:15px">Search statuses paginated</td>
    <td style="padding:15px">{base_path}/{version}/statuses/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUiModifications(startAt, maxResults, expand, callback)</td>
    <td style="padding:15px">Get UI modifications</td>
    <td style="padding:15px">{base_path}/{version}/uiModifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUiModification(body, callback)</td>
    <td style="padding:15px">Create UI modification</td>
    <td style="padding:15px">{base_path}/{version}/uiModifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUiModification(uiModificationId, body, callback)</td>
    <td style="padding:15px">Update UI modification</td>
    <td style="padding:15px">{base_path}/{version}/uiModifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUiModification(uiModificationId, callback)</td>
    <td style="padding:15px">Delete UI modification</td>
    <td style="padding:15px">{base_path}/{version}/uiModifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addonPropertiesResourceGetAddonPropertiesGet(addonKey, callback)</td>
    <td style="padding:15px">Get app properties</td>
    <td style="padding:15px">/rest/atlassian-connect/1/addons/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addonPropertiesResourceGetAddonPropertyGet(addonKey, propertyKey, callback)</td>
    <td style="padding:15px">Get app property</td>
    <td style="padding:15px">/rest/atlassian-connect/1/addons/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addonPropertiesResourcePutAddonPropertyPut(addonKey, propertyKey, body, callback)</td>
    <td style="padding:15px">Set app property</td>
    <td style="padding:15px">/rest/atlassian-connect/1/addons/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addonPropertiesResourceDeleteAddonPropertyDelete(addonKey, propertyKey, callback)</td>
    <td style="padding:15px">Delete app property</td>
    <td style="padding:15px">/rest/atlassian-connect/1/addons/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addonPropertiesResourcePutAppPropertyPut(propertyKey, body, callback)</td>
    <td style="padding:15px">Set app property (Forge)</td>
    <td style="padding:15px">/rest/forge/1/app/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addonPropertiesResourceDeleteAppPropertyDelete(propertyKey, callback)</td>
    <td style="padding:15px">Delete app property (Forge)</td>
    <td style="padding:15px">/rest/forge/1/app/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dynamicModulesResourceGetModulesGet(callback)</td>
    <td style="padding:15px">Get modules</td>
    <td style="padding:15px">/rest/atlassian-connect/1/app/module/dynamic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dynamicModulesResourceRegisterModulesPost(body, callback)</td>
    <td style="padding:15px">Register modules</td>
    <td style="padding:15px">/rest/atlassian-connect/1/app/module/dynamic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dynamicModulesResourceRemoveModulesDelete(moduleKey, callback)</td>
    <td style="padding:15px">Remove modules</td>
    <td style="padding:15px">/rest/atlassian-connect/1/app/module/dynamic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appIssueFieldValueUpdateResourceUpdateIssueFieldsPut(body, callback)</td>
    <td style="padding:15px">Bulk update custom field value</td>
    <td style="padding:15px">/rest/atlassian-connect/1/migration/field?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationResourceUpdateEntityPropertiesValuePut(entityType = 'IssueProperty', body, callback)</td>
    <td style="padding:15px">Bulk update entity properties</td>
    <td style="padding:15px">/rest/atlassian-connect/1/migration/properties/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrationResourceWorkflowRuleSearchPost(body, callback)</td>
    <td style="padding:15px">Get workflow transition rule configurations</td>
    <td style="padding:15px">/rest/atlassian-connect/1/migration/workflow/rule/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
